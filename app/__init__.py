from PySide2 import QtWidgets

from ComumComissoes.cargo import listar as car_listar, cadastro as car_cadastro
from ComumComissoes.lotacao import listar as lot_listar, cadastro as lot_cadastro
from QtUtils import QtUtils
from QtUtils.backup import backup
from QtUtils.mensageiro import Mensageiro
from QtUtils.user import autenticacao, listar as user_listar, cadastro, usuario
from QtUtils.user.models import User
from QtUtils.registro.listar import ListarRegistro

from avaliacao import listar as av_listar
from intersticio.criar import CriarIntersticioDialogo
from intersticio.recalcular import RecalcularIntersticioDialogo
from intersticio.relatorio import IntersticioRelatorioJanela
from servidor import listar as ser_listar, cadastro as ser_cadastro
from .views import app


class JanelaPrincipal(QtWidgets.QMainWindow, Mensageiro):
    def __init__(self, parent=None):
        super(JanelaPrincipal, self).__init__(parent)
        self.ui = app.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.mdiArea.setActivationOrder(
            QtWidgets.QMdiArea.ActivationHistoryOrder
        )
        self.mdiArea = self.ui.mdiArea
        self.carregar_titulo()
        self.contruir()
    
    def alterar_nome(self):
        cadastro.AlterarNome(self, usuario.Usuario().getUsuario()).exec_()

    def alterar_senha(self):
        cadastro.AlterarSenha(self, usuario.Usuario().getUsuario()).exec_()

    def backup_db(self):
        backup.BackupDialogo(self).exec_()

    def cadastrar_cargo(self):
        car_cadastro.Cadastro(self).exec_()
    
    def carregar_titulo(self):
        if not usuario.Usuario().getUsuario():
            self.ui.menuUsuarios.setEnabled(False)
            self.setWindowTitle(QtUtils.definicoes.nome_aplicacao)
        else:
            self.setWindowTitle(
                "%s -*-*-*- Usuário logado: %s" % (
                    QtUtils.definicoes.nome_aplicacao, usuario.Usuario().getUsuario().nome_acesso
                )
            )

    def configuracao(self):
        QtUtils.config.janela(parent=self).exec_()

    def listar_cargos(self):
        car_listar.Control(self).show()

    def listar_lotacoes(self):
        lot_listar.Control(self).show()

    def listar_servidores(self):
        ser_listar.Control(self).show()

    def listar_avaliacoes(self):
        av_listar.Control(self).show()

    def nova_lotacao(self):
        lot_cadastro.Cadastro(self).exec_()

    def novo_servidor(self):
        ser_cadastro.Cadastro(self).exec_()

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, User):
            if usuario.Usuario().getUsuario() is None:
                autenticacao.Autenticacao(self).exec_()
            elif instance == usuario.Usuario().getUsuario():
                usuario.Usuario().setUsuario(instance)
                self.carregar_titulo()

    def registros(self):
        ListarRegistro(self).show()
    
    def relatorio_intesticios_cumpridos(self):
        IntersticioRelatorioJanela(parent=self).show()
    
    def show(self) -> None:
        return super().showMaximized()

    def usuarios(self):
        user_listar.Listar(self).show()
    
    def util_criar_intersticios(self):
        CriarIntersticioDialogo(parent=self).exec_()
    
    def util_recalcular_intersticios(self):
        RecalcularIntersticioDialogo(parent=self).exec_()

