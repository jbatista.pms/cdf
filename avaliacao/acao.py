from datetime import datetime

from QtUtils import subwindowsbase, date
from QtUtils.colecoes import dialogos
from QtUtils.db import DataBase
from QtUtils.validadores import TextoEmMaisculo
from QtUtils.text import text_format_uppercase

from .views.acao import Ui_Dialog
from .models import Tramite


class Acao(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def accept(self):
        data = self.ui.data.date()
        data = datetime(year=data.year(), month=data.month(), day=data.day())
        observacao = self.ui.observacao.toPlainText()
        comprovante = self.ui.comprovante.text()
        data_excecao = False
        with DataBase().obter_database().atomic() as dba:
            for av in self.instance:
                tramite = Tramite(
                    data=data,
                    avaliacao=av,
                    observacao=observacao,
                    comprovante=comprovante,
                )
                ultimo_tramite = av.ultimo_tramite()
                if ultimo_tramite is None:
                    tramite.lotacao = av.servidor.lotacao
                    tramite.tipo = Tramite.ENVIO
                    tramite.save()
                else:
                    tramite.lotacao = ultimo_tramite.lotacao
                    tramite.tipo = not ultimo_tramite.tipo
                    if ultimo_tramite.data > data:
                        if data_excecao:
                            tramite.data = ultimo_tramite.data
                        elif dialogos.Confirmacao(self, text=
                                "Existe avaliação, cuja data do último tramite é superior "
                                "a data selecionada para o que está fazendo.\n"
                                "Para manter coerente o histórico de envios e recebimentos, "
                                "os tramites destas avaliações terão a última data possível."
                                "Deseja cotinuar?"
                            ).exec_():
                            data_excecao = True
                            tramite.data = ultimo_tramite.data
                        else:
                            dba.rollback()
                            return 
                    tramite.save()
                    ultimo_tramite.posterior = tramite
                    ultimo_tramite.save()
        return super().accept()

    def inicializar(self, *args, **kwargs):
        self.ui.comprovante.setValidator(TextoEmMaisculo(self))
        self.ui.data.setDate(date.datetime_qdate(datetime.today()))
        text_format_uppercase(self.ui.observacao)
        if kwargs['tipo'] == Tramite.ENVIO:
            self.setWindowTitle("Dados para envio")
            self.ui.data_label.setText("Data de envio:")
        else:
            self.setWindowTitle("Dados para recebimento")
            self.ui.data_label.setText("Data de recebimento:")