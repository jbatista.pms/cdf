from PySide2 import QtCore, QtGui, QtWidgets

from QtUtils import subwindowsbase, QtUtils
from QtUtils.db import DataBase
from QtUtils.historico.historico import Historico
from QtUtils.relatorio import Relatorio, RelatorioViewDialog
from QtUtils.text import text_format_uppercase

from avaliacao.mantenedor import MantenedorAvaliacao
from intersticio.mantenedor import IntersticioMantenedor

from . import tramite
from .colecoes import FATORES
from .views.avaliacao import Ui_Dialog
from .models import Tramite
from .utils import obter_pontuacao


class Avaliacao(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog
    fatores = ('assiduidade','conhecimento','comprometimento','iniciativa',
               'planejamento','pontualidade','qualidade','relacionamento',
               'trabalho','zelo',)
    graus = ('','I - Insuficiente','R - Regular','B - Bom','O - Ótimo')
    pontos = ('chefia', 'servidor', 'disciplina', 'evolucao', 'total', 'penalidade')

    def acao(self):
        ultimo_tramite = self.instance.ultimo_tramite()
        if ultimo_tramite:
            tipo = not ultimo_tramite.tipo
        else:
            tipo = Tramite.ENVIO
        tramite.Cadastro(
            parent=self, 
            instance=Tramite(avaliacao=self.instance, tipo=tipo),
            ultimo_tramite=ultimo_tramite,
        ).exec_()
    
    def calcular_pontos(self):
        pontuacao = {}
        for t in ['chefia', 'servidor']:
            graus = {}
            for f in FATORES:
                grau = getattr(self.ui,f+'_'+t).currentText().lower()
                if not grau:
                    graus.update({'falha': (f,t)})
                    break
                graus.update({f:grau})
            if not 'falha' in graus:
                pontuacao_parcial = obter_pontuacao(cargo=self.instance.servidor.cargo, graus=graus)
                pontuacao.update({t: pontuacao_parcial})
            else:
                pontuacao.update({t: graus})
        return pontuacao

    def carregar_tramite(self, indice, tramite):
        self.ui.tramites.setVerticalHeaderItem(
            indice, QtWidgets.QTableWidgetItem()
        )
        self.ui.tramites.verticalHeaderItem(indice).setText(
            str(tramite.id)
        )
        self.ui.tramites.item(indice,0).setText(" "+tramite.data_str()+" ")
        self.ui.tramites.item(indice,0).setTextAlignment(
            QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter
        )
        self.ui.tramites.item(indice,1).setText(" "+tramite.comprovante+" ")
        if tramite.tipo==Tramite.ENVIO:
            self.ui.tramites.item(indice,2).setText(" "+tramite.lotacao.descricao+" ")
        else:
            self.ui.tramites.item(indice,2).setText(" "+QtUtils.definicoes.nome_aplicacao+" ")

    def carregar_tramites(self):
        tramites = self.instance.ultimos_tramites(
            excluidos=self.ui.excluidos.isChecked(),
        ).objects()
        self.ui.tramites.setRowCount(tramites.count())
        for f in range(tramites.count()):
            self.ui.tramites.setVerticalHeaderItem(
                f,QtWidgets.QTableWidgetItem()
            )
            self.ui.tramites.setItem(f,0, QtWidgets.QTableWidgetItem())
            self.ui.tramites.setItem(f,1, QtWidgets.QTableWidgetItem())
            self.ui.tramites.setItem(f,2, QtWidgets.QTableWidgetItem())
            self.ui.tramites.setItem(f,3, QtWidgets.QTableWidgetItem())
            self.carregar_tramite(f, tramites[f])
        self.ui.tramites.resizeColumnsToContents()
        self.lista_dict_tramites = {str(t.id):t for t in tramites}

        if tramites:
            self.ultimo_tramite = tramites[0]
            self.ui.data.setText(self.ultimo_tramite.data_str())
            self.ui.comprovante.setText(self.ultimo_tramite.comprovante)
            if self.ultimo_tramite.tipo==Tramite.RECEBIMENTO:
                for i in self.pontos:
                    getattr(self.ui, 'pontos_'+i).setText(str(getattr(self.instance, 'pontos_'+i)))
                if self.instance.reavaliar and self.instance.pontos_total:
                    self.ui.reavaliar.setText(
                        '<p><span style=" font-weight:600; color:#ff0000;">Sim</span></p>'
                    )
                else:
                    self.ui.reavaliar.setText('Não')
                self.ui.formulario_pontos.setEnabled(True)
                self.ui.estado.setText("Devolvido em:")
                self.ui.acao.setText("Enviar")
                self.ui.local.setText(QtUtils.definicoes.nome_aplicacao)
                self.carregar_pontos()
            else:
                self.ui.local.setText(self.ultimo_tramite.lotacao.descricao)
                self.ui.estado.setText("Enviado em:")
                self.ui.acao.setText("Receber")
                self.ui.formulario_pontos.setEnabled(False)
        else:
            self.ultimo_tramite = None
            self.ui.data.setText("")
            self.ui.comprovante.setText("")
            self.ui.local.setText("")
            self.ui.estado.setText("Enviado em:")
            self.ui.acao.setText("Enviar")
            self.ui.formulario_pontos.setEnabled(False)

    def carregar_pontos(self):
        resultado = self.obter_pontos()
        resultado.pop('pontos_disciplina')
        resultado.pop('pontos_penalidade')
        resultado.pop('pontos_evolucao')
        if 'reavaliar' in resultado:
            if resultado.pop('reavaliar'):
                self.ui.reavaliar.setText(
                    '<p><span style=" font-weight:600; color:#ff0000;">Sim</span></p>'
                    )
            else:
                self.ui.reavaliar.setText('Não')
        else:
            self.ui.reavaliar.setText('')
        for k,v in resultado.items():
            if v:
                getattr(self.ui, k).setText(str(v))
            else:
                getattr(self.ui, k).setText('')
    
    def emitir_aviso(self, mensagem):
        aviso = (
            '<html><head/><body><p align="center"><span style=" color:#ff0004;">'
            '{aviso}</span></p></body></html>'
        ).format(aviso=mensagem)
        self.ui.aviso.show()
        self.ui.aviso.setText(aviso)

    def emitir_formulario(self):
        relatorio = Relatorio(
            parent=self, 
            objetos=[{
                'forms': [MantenedorAvaliacao(self.instance).dados_formulario()],
            }],
            template='avaliacao/documentos/formulario.odt',
        )
        relatorio.exec_()
        if relatorio.documento:
            RelatorioViewDialog(parent=self, relatorio=relatorio.documento).exec_()

    def historico(self):
        Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        text_format_uppercase(self.ui.observacao)
        data_inicial, data_final = self.instance.obter_prazos()
        self.ui.servidor.setText(self.instance.servidor.nome)
        self.ui.matricula.setText(self.instance.servidor.matricula)
        self.ui.cargo.setText(self.instance.servidor.cargo.nome)
        self.ui.lotacao.setText(self.instance.servidor.lotacao.descricao)
        self.ui.data_inicial.setText(data_inicial)
        self.ui.data_final.setText(data_final)
        self.ui.observacao.setPlainText(self.instance.observacao)
        self.ui.pontos_disciplina.setValidator(QtGui.QIntValidator(0,100, self))
        self.ui.pontos_penalidade.setValidator(QtGui.QIntValidator(0,500, self))
        self.ui.pontos_evolucao.setValidator(QtGui.QIntValidator(0,1000,self))
        # Preecher os graus
        for f in FATORES:
            for t in ['_chefia', '_servidor']:
                widget = getattr(self.ui,f+t)
                widget.addItems(self.graus)
                widget.setCurrentIndex(getattr(self.instance,f+t))
                QtCore.QObject.connect(
                    widget,
                    QtCore.SIGNAL("currentIndexChanged(QString)"), 
                    self.carregar_pontos
                )
        self.carregar_tramites()
        if self.instance.servidor.data_exclusao is None:
            self.ui.aviso.hide()
        else:
            self.ui.acao.setEnabled(False)
            self.ui.salvar.setEnabled(False)
        # Impedir edição caso período de avaliação seja indeterminado
        if self.instance.indeterminado:
            self.ui.acao.setEnabled(False)
            self.ui.salvar.setEnabled(False)
            self.emitir_aviso(mensagem='Período de avaliação indeterminado.')

    def obter_pontos(self):
        pontuacao = self.calcular_pontos()
        pontuacao_final = {}
        resultado = {'pontos_'+i: 0 for i in self.pontos}
        for k,v in pontuacao.items():
            widget = getattr(self.ui, 'pontos_'+k)
            if not 'falha' in v:
                soma = sum([v for k,v in v.items()])
                pontuacao_final.update({'pontos_'+k:soma})
        if len(pontuacao_final)>1:
            pontos_disciplina = self.ui.pontos_disciplina.text()
            pontos_disciplina = int(pontos_disciplina) if pontos_disciplina else 0
            pontos_penalidade = self.ui.pontos_penalidade.text()
            pontos_penalidade = int(pontos_penalidade) if pontos_penalidade else 0
            pontos_evolucao = self.ui.pontos_evolucao.text()
            pontos_evolucao = int(pontos_evolucao) if pontos_evolucao else 0
            pontos_total = pontuacao_final.get('pontos_chefia', 0)
            pontos_total += pontos_disciplina + pontos_evolucao - pontos_penalidade
            pontos_total_servidor = pontuacao_final.get('pontos_servidor', 0)
            pontos_total_servidor += pontos_disciplina + pontos_evolucao - pontos_penalidade
            reavaliar = pontuacao_final['pontos_servidor']*0.8>pontuacao_final['pontos_chefia']
            resultado.update({
                'pontos_disciplina': pontos_disciplina,
                'pontos_evolucao': pontos_evolucao,
                'pontos_penalidade': pontos_penalidade,
                'pontos_total': pontos_total,
                'pontos_total_servidor': pontos_total_servidor,
                'reavaliar': reavaliar
            })
        else:
            resultado.update({'reavaliar': False})
        resultado.update(pontuacao_final)
        return resultado

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, Tramite):
            resultado = self.obter_pontos()
            if 'reavaliar' in resultado:
                if resultado.pop('reavaliar'):
                    for t in ['_chefia', '_servidor']:
                        for f in FATORES:
                            getattr(self.ui,f+t).setCurrentIndex(0)
            self.carregar_tramites()
            self.salvar(fechar=False)

    def salvar(self, fechar=True):
        self.instance.observacao = self.ui.observacao.toPlainText()
        if self.ultimo_tramite and self.ultimo_tramite.tipo==Tramite.RECEBIMENTO:
            # Obter pontos
            for k,v in self.obter_pontos().items():
                setattr(self.instance, k, v)
            # Obter os graus
            for t in ['_chefia', '_servidor']:
                for f in FATORES:
                    grau = getattr(self.ui,f+t).currentIndex()
                    setattr(self.instance,f+t,grau)
        with DataBase().obter_database().atomic():
            self.instance.save()
            mant_inter = IntersticioMantenedor(self.instance.intersticio)
            mant_inter.apurar_aproveitamento()
            mant_inter.salvar()
        if fechar:
            self.enviar_sinal_atualizacao(self.instance)
            self.accept()

    def teste_unica(self, windows):
        if windows.instance.id == self.instance.id:
            return True
        return False

    def tramite(self, row, column):
        tramite_id = self.ui.tramites.verticalHeaderItem(row).text()
        instance = self.lista_dict_tramites[tramite_id]
        tramite.Cadastro(self, instance=instance).exec_()
