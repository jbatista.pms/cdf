from QtUtils.db import DataBase
from QtUtils import subwindowsbase
from QtUtils.text import text_format_uppercase

from .views.avaliacao_edicao_lote import Ui_Dialog


class Control(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def inicializar(self, *args, **kwargs):
        text_format_uppercase(self.ui.observacao)

    def accept(self):
        obs = self.ui.observacao.toPlainText()
        if obs != '':
            with DataBase().obter_database().atomic():
                for av in self.instance:
                    if av.observacao == '':
                        av.observacao = obs
                    else:
                        av.observacao += '\n' + obs
                    av.save()
        return super().accept()
