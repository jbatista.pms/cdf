from ComumComissoes.cargo.models import Cargo

PONTUACAO = {
    Cargo.NIVEL_SUPERIOR: {
        'assiduidade': {
            'i - insuficiente': 6, 'r - regular': 12, 'b - bom': 18, 'o - ótimo': 25
            },
        'conhecimento': {
            'i - insuficiente': 38, 'r - regular': 76, 'b - bom': 114, 'o - ótimo': 150
            },
        'comprometimento': {
            'i - insuficiente': 38, 'r - regular': 76, 'b - bom': 114, 'o - ótimo': 150
            },
        'iniciativa': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'planejamento': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'pontualidade': {
            'i - insuficiente': 6, 'r - regular': 12, 'b - bom': 18, 'o - ótimo': 25
            },
        'qualidade': {
            'i - insuficiente': 50, 'r - regular': 100, 'b - bom': 150, 'o - ótimo': 200
            },
        'relacionamento': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'trabalho': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'zelo': {
            'i - insuficiente': 12, 'r - regular': 24, 'b - bom': 36, 'o - ótimo': 50
            },
    },
    Cargo.NIVEL_MEDIO: {
        'assiduidade': {
            'i - insuficiente': 12, 'r - regular': 24, 'b - bom': 36, 'o - ótimo': 48
            },
        'conhecimento': {
            'i - insuficiente': 38, 'r - regular': 76, 'b - bom': 114, 'o - ótimo': 152
            },
        'comprometimento': {
            'i - insuficiente': 38, 'r - regular': 76, 'b - bom': 114, 'o - ótimo': 152
            },
        'iniciativa': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'planejamento': {
            'i - insuficiente': 12, 'r - regular': 24, 'b - bom': 36, 'o - ótimo': 48
            },
        'pontualidade': {
            'i - insuficiente': 12, 'r - regular': 24, 'b - bom': 36, 'o - ótimo': 48
            },
        'qualidade': {
            'i - insuficiente': 50, 'r - regular': 100, 'b - bom': 150, 'o - ótimo': 200
            },
        'relacionamento': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'trabalho': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'zelo': {
            'i - insuficiente': 12, 'r - regular': 24, 'b - bom': 36, 'o - ótimo': 48
            },
    },
	Cargo.NIVEL_FUNDAMENTAL: {					
        'assiduidade': {
            'i - insuficiente': 12, 'r - regular': 24, 'b - bom': 36, 'o - ótimo': 50
            },
        'conhecimento': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'comprometimento': {
            'i - insuficiente': 38, 'r - regular': 75, 'b - bom': 115, 'o - ótimo': 150
            },
        'iniciativa': {
            'i - insuficiente': 12, 'r - regular': 25, 'b - bom': 36, 'o - ótimo': 50
            },
        'planejamento': {
            'i - insuficiente': 12, 'r - regular': 25, 'b - bom': 36, 'o - ótimo': 50
            },
        'pontualidade': {
            'i - insuficiente': 12, 'r - regular': 25, 'b - bom': 36, 'o - ótimo': 50
            },
        'qualidade': {
            'i - insuficiente': 50, 'r - regular': 100, 'b - bom': 150, 'o - ótimo': 200
            },
        'relacionamento': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'trabalho': {
            'i - insuficiente': 25, 'r - regular': 50, 'b - bom': 75, 'o - ótimo': 100
            },
        'zelo': {
            'i - insuficiente': 38, 'r - regular': 75, 'b - bom': 115, 'o - ótimo': 150
            },
    }
}

FATORES = (
    'assiduidade','conhecimento','comprometimento','iniciativa',
    'planejamento','pontualidade','qualidade','relacionamento',
    'trabalho','zelo',
    )
