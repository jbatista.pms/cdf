from datetime import date, timedelta
from PySide2 import QtCore
from peewee import *

from ComumComissoes.lotacao.models import Lotacao
from ComumComissoes.lotacao import controles_uteis
from QtUtils import exportar, qt, subwindowsbase
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.relatorio import Relatorio, RelatorioViewSubWindows

from avaliacao import avaliacao
from avaliacao.mantenedor import MantenedorAvaliacao
from avaliacao.models import Avaliacao, Tramite
from servidor.models import Servidor

from . import avaliacao_edicao_lote
from .acao import Acao
from .views.listar import Ui_Form


class Control(subwindowsbase.Listar, controles_uteis.LotacaoSelecao):
    classe_ui = Ui_Form
    tipo_acao = None

    def acao(self):
        avaliacoes = self.avaliacoes_selecionadas()
        if avaliacoes:
            if Acao(self, instance=avaliacoes, tipo=self.tipo_acao).exec_():
                self.atualizar()

    def avaliacoes_selecionadas(self):
        avaliacoes = []
        for i in range(self.ui.avaliacoes.rowCount()):
            if self.ui.avaliacoes.item(i,1).checkState() == QtCore.Qt.Checked:
                avaliacoes.append(
                    self.avaliacoes_dicionario[
                        int(self.ui.avaliacoes.item(i,0).text())
                        ]
                    )
        if not avaliacoes:
            Alerta(self, text='Não há avaliações selecionadas').exec_()
        return avaliacoes

    def gerar_formularios(self):
        avaliacoes = self.avaliacoes_selecionadas()
        if avaliacoes:
            formularios = []
            for av in avaliacoes:
                dados = MantenedorAvaliacao(av).dados_formulario()
                dados.update({'quebrar': True})
                formularios.append(dados)
            formularios[-1]['quebrar'] = False
            relatorio = Relatorio(
                parent=self, 
                objetos=[{'forms': formularios}],
                template='avaliacao/documentos/formulario.odt',
            )
            relatorio.exec_()
            if relatorio.documento:
                RelatorioViewSubWindows(self, relatorio.documento).show()

    def inicializar(self, *args, **kwargs):
        self.lista_dict_av = {}
        self.carregar_lotacoes()
        self.ui.data_base.setDate(datetime_qdate(date.today()))
        self.headerH = self.ui.avaliacoes.horizontalHeader()
        self.headerH.setSectionHidden(0,True)
        self.headerH.setDefaultAlignment(QtCore.Qt.AlignCenter)

    def listar(self):
        avaliacoes = Avaliacao.select2().where(
            Avaliacao.indeterminado==False,
        )

        if self.ui.base.isChecked():
            avaliacoes = avaliacoes.where(
                Avaliacao.data_final<=qdate_datetime(
                    self.ui.data_base.date()
                )
            )

        if self.ui.aberto.isChecked():
            avaliacoes = avaliacoes.where(Avaliacao.id.not_in(self.tramites_p_avaliacao()))
        else:
            avaliacoes = avaliacoes.join(Tramite, on=(Avaliacao.id == Tramite.avaliacao))
            
        if self.ui.enviado.isChecked():
            avaliacoes = avaliacoes.where(
                (Tramite.posterior.is_null(True)) & 
                (Tramite.origem.is_null(True)) &
                (Tramite.data_exclusao.is_null(True)) &
                (Tramite.tipo==Tramite.ENVIO)
                )
        elif self.ui.atrasado.isChecked():
            avaliacoes = avaliacoes.where(
                (Tramite.posterior.is_null(True)) & 
                (Tramite.origem.is_null(True)) &
                (Tramite.data_exclusao.is_null(True)) &
                (Tramite.tipo==Tramite.ENVIO)
                )
            if self.ui.base.isChecked():
                data_atraso = qdate_datetime(self.ui.data_base.date())-timedelta(days=15)
                avaliacoes = avaliacoes.where(Tramite.data<=data_atraso)
        elif self.ui.devolvido.isChecked():
            avaliacoes = avaliacoes.where(
                (Tramite.posterior.is_null(True)) & 
                (Tramite.origem.is_null(True)) &
                (Tramite.data_exclusao.is_null(True)) &
                (Tramite.tipo==Tramite.RECEBIMENTO) &
                (Avaliacao.pontos_total==0)
                )
        elif self.ui.reavaliar.isChecked():
            avaliacoes = avaliacoes.where(
                (Tramite.posterior.is_null(True)) & 
                (Tramite.origem.is_null(True)) &
                (Tramite.data_exclusao.is_null(True)) &
                (Tramite.tipo==Tramite.RECEBIMENTO) &
                (Avaliacao.reavaliar==True)
                )
        elif self.ui.finalizado.isChecked():
            avaliacoes = avaliacoes.where(
                (Tramite.posterior.is_null(True)) & 
                (Tramite.origem.is_null(True)) &
                (Tramite.data_exclusao.is_null(True)) &
                (Tramite.tipo==Tramite.RECEBIMENTO) &
                (Avaliacao.reavaliar==False) &
                (Avaliacao.pontos_total>0)
                )

        lotacao = self.lotacao_selecionada()
        sublotacao_indice = self.sublotacao_indice()
        
        if self.ui.aberto.isChecked():
            if sublotacao_indice>1:
                avaliacoes = avaliacoes.where(
                    Servidor.lotacao==self.sublotacao_selecionada()
                    )
            elif sublotacao_indice==0:
                avaliacoes = avaliacoes.join(Lotacao)
                avaliacoes = avaliacoes.where((Lotacao.pai==lotacao) | (Servidor.lotacao==lotacao))
            elif lotacao:
                avaliacoes = avaliacoes.where(Servidor.lotacao==lotacao)
        else:
            if sublotacao_indice>1:
                avaliacoes = avaliacoes.where(Tramite.lotacao==self.sublotacao_selecionada())
            elif sublotacao_indice==0:
                avaliacoes = avaliacoes.join(Lotacao)
                avaliacoes = avaliacoes.where((Lotacao.pai==lotacao) | (Tramite.lotacao==lotacao))
            elif lotacao:
                avaliacoes = avaliacoes.where(Tramite.lotacao==lotacao)
        
        avaliacoes = avaliacoes.where(Servidor.inativo==False)

        self.lista_dict_av = {str(a.id):a for a in avaliacoes.objects()}
        return avaliacoes

    def atualizar(self, *args, **kwargs):
        self.verificar_estado()
        avaliacoes = self.listar()
        self.avaliacoes_dicionario = dict([(i.id,i) for i in avaliacoes])
        avaliacoes_count = len(avaliacoes)
        self.ui.total_avaliacoes.setText(
            "%i avaliações encontradas" % avaliacoes_count
            )
        self.ui.avaliacoes.setSortingEnabled(False)
        self.ui.avaliacoes.setRowCount(avaliacoes_count)
        for f in range(avaliacoes_count):
            av = avaliacoes[f]

            item = qt.QTableWidgetItem()
            item.setText(str(av.id))
            self.ui.avaliacoes.setItem(f,0, item)

            item = qt.QTableWidgetItem()
            item.setFlags(
                QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled
                )
            item.setCheckState(QtCore.Qt.Unchecked)
            self.ui.avaliacoes.setItem(f,1, item)

            item = qt.QTableWidgetItem()
            item.setText(av.strftime('data_final'))
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.avaliacoes.setItem(f,3, item)

            ultimo_tramite = av.ultimo_tramite()
            if ultimo_tramite:
                lotacao = ultimo_tramite.lotacao
            else:
                lotacao = av.servidor.lotacao

            item = qt.QTableWidgetItem()
            item.setText(lotacao.pai.nome if lotacao.pai else lotacao.nome)
            self.ui.avaliacoes.setItem(f,4, item)

            item = qt.QTableWidgetItem()
            item.setText(lotacao.nome if lotacao.pai else "")
            self.ui.avaliacoes.setItem(f,5, item)

            item = qt.QTableWidgetItem()
            item.setText(av.servidor.nome)
            self.ui.avaliacoes.setItem(f,2, item)
        self.ui.avaliacoes.setSortingEnabled(True)
        self.headerH.resizeSection(1,15)
        self.ui.avaliacoes.resizeColumnsToContents()

    def exportar(self):
        exportar.exportar(parent=self, query=self.listar())

    def abrir_avaliacao(self, row, column):
        id_av = self.ui.avaliacoes.item(row,0).text()
        instance = self.lista_dict_av[id_av]
        if avaliacao.Avaliacao(self, instance=instance).exec_():
            self.atualizar()

    def selecionar_todos(self):
        for i in range(self.ui.avaliacoes.rowCount()):
            self.ui.avaliacoes.item(i,1).setCheckState(QtCore.Qt.Checked)

    def deselecionar_todos(self):
        for i in range(self.ui.avaliacoes.rowCount()):
            self.ui.avaliacoes.item(i,1).setCheckState(QtCore.Qt.Unchecked)

    def reverter_selecao(self):
        for i in range(self.ui.avaliacoes.rowCount()):
            item = self.ui.avaliacoes.item(i,1)
            if item.checkState() == QtCore.Qt.Checked:
                item.setCheckState(QtCore.Qt.Unchecked)
            else:
                item.setCheckState(QtCore.Qt.Checked)

    def edicao_lote(self):
        avaliacoes = self.avaliacoes_selecionadas()
        if avaliacoes:
            avaliacao_edicao_lote.Control(self, instance=avaliacoes).exec_()

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, (Servidor, Lotacao)):
            self.atualizar()
        elif isinstance(instance, Tramite):
            if str(instance.avaliacao.id) in self.lista_dict_av:
                self.atualizar()
        elif isinstance(instance, Avaliacao):
            if str(instance.id) in self.lista_dict_av:
                self.atualizar()
        elif isinstance(instance, Lotacao):
            self.carregar_lotacoes()
            self.limpar_lista()
    
    def limpar_lista(self):
        self.ui.avaliacoes.setRowCount(0)

    def tramites_p_avaliacao(self):
        return Tramite.select(Avaliacao.id).join(Avaliacao).where(
                (Tramite.data_exclusao.is_null(True)) & (Tramite.origem.is_null(True))
            ).group_by(Avaliacao)
    
    def verificar_estado(self):
        if self.ui.aberto.isChecked() or self.ui.reavaliar.isChecked():
            self.ui.acao.setText("Enviar formulários")
            self.ui.acao.setEnabled(True)
            self.tipo_acao = Tramite.ENVIO
        elif self.ui.enviado.isChecked() or self.ui.reavaliar.isChecked():
            self.ui.acao.setText("Receber formulários")
            self.ui.acao.setEnabled(True)
            self.tipo_acao = Tramite.RECEBIMENTO
        else:
            self.ui.acao.setEnabled(False)