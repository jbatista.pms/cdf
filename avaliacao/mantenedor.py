from datetime import timedelta
from loguru import logger

from QtUtils.prazos import Prazo

from avaliacao.models import Avaliacao
from evento.models import Evento
from falta.models import Falta


class MantenedorAvaliacao(object):
    __av = None

    def __init__(self, avaliacao):
        self.__av = avaliacao

    def __estimar_datas(self):
        logger.debug(f"Estimando prazo de {self.__av}")
        # Determinar data inicial e data base
        intersticio = self.__av.intersticio
        if self.__av.anterior:
            self.__av.data_base = self.__av.anterior.data_base
            # Avaliação se inicia um dia após a anterior
            self.__av.data_inicial = self.__av.anterior.data_final + timedelta(days=1)
        else:
            self.__av.data_base = intersticio.data_inicial
            self.__av.data_inicial = intersticio.data_inicial
        logger.debug(f"Data inicial estimada para {self.__av.data_inicial}")
        # Estimando data final um ano após a data inicial
        ordem = self.__av.data_inicial.year - self.__av.data_base.year + 1
        prorr = Prazo(self.__av.data_base)
        self.__av.data_final = prorr.prorrogar(ordem * 12)
        logger.debug(f"Data final estimada para {self.__av.data_final}")
    
    def __postergar_calculo_eventos(self, eventos):
        if not eventos:
            return 0, []
        eventos = sorted(eventos, key=lambda ev: ev['ev'].data_inicial)
        # Obter soma de dias por tipo de evento
        eventos_considerados = [] # Eventos considerados para suspensão
        total_dias_considerados = 0 # Dias total considerados para suspensão
        lista_acompanhamento = []
        lista_tratamento_saude = []
        total_acompanhamento = 0
        total_tratamento_saude = 0
        for i in eventos:
            logger.debug(f"Analisando evento {i['ev']}")
            if i['ev'].tratamento_saude:
                logger.debug(f"{i['total']} dias de tratamento de saúde encontrado.")
                total_tratamento_saude += i['total']
                lista_tratamento_saude.append(i['ev'])
            elif i['ev'].acompanhamento:
                total_acompanhamento += i['total']
                lista_acompanhamento.append(i['ev'])
            else:
                eventos_considerados.append(i['ev'])
                total_dias_considerados += i['total']
        # Verificar se total de dias por tipo de evento leva a suspensão
        # > Licença para tratamento de saúde
        if total_tratamento_saude > 180:
            total_dias_considerados += total_tratamento_saude
            eventos_considerados += lista_tratamento_saude
        # > Licença para tratamento de saúde
        if total_acompanhamento > 90:
            total_dias_considerados += total_acompanhamento
            eventos_considerados += lista_acompanhamento
        return total_dias_considerados, eventos_considerados
    
    def __postergar_obter_eventos(self):
        """ Obter eventos para posterior análise e suspensão de prazo de avaliações"""
        # Eventos com períodos abrangidos pelo período de avaliação
        ev_inteiros = Evento.select2().where(
            (Evento.data_inicial>=self.__av.data_inicial) & 
            (Evento.data_final<=self.__av.data_final) &
            (Evento.servidor==self.__av.intersticio.servidor)
        )
        # Lista de eventos retornáveis no formato (instância, dias considerados)
        eventos = [{'ev': ev, 'total': ev.total} for ev in ev_inteiros]
        # Eventos que abrangem parte do período de avaliação no início
        ev_inicio = Evento.select2().where(
            (Evento.data_inicial<self.__av.data_inicial) & 
            (Evento.data_final>=self.__av.data_inicial) &
            (Evento.servidor==self.__av.intersticio.servidor)
        )
        for ev in ev_inicio:
            eventos.append({'ev': ev, 'total': (ev.data_final-self.__av.data_inicial).days+1})
        # Eventos que abrangem parte do período de avaliação no final
        ev_final = Evento.select2().where(
            (Evento.data_inicial<=self.__av.data_final) & 
            (Evento.data_final>self.__av.data_final) &
            (Evento.servidor==self.__av.intersticio.servidor)
        )
        for ev in ev_final:
            eventos.append({'ev': ev, 'total': (self.__av.data_final-ev.data_inicial).days+1})
        # Ordenar e retornar lista de eventos calculados
        logger.debug(f"{len(eventos)} eventos encontrados no período de avaliação.")
        return self.__postergar_calculo_eventos(eventos=eventos)
    
    def __postergar_obter_faltas(self):
        faltas = Falta.select2().where(
            (Falta.data_efeito>=self.__av.data_inicial) & 
            (Falta.data_efeito<=self.__av.data_final) &
            (Falta.servidor==self.__av.intersticio.servidor)
        )
        return sum([falta.dias for falta in faltas]), list(faltas)
    
    @property
    def avaliacao(self):
        return self.__av
    
    def calcular_prazo(self):
        # TODO: Impedir alteração de período de avaliação quando houver tramitação do formulário
        # if avaliacao.ultimo_tramite():
        #    return
        # Determinar se prazo final é indeterminado com base em eventos
        logger.info(f"Calculando prazo de {self.__av}.")
        self.__av.indeterminado = Evento.ha_indeterminados(
            data_limite=self.__av.data_final,
            servidor=self.__av.servidor,
        )
        if self.__av.indeterminado:
            self.__av.save()
            return
        # Estimando data final um ano após a data inicial
        self.__estimar_datas()
        # Postegar data final com base em eventos e faltas
        # O laço terminará quando não houver mais dias a serem acrescentados
        # no cálculo de suspensão
        eventos_considerados = [] # Eventos considerados para suspensão
        faltas_consideradas = [] # Faltas consideradas para suspensão
        total_dias_anterior = 0 # Total de dias obtido na verificação anterior
        data_final = self.__av.data_final
        self.__av.data_final = self.__av.data_inicial
        while data_final > self.__av.data_final:
            self.__av.data_final = data_final
            t_dias_ev, eventos_considerados = self.__postergar_obter_eventos()
            t_dias_fa, faltas_consideradas = self.__postergar_obter_faltas()
            total_dias = t_dias_ev + t_dias_fa # Total de dias obtidos na verificação atual
            total_dias -= total_dias_anterior
            data_final += timedelta(days=total_dias)
            total_dias_anterior = t_dias_ev + t_dias_fa
            if data_final > self.__av.data_final:
                logger.debug(f"Data final postegarda de {self.__av.data_final} para {data_final}.")

        # Acrescentar eventos em avaliação para futuras consultas
        for ev in eventos_considerados:
            self.__av.eventos.add(ev, clear_existing=True)
        # Acrescentar faltas em avaliação para futuras consultas
        for fal in faltas_consideradas:
            if fal.avaliacao != self.__av:
                fal.avaliacao = self.__av
                fal.save()
        # Defindir data base para estimativa de período de avaliações seguintes
        if eventos_considerados or faltas_consideradas:
            self.__av.data_base = self.__av.data_final + timedelta(days=1)
    
    @classmethod
    def criar(cls, intersticio, anterior, ordem):
        mant_av = cls(Avaliacao(intersticio=intersticio, anterior=anterior))
        mant_av.__estimar_datas()
        mant_av.salvar()
        return mant_av
    
    def dados_formulario(self):
        return {
            'cargo': self.__av.intersticio.servidor.cargo.nome,
            'data_final': self.__av.data_final.strftime('%d/%m/%Y'),
            'data_inicial': self.__av.data_inicial.strftime('%d/%m/%Y'),
            'lotacao': self.__av.intersticio.servidor.lotacao.descricao,
            'matricula': self.__av.intersticio.servidor.matricula,
            'nome': self.__av.intersticio.servidor.nome,
            'quebrar': False,
            'qtd': [True, False]
        }
    
    def ordenar_datas(self, ordem):
        if self.__av.anterior:
            self.__av.data_inicial = self.__av.anterior.data_final + timedelta(days=1)
        else:
            self.__av.data_inicial = self.__av.intersticio.data_inicial
        prorr = Prazo(self.__av.intersticio.data_inicial)
        self.__av.data_final = prorr.prorrogar(ordem * 12)
        self.__av.data_base = self.__av.intersticio.data_inicial

    def salvar(self):
        self.__av.save()
        self.__av._original = self.__av.__data__.copy()