from enum import Enum
from peewee import *

from ComumComissoes.lotacao.models import Lotacao
from QtUtils.user.models import User
from QtUtils.historico.models import Historico

from intersticio.modelos import Intersticio
from servidor.models import Servidor


class Grau(Enum):
    INSUFICIENTE = 'I - Insuficiente'
    REGULAR = 'R - Regular'
    BOM = 'B - Bom'
    OTIMO = 'O - Ótimo'


class Avaliacao(Historico):
    ESTADO_APURAR = 'Apurar'
    ESTADO_ENVIADO = 'Enviado'
    ESTADO_ENVIADO_REAVALIAR = 'Enviado para reavaliar'
    ESTADO_FINALIZADO = 'Finalizado'
    ESTADO_NENHUM = ''
    ESTADO_REAVALIAR = 'Reavaliar'
    GRAU_CHOICES = (
        (0, ''),
        (1, 'I - Insuficiente'),
        (2, 'R - Regular'),
        (3, 'B - Bom'),
        (4, 'O - Ótimo'),
    )

    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    user = ForeignKeyField(User, null=True, db_column='user')
    intersticio = ForeignKeyField(
        Intersticio,
        related_name='avaliacoes',
        on_delete='CASCADE',
        )
    data_inicial = DateField()
    data_final = DateField()
    data_base = DateField()
    anterior = ForeignKeyField(
        'self',
        related_name='posterior',
        null=True,
        on_delete='SET NULL',
        )
    observacao = TextField(null=True, default='')
    indeterminado = BooleanField(default=False)
    # Fatores chefia
    assiduidade_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    conhecimento_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    comprometimento_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    iniciativa_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    planejamento_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    pontualidade_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    qualidade_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    relacionamento_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    trabalho_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    zelo_chefia = IntegerField(choices=GRAU_CHOICES,default=0)
    # Fatores servidor
    assiduidade_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    conhecimento_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    comprometimento_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    iniciativa_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    planejamento_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    pontualidade_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    qualidade_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    relacionamento_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    trabalho_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    zelo_servidor = IntegerField(choices=GRAU_CHOICES,default=0)
    # Pontuação
    pontos_chefia = IntegerField(default=0)
    pontos_servidor = IntegerField(default=0)
    pontos_disciplina = IntegerField(default=0)
    pontos_penalidade = IntegerField(default=0)
    pontos_evolucao = IntegerField(default=0)
    pontos_total = IntegerField(default=0)
    reavaliar = BooleanField(default=False)

    def __str__(self):
        return (
            f"Avaliação de {self.intersticio.servidor} "
            f"no período de {self.data_inicial} a {self.data_final}"
        )
    
    def estado(self):
        tramite = self.ultimo_tramite()
        if tramite:
            if tramite.tipo == Tramite.ENVIO:
                if self.reavaliar:
                    return self.ESTADO_ENVIADO_REAVALIAR
                return self.ESTADO_ENVIADO
            else:
                if self.reavaliar:
                    return self.ESTADO_REAVALIAR
                elif self.pontos_total:
                    return self.ESTADO_FINALIZADO
                return self.ESTADO_APURAR
        return self.ESTADO_NENHUM
        
    def exportar_extras():
        return [
            'intersticio',
            'intersticio.servidor', 
            'intersticio.servidor.lotacao', 
            'intersticio.servidor.cargo',
            ['ultimo_tramite', Tramite],
            ]
    
    def exportar_ignorar():
        return Historico.exportar_ignorar() + ['indeterminado','data_base']
    
    def ignorar_campos_mudancas():
        return Historico.ignorar_campos_mudancas() + ['data_base','anterior','intersticio']
    
    def obter_prazos(self):
        if self.indeterminado:
            if self.intersticio.anterior and self.intersticio.anterior.indeterminado:
                return "Indeterminado", "Indeterminado"
            elif self.anterior and self.anterior.indeterminado:
                return "Indeterminado", "Indeterminado"
            else:
                return self.strftime('data_inicial'), "Indeterminado"
        else:
            return self.strftime('data_inicial'), self.strftime('data_final')
    
    def pronto_para_intersticio(self):
        """ Verifica se avaliação pode ou não ser incluida no resultado final do interstício """
        return self.estado() in [self.ESTADO_FINALIZADO, self.ESTADO_REAVALIAR]

    def select2(excluidos=False, *args, **kwargs):
        return Avaliacao.select(*args, **kwargs).join(Intersticio).join(Servidor).where(
            (Avaliacao.origem.is_null(True)) &
            (Avaliacao.data_exclusao.is_null(not excluidos)) &
            (Servidor.data_exclusao.is_null(True))
        ).order_by(Avaliacao.data_inicial)

    @property
    def servidor(self):
        return self.intersticio.servidor
                
    def ultimo_tramite(self):
        return self.ultimos_tramites().first()
    
    def ultimos_tramites(self, excluidos=False):
        if excluidos:
            condicao = (Tramite.data.is_null(False)) & (Tramite.origem.is_null(True))
        else:
            condicao = (
                (Tramite.data.is_null(False)) & 
                (Tramite.data_exclusao.is_null(True)) & 
                (Tramite.origem.is_null(True))
            )
        return self.tramites.select().where(condicao).order_by(
            Tramite.data_criacao.desc(),
            Tramite.id.desc(),
        )


class Tramite(Historico):
    ENVIO = True
    RECEBIMENTO = False

    tramite_escolhas = {
        ENVIO: 'Envio',
        RECEBIMENTO: 'Recebimento',
    }

    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    posterior = ForeignKeyField('self', related_name='anterior', null=True)
    avaliacao = ForeignKeyField(Avaliacao, related_name='tramites')
    comprovante = CharField(default="")
    data = DateTimeField()
    data_exclusao = DateField(null=True)
    lotacao = ForeignKeyField(Lotacao, null=True, related_name='tramites')
    observacao = CharField(default="")
    tipo = BooleanField(choices=tramite_escolhas)

    def __str__(self):
        return "Tramite"
    
    def data_str(self):
        if self.data:
            return self.data.strftime("%d/%m/%Y")
        return ""
    
    def ignorar_campos_mudancas():
        return Historico.ignorar_campos_mudancas() + ['posterior','avaliacao', 'lotacao', 'tipo']
    
    def exportar_ignorar():
        return Historico.exportar_ignorar() + ['posterior', 'data_exclusao']
    
    def select2(*args, **kwargs):
        return Tramite.select(*args, **kwargs).where(
            Tramite.origem.is_null(True)
        ).order_by(Tramite.data.desc())