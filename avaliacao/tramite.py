from datetime import datetime

from QtUtils import subwindowsbase
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.colecoes import dialogos
from QtUtils.db import DataBase
from QtUtils.text import text_format_uppercase
from QtUtils.historico.historico import Historico
from QtUtils.validadores import TextoEmMaisculo

from .models import Tramite
from .views.tramite import Ui_Dialog


class Cadastro(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def excluir(self):
        if dialogos.Confirmacao(text="Excluir o tramite:").exec_():
            with DataBase().obter_database().atomic():
                self.instance.data_exclusao = datetime.today()
                anterior = self.instance.anterior.select().first()
                if anterior:
                    anterior.posterior = None
                    anterior.save()
                self.instance.save()
                self.enviar_sinal_atualizacao(self.instance)
            self.accept()

    def historico(self):
        Historico(self, instance=self.instance).exec_()

    def inicializar(self, *args, **kwargs):
        text_format_uppercase(self.ui.observacao)
        self.ui.comprovante.setValidator(TextoEmMaisculo(self))
        if self.instance.id is None:
            self.ui.data.setDate(datetime_qdate(datetime.today()))
            self.ui.historico.hide()
            self.ui.excluir.hide()
            self.ultimo_tramite = kwargs['ultimo_tramite']
        else:
            self.ui.comprovante.setText(self.instance.comprovante)
            self.ui.data.setDate(datetime_qdate(self.instance.data))
            self.ui.observacao.setPlainText(self.instance.observacao)
            if self.instance.posterior:
                self.ui.excluir.hide()
                self.ui.data.setMaximumDate(datetime_qdate(self.instance.posterior.data))
            self.ultimo_tramite = self.instance.anterior.select().first()
        
        if self.instance.tipo==Tramite.ENVIO:
            self.setWindowTitle("Envio")
        else:
            self.setWindowTitle("Recebimento")
        # Limitar data
        if self.ultimo_tramite:
            self.ui.data.setMinimumDate(datetime_qdate(self.ultimo_tramite.data))
        # Excluído
        if (self.instance.data_exclusao != None or 
            self.instance.avaliacao.servidor.data_exclusao != None):
            self.ui.excluir.setEnabled(False)
            self.ui.salvar.setEnabled(False)
            if self.instance.data_exclusao != None:
                self.ui.aviso.setText(
                    '<p align="center"><span style=" color:#ff0000;">Excluído.</span></p>'
                )
            elif self.instance.avaliacao.servidor.data_exclusao != None:
                self.ui.aviso.setText(
                    '<p align="center"><span style=" color:#ff0000;">'
                    'Servidor excluído.'
                    '</span></p>'
                )
        else:
            self.ui.aviso.hide()
    
    def salvar(self):
        self.instance.data = qdate_datetime(self.ui.data.date())
        self.instance.comprovante = self.ui.comprovante.text()
        self.instance.observacao = self.ui.observacao.toPlainText().upper()
        if self.ultimo_tramite:
            self.instance.lotacao =  self.ultimo_tramite.lotacao
            self.ultimo_tramite.posterior = self.instance
        else:
            self.instance.lotacao = self.instance.avaliacao.servidor.lotacao
        with DataBase().obter_database().atomic():
            self.instance.save()
            if self.ultimo_tramite:
                self.ultimo_tramite.save()
        self.enviar_sinal_atualizacao(self.instance)
        self.accept()
