from .colecoes import PONTUACAO, FATORES

def calcular_pontos(cargo, graus):
    return sum([v for k,v in obter_pontuacao(cargo=cargo, graus=graus).items()])

def obter_pontuacao(cargo, graus):
    pontuacao_cargo = PONTUACAO[cargo.nivel_escolaridade]
    pontuacao_avaliacao = {f:0 for f in FATORES}
    for k,v in graus.items():
        pontuacao_avaliacao[k] = pontuacao_cargo[k][v]
    return pontuacao_avaliacao