# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'acao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(352, 231)
        Dialog.setMinimumSize(QSize(352, 231))
        Dialog.setMaximumSize(QSize(352, 231))
        self.gridLayout_2 = QGridLayout(Dialog)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.data_label = QLabel(Dialog)
        self.data_label.setObjectName(u"data_label")
        self.data_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.data_label, 0, 0, 1, 1)

        self.data = QDateEdit(Dialog)
        self.data.setObjectName(u"data")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.data.sizePolicy().hasHeightForWidth())
        self.data.setSizePolicy(sizePolicy)
        self.data.setCalendarPopup(True)

        self.gridLayout.addWidget(self.data, 0, 1, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.comprovante = QLineEdit(Dialog)
        self.comprovante.setObjectName(u"comprovante")

        self.gridLayout.addWidget(self.comprovante, 1, 1, 1, 1)


        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label_2)

        self.observacao = QTextEdit(Dialog)
        self.observacao.setObjectName(u"observacao")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.observacao.sizePolicy().hasHeightForWidth())
        self.observacao.setSizePolicy(sizePolicy1)
        self.observacao.setTabChangesFocus(True)

        self.verticalLayout.addWidget(self.observacao)


        self.gridLayout_2.addLayout(self.verticalLayout, 1, 0, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 0, -1, -1)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(Dialog)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout_2.addLayout(self.horizontalLayout, 2, 0, 1, 1)


        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)
        self.pushButton_2.released.connect(Dialog.accept)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dados para envio", None))
        self.data_label.setText(QCoreApplication.translate("Dialog", u"Data de envio:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Comprovante:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Oberserva\u00e7\u00e3o/Informa\u00e7\u00e3o:", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

