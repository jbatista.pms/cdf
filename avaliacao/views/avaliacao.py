# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'avaliacao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.setWindowModality(Qt.ApplicationModal)
        Dialog.resize(523, 572)
        Dialog.setMinimumSize(QSize(523, 572))
        Dialog.setMaximumSize(QSize(523, 572))
        self.gridLayout_4 = QGridLayout(Dialog)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.botoes = QFrame(Dialog)
        self.botoes.setObjectName(u"botoes")
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.botoes.sizePolicy().hasHeightForWidth())
        self.botoes.setSizePolicy(sizePolicy)
        self.botoes.setLayoutDirection(Qt.RightToLeft)
        self.botoes.setFrameShape(QFrame.StyledPanel)
        self.botoes.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.botoes)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)

        self.salvar = QPushButton(self.botoes)
        self.salvar.setObjectName(u"salvar")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy1)

        self.horizontalLayout_2.addWidget(self.salvar)

        self.pushButton = QPushButton(self.botoes)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout_2.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(self.botoes)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout_2.addWidget(self.pushButton_2)

        self.fechar = QPushButton(self.botoes)
        self.fechar.setObjectName(u"fechar")
        sizePolicy1.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy1)

        self.horizontalLayout_2.addWidget(self.fechar)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.gridLayout_4.addWidget(self.botoes, 12, 0, 1, 2)

        self.aviso = QLabel(Dialog)
        self.aviso.setObjectName(u"aviso")

        self.gridLayout_4.addWidget(self.aviso, 11, 0, 1, 2)

        self.tabWidget = QTabWidget(Dialog)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_3 = QGridLayout(self.tab)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.widget2 = QWidget(self.tab)
        self.widget2.setObjectName(u"widget2")
        self.verticalLayout = QVBoxLayout(self.widget2)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(-1, 1, -1, 1)
        self.groupBox2 = QGroupBox(self.widget2)
        self.groupBox2.setObjectName(u"groupBox2")
        sizePolicy2 = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.groupBox2.sizePolicy().hasHeightForWidth())
        self.groupBox2.setSizePolicy(sizePolicy2)
        self.gridLayout_2 = QGridLayout(self.groupBox2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.widget1 = QWidget(self.groupBox2)
        self.widget1.setObjectName(u"widget1")
        self.gridLayout_7 = QGridLayout(self.widget1)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(-1, 1, -1, -1)
        self.label_7 = QLabel(self.widget1)
        self.label_7.setObjectName(u"label_7")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy3)
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_7.addWidget(self.label_7, 0, 0, 1, 1)

        self.label_6 = QLabel(self.widget1)
        self.label_6.setObjectName(u"label_6")
        sizePolicy3.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy3)
        self.label_6.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_7.addWidget(self.label_6, 1, 0, 1, 1)

        self.matricula = QLabel(self.widget1)
        self.matricula.setObjectName(u"matricula")
        sizePolicy4 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy4)
        self.matricula.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.matricula.setWordWrap(True)

        self.gridLayout_7.addWidget(self.matricula, 1, 1, 1, 1)

        self.cargo = QLabel(self.widget1)
        self.cargo.setObjectName(u"cargo")
        sizePolicy4.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy4)
        self.cargo.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.cargo.setWordWrap(True)

        self.gridLayout_7.addWidget(self.cargo, 2, 1, 1, 1)

        self.servidor = QLabel(self.widget1)
        self.servidor.setObjectName(u"servidor")
        sizePolicy4.setHeightForWidth(self.servidor.sizePolicy().hasHeightForWidth())
        self.servidor.setSizePolicy(sizePolicy4)
        self.servidor.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.servidor.setWordWrap(True)

        self.gridLayout_7.addWidget(self.servidor, 0, 1, 1, 1)

        self.label_5 = QLabel(self.widget1)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_7.addWidget(self.label_5, 2, 0, 1, 1)

        self.label_8 = QLabel(self.widget1)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_7.addWidget(self.label_8, 3, 0, 1, 1)

        self.lotacao = QLabel(self.widget1)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy4.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy4)
        self.lotacao.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.lotacao.setWordWrap(True)

        self.gridLayout_7.addWidget(self.lotacao, 3, 1, 1, 1)


        self.gridLayout_2.addWidget(self.widget1, 0, 0, 1, 2)


        self.verticalLayout.addWidget(self.groupBox2)

        self.groupBox1 = QGroupBox(self.widget2)
        self.groupBox1.setObjectName(u"groupBox1")
        sizePolicy5 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.groupBox1.sizePolicy().hasHeightForWidth())
        self.groupBox1.setSizePolicy(sizePolicy5)
        self.gridLayout = QGridLayout(self.groupBox1)
        self.gridLayout.setObjectName(u"gridLayout")
        self.data_final = QLabel(self.groupBox1)
        self.data_final.setObjectName(u"data_final")
        sizePolicy.setHeightForWidth(self.data_final.sizePolicy().hasHeightForWidth())
        self.data_final.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.data_final, 0, 5, 1, 1)

        self.data_inicial = QLabel(self.groupBox1)
        self.data_inicial.setObjectName(u"data_inicial")
        sizePolicy.setHeightForWidth(self.data_inicial.sizePolicy().hasHeightForWidth())
        self.data_inicial.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.data_inicial, 0, 1, 1, 1)

        self.label_2 = QLabel(self.groupBox1)
        self.label_2.setObjectName(u"label_2")
        sizePolicy3.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy3)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.label_3 = QLabel(self.groupBox1)
        self.label_3.setObjectName(u"label_3")
        sizePolicy3.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy3)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 0, 4, 1, 1)


        self.verticalLayout.addWidget(self.groupBox1)

        self.groupBox_2 = QGroupBox(self.widget2)
        self.groupBox_2.setObjectName(u"groupBox_2")
        sizePolicy5.setHeightForWidth(self.groupBox_2.sizePolicy().hasHeightForWidth())
        self.groupBox_2.setSizePolicy(sizePolicy5)
        self.gridLayout_5 = QGridLayout(self.groupBox_2)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.frame_2 = QFrame(self.groupBox_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.gridLayout_12 = QGridLayout(self.frame_2)
        self.gridLayout_12.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_12.setObjectName(u"gridLayout_12")
        self.acao = QPushButton(self.frame_2)
        self.acao.setObjectName(u"acao")

        self.gridLayout_12.addWidget(self.acao, 0, 1, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_12.addItem(self.horizontalSpacer_3, 0, 0, 1, 1)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_12.addItem(self.horizontalSpacer_4, 0, 2, 1, 1)


        self.gridLayout_5.addWidget(self.frame_2, 3, 0, 1, 2)

        self.estado = QLabel(self.groupBox_2)
        self.estado.setObjectName(u"estado")
        sizePolicy5.setHeightForWidth(self.estado.sizePolicy().hasHeightForWidth())
        self.estado.setSizePolicy(sizePolicy5)
        self.estado.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.estado, 0, 0, 1, 1)

        self.data = QLabel(self.groupBox_2)
        self.data.setObjectName(u"data")
        sizePolicy6 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.data.sizePolicy().hasHeightForWidth())
        self.data.setSizePolicy(sizePolicy6)

        self.gridLayout_5.addWidget(self.data, 0, 1, 1, 1)

        self.label_30 = QLabel(self.groupBox_2)
        self.label_30.setObjectName(u"label_30")
        sizePolicy5.setHeightForWidth(self.label_30.sizePolicy().hasHeightForWidth())
        self.label_30.setSizePolicy(sizePolicy5)
        self.label_30.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.label_30, 1, 0, 1, 1)

        self.label_32 = QLabel(self.groupBox_2)
        self.label_32.setObjectName(u"label_32")
        sizePolicy5.setHeightForWidth(self.label_32.sizePolicy().hasHeightForWidth())
        self.label_32.setSizePolicy(sizePolicy5)
        self.label_32.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_5.addWidget(self.label_32, 2, 0, 1, 1)

        self.local = QLabel(self.groupBox_2)
        self.local.setObjectName(u"local")
        sizePolicy6.setHeightForWidth(self.local.sizePolicy().hasHeightForWidth())
        self.local.setSizePolicy(sizePolicy6)

        self.gridLayout_5.addWidget(self.local, 2, 1, 1, 1)

        self.comprovante = QLabel(self.groupBox_2)
        self.comprovante.setObjectName(u"comprovante")
        sizePolicy6.setHeightForWidth(self.comprovante.sizePolicy().hasHeightForWidth())
        self.comprovante.setSizePolicy(sizePolicy6)

        self.gridLayout_5.addWidget(self.comprovante, 1, 1, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_2)

        self.widget3 = QWidget(self.widget2)
        self.widget3.setObjectName(u"widget3")
        self.verticalLayout_3 = QVBoxLayout(self.widget3)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label = QLabel(self.widget3)
        self.label.setObjectName(u"label")
        sizePolicy5.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy5)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_3.addWidget(self.label)

        self.observacao = QTextEdit(self.widget3)
        self.observacao.setObjectName(u"observacao")
        self.observacao.setTabChangesFocus(True)

        self.verticalLayout_3.addWidget(self.observacao)


        self.verticalLayout.addWidget(self.widget3)


        self.gridLayout_3.addWidget(self.widget2, 0, 0, 1, 1)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_4 = QVBoxLayout(self.tab_2)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.formulario_pontos = QFrame(self.tab_2)
        self.formulario_pontos.setObjectName(u"formulario_pontos")
        sizePolicy7 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.formulario_pontos.sizePolicy().hasHeightForWidth())
        self.formulario_pontos.setSizePolicy(sizePolicy7)
        self.formulario_pontos.setFrameShape(QFrame.StyledPanel)
        self.formulario_pontos.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.formulario_pontos)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout_10 = QGridLayout()
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.gridLayout_10.setContentsMargins(-1, 0, -1, 11)
        self.label_10 = QLabel(self.formulario_pontos)
        self.label_10.setObjectName(u"label_10")
        sizePolicy8 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy8.setHorizontalStretch(0)
        sizePolicy8.setVerticalStretch(0)
        sizePolicy8.setHeightForWidth(self.label_10.sizePolicy().hasHeightForWidth())
        self.label_10.setSizePolicy(sizePolicy8)
        self.label_10.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_10, 3, 0, 1, 1)

        self.iniciativa_chefia = QComboBox(self.formulario_pontos)
        self.iniciativa_chefia.setObjectName(u"iniciativa_chefia")
        sizePolicy9 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy9.setHorizontalStretch(0)
        sizePolicy9.setVerticalStretch(0)
        sizePolicy9.setHeightForWidth(self.iniciativa_chefia.sizePolicy().hasHeightForWidth())
        self.iniciativa_chefia.setSizePolicy(sizePolicy9)
        self.iniciativa_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.iniciativa_chefia, 4, 1, 1, 1)

        self.conhecimento_chefia = QComboBox(self.formulario_pontos)
        self.conhecimento_chefia.setObjectName(u"conhecimento_chefia")
        sizePolicy9.setHeightForWidth(self.conhecimento_chefia.sizePolicy().hasHeightForWidth())
        self.conhecimento_chefia.setSizePolicy(sizePolicy9)
        self.conhecimento_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.conhecimento_chefia, 2, 1, 1, 1)

        self.label_18 = QLabel(self.formulario_pontos)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_18, 0, 1, 1, 1)

        self.assiduidade_chefia = QComboBox(self.formulario_pontos)
        self.assiduidade_chefia.setObjectName(u"assiduidade_chefia")
        sizePolicy9.setHeightForWidth(self.assiduidade_chefia.sizePolicy().hasHeightForWidth())
        self.assiduidade_chefia.setSizePolicy(sizePolicy9)
        self.assiduidade_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.assiduidade_chefia, 1, 1, 1, 1)

        self.label_9 = QLabel(self.formulario_pontos)
        self.label_9.setObjectName(u"label_9")
        sizePolicy8.setHeightForWidth(self.label_9.sizePolicy().hasHeightForWidth())
        self.label_9.setSizePolicy(sizePolicy8)
        self.label_9.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_9, 2, 0, 1, 1)

        self.label_11 = QLabel(self.formulario_pontos)
        self.label_11.setObjectName(u"label_11")
        sizePolicy8.setHeightForWidth(self.label_11.sizePolicy().hasHeightForWidth())
        self.label_11.setSizePolicy(sizePolicy8)
        self.label_11.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_11, 4, 0, 1, 1)

        self.conhecimento_servidor = QComboBox(self.formulario_pontos)
        self.conhecimento_servidor.setObjectName(u"conhecimento_servidor")
        sizePolicy9.setHeightForWidth(self.conhecimento_servidor.sizePolicy().hasHeightForWidth())
        self.conhecimento_servidor.setSizePolicy(sizePolicy9)
        self.conhecimento_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.conhecimento_servidor, 2, 2, 1, 1)

        self.iniciativa_servidor = QComboBox(self.formulario_pontos)
        self.iniciativa_servidor.setObjectName(u"iniciativa_servidor")
        sizePolicy9.setHeightForWidth(self.iniciativa_servidor.sizePolicy().hasHeightForWidth())
        self.iniciativa_servidor.setSizePolicy(sizePolicy9)
        self.iniciativa_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.iniciativa_servidor, 4, 2, 1, 1)

        self.comprometimento_chefia = QComboBox(self.formulario_pontos)
        self.comprometimento_chefia.setObjectName(u"comprometimento_chefia")
        sizePolicy9.setHeightForWidth(self.comprometimento_chefia.sizePolicy().hasHeightForWidth())
        self.comprometimento_chefia.setSizePolicy(sizePolicy9)
        self.comprometimento_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.comprometimento_chefia, 3, 1, 1, 1)

        self.label_4 = QLabel(self.formulario_pontos)
        self.label_4.setObjectName(u"label_4")
        sizePolicy8.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy8)
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_4, 1, 0, 1, 1)

        self.label_19 = QLabel(self.formulario_pontos)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_19, 0, 2, 1, 1)

        self.assiduidade_servidor = QComboBox(self.formulario_pontos)
        self.assiduidade_servidor.setObjectName(u"assiduidade_servidor")
        sizePolicy9.setHeightForWidth(self.assiduidade_servidor.sizePolicy().hasHeightForWidth())
        self.assiduidade_servidor.setSizePolicy(sizePolicy9)
        self.assiduidade_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.assiduidade_servidor, 1, 2, 1, 1)

        self.comprometimento_servidor = QComboBox(self.formulario_pontos)
        self.comprometimento_servidor.setObjectName(u"comprometimento_servidor")
        sizePolicy9.setHeightForWidth(self.comprometimento_servidor.sizePolicy().hasHeightForWidth())
        self.comprometimento_servidor.setSizePolicy(sizePolicy9)
        self.comprometimento_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.comprometimento_servidor, 3, 2, 1, 1)

        self.zelo_servidor = QComboBox(self.formulario_pontos)
        self.zelo_servidor.setObjectName(u"zelo_servidor")
        sizePolicy9.setHeightForWidth(self.zelo_servidor.sizePolicy().hasHeightForWidth())
        self.zelo_servidor.setSizePolicy(sizePolicy9)
        self.zelo_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.zelo_servidor, 10, 2, 1, 1)

        self.trabalho_servidor = QComboBox(self.formulario_pontos)
        self.trabalho_servidor.setObjectName(u"trabalho_servidor")
        sizePolicy9.setHeightForWidth(self.trabalho_servidor.sizePolicy().hasHeightForWidth())
        self.trabalho_servidor.setSizePolicy(sizePolicy9)
        self.trabalho_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.trabalho_servidor, 9, 2, 1, 1)

        self.trabalho_chefia = QComboBox(self.formulario_pontos)
        self.trabalho_chefia.setObjectName(u"trabalho_chefia")
        sizePolicy9.setHeightForWidth(self.trabalho_chefia.sizePolicy().hasHeightForWidth())
        self.trabalho_chefia.setSizePolicy(sizePolicy9)
        self.trabalho_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.trabalho_chefia, 9, 1, 1, 1)

        self.zelo_chefia = QComboBox(self.formulario_pontos)
        self.zelo_chefia.setObjectName(u"zelo_chefia")
        sizePolicy9.setHeightForWidth(self.zelo_chefia.sizePolicy().hasHeightForWidth())
        self.zelo_chefia.setSizePolicy(sizePolicy9)
        self.zelo_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.zelo_chefia, 10, 1, 1, 1)

        self.label_17 = QLabel(self.formulario_pontos)
        self.label_17.setObjectName(u"label_17")
        sizePolicy8.setHeightForWidth(self.label_17.sizePolicy().hasHeightForWidth())
        self.label_17.setSizePolicy(sizePolicy8)
        self.label_17.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_17, 10, 0, 1, 1)

        self.label_15 = QLabel(self.formulario_pontos)
        self.label_15.setObjectName(u"label_15")
        sizePolicy8.setHeightForWidth(self.label_15.sizePolicy().hasHeightForWidth())
        self.label_15.setSizePolicy(sizePolicy8)
        self.label_15.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_15, 8, 0, 1, 1)

        self.relacionamento_chefia = QComboBox(self.formulario_pontos)
        self.relacionamento_chefia.setObjectName(u"relacionamento_chefia")
        sizePolicy9.setHeightForWidth(self.relacionamento_chefia.sizePolicy().hasHeightForWidth())
        self.relacionamento_chefia.setSizePolicy(sizePolicy9)
        self.relacionamento_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.relacionamento_chefia, 8, 1, 1, 1)

        self.relacionamento_servidor = QComboBox(self.formulario_pontos)
        self.relacionamento_servidor.setObjectName(u"relacionamento_servidor")
        sizePolicy9.setHeightForWidth(self.relacionamento_servidor.sizePolicy().hasHeightForWidth())
        self.relacionamento_servidor.setSizePolicy(sizePolicy9)
        self.relacionamento_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.relacionamento_servidor, 8, 2, 1, 1)

        self.qualidade_servidor = QComboBox(self.formulario_pontos)
        self.qualidade_servidor.setObjectName(u"qualidade_servidor")
        sizePolicy9.setHeightForWidth(self.qualidade_servidor.sizePolicy().hasHeightForWidth())
        self.qualidade_servidor.setSizePolicy(sizePolicy9)
        self.qualidade_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.qualidade_servidor, 7, 2, 1, 1)

        self.qualidade_chefia = QComboBox(self.formulario_pontos)
        self.qualidade_chefia.setObjectName(u"qualidade_chefia")
        sizePolicy9.setHeightForWidth(self.qualidade_chefia.sizePolicy().hasHeightForWidth())
        self.qualidade_chefia.setSizePolicy(sizePolicy9)
        self.qualidade_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.qualidade_chefia, 7, 1, 1, 1)

        self.label_16 = QLabel(self.formulario_pontos)
        self.label_16.setObjectName(u"label_16")
        sizePolicy8.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy8)
        self.label_16.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_16, 9, 0, 1, 1)

        self.label_12 = QLabel(self.formulario_pontos)
        self.label_12.setObjectName(u"label_12")
        sizePolicy8.setHeightForWidth(self.label_12.sizePolicy().hasHeightForWidth())
        self.label_12.setSizePolicy(sizePolicy8)
        self.label_12.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_12, 5, 0, 1, 1)

        self.pontualidade_servidor = QComboBox(self.formulario_pontos)
        self.pontualidade_servidor.setObjectName(u"pontualidade_servidor")
        sizePolicy9.setHeightForWidth(self.pontualidade_servidor.sizePolicy().hasHeightForWidth())
        self.pontualidade_servidor.setSizePolicy(sizePolicy9)
        self.pontualidade_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.pontualidade_servidor, 6, 2, 1, 1)

        self.planejamento_servidor = QComboBox(self.formulario_pontos)
        self.planejamento_servidor.setObjectName(u"planejamento_servidor")
        sizePolicy9.setHeightForWidth(self.planejamento_servidor.sizePolicy().hasHeightForWidth())
        self.planejamento_servidor.setSizePolicy(sizePolicy9)
        self.planejamento_servidor.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.planejamento_servidor, 5, 2, 1, 1)

        self.label_13 = QLabel(self.formulario_pontos)
        self.label_13.setObjectName(u"label_13")
        sizePolicy8.setHeightForWidth(self.label_13.sizePolicy().hasHeightForWidth())
        self.label_13.setSizePolicy(sizePolicy8)
        self.label_13.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_13, 6, 0, 1, 1)

        self.pontualidade_chefia = QComboBox(self.formulario_pontos)
        self.pontualidade_chefia.setObjectName(u"pontualidade_chefia")
        sizePolicy9.setHeightForWidth(self.pontualidade_chefia.sizePolicy().hasHeightForWidth())
        self.pontualidade_chefia.setSizePolicy(sizePolicy9)
        self.pontualidade_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.pontualidade_chefia, 6, 1, 1, 1)

        self.planejamento_chefia = QComboBox(self.formulario_pontos)
        self.planejamento_chefia.setObjectName(u"planejamento_chefia")
        sizePolicy9.setHeightForWidth(self.planejamento_chefia.sizePolicy().hasHeightForWidth())
        self.planejamento_chefia.setSizePolicy(sizePolicy9)
        self.planejamento_chefia.setMinimumSize(QSize(100, 0))

        self.gridLayout_10.addWidget(self.planejamento_chefia, 5, 1, 1, 1)

        self.label_14 = QLabel(self.formulario_pontos)
        self.label_14.setObjectName(u"label_14")
        sizePolicy8.setHeightForWidth(self.label_14.sizePolicy().hasHeightForWidth())
        self.label_14.setSizePolicy(sizePolicy8)
        self.label_14.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_14, 7, 0, 1, 1)

        self.label_37 = QLabel(self.formulario_pontos)
        self.label_37.setObjectName(u"label_37")
        sizePolicy5.setHeightForWidth(self.label_37.sizePolicy().hasHeightForWidth())
        self.label_37.setSizePolicy(sizePolicy5)
        self.label_37.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_10.addWidget(self.label_37, 11, 0, 1, 1)

        self.pontos_chefia = QLabel(self.formulario_pontos)
        self.pontos_chefia.setObjectName(u"pontos_chefia")
        sizePolicy5.setHeightForWidth(self.pontos_chefia.sizePolicy().hasHeightForWidth())
        self.pontos_chefia.setSizePolicy(sizePolicy5)

        self.gridLayout_10.addWidget(self.pontos_chefia, 11, 1, 1, 1)

        self.pontos_servidor = QLabel(self.formulario_pontos)
        self.pontos_servidor.setObjectName(u"pontos_servidor")
        sizePolicy5.setHeightForWidth(self.pontos_servidor.sizePolicy().hasHeightForWidth())
        self.pontos_servidor.setSizePolicy(sizePolicy5)

        self.gridLayout_10.addWidget(self.pontos_servidor, 11, 2, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_10)

        self.verticalSpacer = QSpacerItem(0, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.gridLayout_8 = QGridLayout()
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.gridLayout_8.setContentsMargins(-1, 11, -1, -1)
        self.pontos_total = QLabel(self.formulario_pontos)
        self.pontos_total.setObjectName(u"pontos_total")
        sizePolicy10 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy10.setHorizontalStretch(0)
        sizePolicy10.setVerticalStretch(0)
        sizePolicy10.setHeightForWidth(self.pontos_total.sizePolicy().hasHeightForWidth())
        self.pontos_total.setSizePolicy(sizePolicy10)
        self.pontos_total.setTextFormat(Qt.RichText)

        self.gridLayout_8.addWidget(self.pontos_total, 4, 1, 1, 1)

        self.pontos_disciplina = QLineEdit(self.formulario_pontos)
        self.pontos_disciplina.setObjectName(u"pontos_disciplina")
        self.pontos_disciplina.setEnabled(True)
        sizePolicy1.setHeightForWidth(self.pontos_disciplina.sizePolicy().hasHeightForWidth())
        self.pontos_disciplina.setSizePolicy(sizePolicy1)
        self.pontos_disciplina.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.pontos_disciplina, 0, 1, 1, 1)

        self.pontos_evolucao = QLineEdit(self.formulario_pontos)
        self.pontos_evolucao.setObjectName(u"pontos_evolucao")
        self.pontos_evolucao.setEnabled(True)
        sizePolicy1.setHeightForWidth(self.pontos_evolucao.sizePolicy().hasHeightForWidth())
        self.pontos_evolucao.setSizePolicy(sizePolicy1)
        self.pontos_evolucao.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.pontos_evolucao, 2, 1, 1, 1)

        self.label_35 = QLabel(self.formulario_pontos)
        self.label_35.setObjectName(u"label_35")
        sizePolicy5.setHeightForWidth(self.label_35.sizePolicy().hasHeightForWidth())
        self.label_35.setSizePolicy(sizePolicy5)
        self.label_35.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.label_35, 6, 0, 1, 1)

        self.label_34 = QLabel(self.formulario_pontos)
        self.label_34.setObjectName(u"label_34")
        sizePolicy8.setHeightForWidth(self.label_34.sizePolicy().hasHeightForWidth())
        self.label_34.setSizePolicy(sizePolicy8)
        self.label_34.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.label_34, 2, 0, 1, 1)

        self.label_39 = QLabel(self.formulario_pontos)
        self.label_39.setObjectName(u"label_39")
        sizePolicy5.setHeightForWidth(self.label_39.sizePolicy().hasHeightForWidth())
        self.label_39.setSizePolicy(sizePolicy5)
        self.label_39.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.label_39, 4, 0, 1, 1)

        self.label_20 = QLabel(self.formulario_pontos)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.label_20, 5, 0, 1, 1)

        self.pontos_total_servidor = QLabel(self.formulario_pontos)
        self.pontos_total_servidor.setObjectName(u"pontos_total_servidor")

        self.gridLayout_8.addWidget(self.pontos_total_servidor, 5, 1, 1, 1)

        self.label_36 = QLabel(self.formulario_pontos)
        self.label_36.setObjectName(u"label_36")
        sizePolicy8.setHeightForWidth(self.label_36.sizePolicy().hasHeightForWidth())
        self.label_36.setSizePolicy(sizePolicy8)
        self.label_36.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.label_36, 0, 0, 1, 1)

        self.reavaliar = QLabel(self.formulario_pontos)
        self.reavaliar.setObjectName(u"reavaliar")
        sizePolicy10.setHeightForWidth(self.reavaliar.sizePolicy().hasHeightForWidth())
        self.reavaliar.setSizePolicy(sizePolicy10)
        self.reavaliar.setMinimumSize(QSize(0, 0))
        self.reavaliar.setTextFormat(Qt.RichText)

        self.gridLayout_8.addWidget(self.reavaliar, 6, 1, 1, 1)

        self.label_21 = QLabel(self.formulario_pontos)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.label_21, 1, 0, 1, 1)

        self.pontos_penalidade = QLineEdit(self.formulario_pontos)
        self.pontos_penalidade.setObjectName(u"pontos_penalidade")
        sizePolicy1.setHeightForWidth(self.pontos_penalidade.sizePolicy().hasHeightForWidth())
        self.pontos_penalidade.setSizePolicy(sizePolicy1)
        self.pontos_penalidade.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_8.addWidget(self.pontos_penalidade, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_8)


        self.verticalLayout_4.addWidget(self.formulario_pontos)

        self.tabWidget.addTab(self.tab_2, "")
        self.tab_5 = QWidget()
        self.tab_5.setObjectName(u"tab_5")
        self.gridLayout_6 = QGridLayout(self.tab_5)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.tramites = QTableWidget(self.tab_5)
        if (self.tramites.columnCount() < 3):
            self.tramites.setColumnCount(3)
        __qtablewidgetitem = QTableWidgetItem()
        self.tramites.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tramites.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tramites.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        self.tramites.setObjectName(u"tramites")
        self.tramites.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tramites.setAlternatingRowColors(True)
        self.tramites.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tramites.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tramites.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tramites.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tramites.setWordWrap(False)
        self.tramites.horizontalHeader().setHighlightSections(False)
        self.tramites.verticalHeader().setVisible(False)

        self.gridLayout_6.addWidget(self.tramites, 1, 0, 1, 2)

        self.excluidos = QCheckBox(self.tab_5)
        self.excluidos.setObjectName(u"excluidos")

        self.gridLayout_6.addWidget(self.excluidos, 0, 0, 1, 2)

        self.tabWidget.addTab(self.tab_5, "")

        self.gridLayout_4.addWidget(self.tabWidget, 0, 0, 1, 2)

        QWidget.setTabOrder(self.tabWidget, self.acao)
        QWidget.setTabOrder(self.acao, self.fechar)
        QWidget.setTabOrder(self.fechar, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.salvar)
        QWidget.setTabOrder(self.salvar, self.assiduidade_chefia)
        QWidget.setTabOrder(self.assiduidade_chefia, self.conhecimento_chefia)
        QWidget.setTabOrder(self.conhecimento_chefia, self.comprometimento_chefia)
        QWidget.setTabOrder(self.comprometimento_chefia, self.iniciativa_chefia)
        QWidget.setTabOrder(self.iniciativa_chefia, self.planejamento_chefia)
        QWidget.setTabOrder(self.planejamento_chefia, self.pontualidade_chefia)
        QWidget.setTabOrder(self.pontualidade_chefia, self.qualidade_chefia)
        QWidget.setTabOrder(self.qualidade_chefia, self.relacionamento_chefia)
        QWidget.setTabOrder(self.relacionamento_chefia, self.trabalho_chefia)
        QWidget.setTabOrder(self.trabalho_chefia, self.zelo_chefia)
        QWidget.setTabOrder(self.zelo_chefia, self.assiduidade_servidor)
        QWidget.setTabOrder(self.assiduidade_servidor, self.conhecimento_servidor)
        QWidget.setTabOrder(self.conhecimento_servidor, self.comprometimento_servidor)
        QWidget.setTabOrder(self.comprometimento_servidor, self.iniciativa_servidor)
        QWidget.setTabOrder(self.iniciativa_servidor, self.planejamento_servidor)
        QWidget.setTabOrder(self.planejamento_servidor, self.pontualidade_servidor)
        QWidget.setTabOrder(self.pontualidade_servidor, self.qualidade_servidor)
        QWidget.setTabOrder(self.qualidade_servidor, self.relacionamento_servidor)
        QWidget.setTabOrder(self.relacionamento_servidor, self.trabalho_servidor)
        QWidget.setTabOrder(self.trabalho_servidor, self.zelo_servidor)
        QWidget.setTabOrder(self.zelo_servidor, self.pontos_disciplina)
        QWidget.setTabOrder(self.pontos_disciplina, self.pontos_penalidade)
        QWidget.setTabOrder(self.pontos_penalidade, self.pontos_evolucao)
        QWidget.setTabOrder(self.pontos_evolucao, self.excluidos)
        QWidget.setTabOrder(self.excluidos, self.tramites)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.close)
        self.salvar.released.connect(Dialog.salvar)
        self.pushButton.released.connect(Dialog.historico)
        self.pushButton_2.released.connect(Dialog.emitir_formulario)
        self.tramites.cellDoubleClicked.connect(Dialog.tramite)
        self.excluidos.released.connect(Dialog.carregar_tramites)
        self.pontos_disciplina.editingFinished.connect(Dialog.carregar_pontos)
        self.pontos_evolucao.editingFinished.connect(Dialog.carregar_pontos)
        self.pontos_penalidade.editingFinished.connect(Dialog.carregar_pontos)
        self.acao.released.connect(Dialog.acao)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Avalia\u00e7\u00e3o", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Formul\u00e1rio", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.aviso.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p align=\"center\"><span style=\" color:#ff0004;\">Servidor exclu\u00eddo.</span></p></body></html>", None))
        self.groupBox2.setTitle(QCoreApplication.translate("Dialog", u"Servidor", None))
        self.label_7.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.matricula.setText(QCoreApplication.translate("Dialog", u"matricula", None))
        self.cargo.setText(QCoreApplication.translate("Dialog", u"cargo", None))
        self.servidor.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p>servidor</p></body></html>", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Cargo:", None))
        self.label_8.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o:", None))
        self.lotacao.setText(QCoreApplication.translate("Dialog", u"lotacao", None))
        self.groupBox1.setTitle(QCoreApplication.translate("Dialog", u"Per\u00edodo", None))
        self.data_final.setText(QCoreApplication.translate("Dialog", u"data_final", None))
        self.data_inicial.setText(QCoreApplication.translate("Dialog", u"data_inicial", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Data inicial:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Data final:", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Formul\u00e1rio:", None))
        self.acao.setText(QCoreApplication.translate("Dialog", u"A\u00e7\u00e3o", None))
        self.estado.setText(QCoreApplication.translate("Dialog", u"Enviado em:", None))
        self.data.setText("")
        self.label_30.setText(QCoreApplication.translate("Dialog", u"Comprovante:", None))
        self.label_32.setText(QCoreApplication.translate("Dialog", u"Local atual:", None))
        self.local.setText("")
        self.comprovante.setText("")
        self.label.setText(QCoreApplication.translate("Dialog", u"Observa\u00e7\u00f5es/Informa\u00e7\u00f5es:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Dialog", u"Dados", None))
        self.label_10.setText(QCoreApplication.translate("Dialog", u"Comprometimento:", None))
        self.label_18.setText(QCoreApplication.translate("Dialog", u"Chefia", None))
        self.label_9.setText(QCoreApplication.translate("Dialog", u"Conhecimento do trabalho:", None))
        self.label_11.setText(QCoreApplication.translate("Dialog", u"Iniciativa:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Assiduidade:", None))
        self.label_19.setText(QCoreApplication.translate("Dialog", u"Servidor", None))
        self.label_17.setText(QCoreApplication.translate("Dialog", u"Zelo pelo patrim\u00f4nio p\u00fablico:", None))
        self.label_15.setText(QCoreApplication.translate("Dialog", u"Relacionamento interpessoal:", None))
        self.label_16.setText(QCoreApplication.translate("Dialog", u"Trabalho em equipe:", None))
        self.label_12.setText(QCoreApplication.translate("Dialog", u"Planejamento e organiza\u00e7\u00e3o do trabalho:", None))
        self.label_13.setText(QCoreApplication.translate("Dialog", u"Pontualidade:", None))
        self.label_14.setText(QCoreApplication.translate("Dialog", u"Qualidade do trabalho:", None))
        self.label_37.setText(QCoreApplication.translate("Dialog", u"Total Geral de Pontos:", None))
        self.pontos_chefia.setText("")
        self.pontos_servidor.setText("")
        self.pontos_total.setText("")
        self.pontos_disciplina.setInputMask("")
        self.pontos_evolucao.setInputMask("")
        self.label_35.setText(QCoreApplication.translate("Dialog", u"Reavaliar:", None))
        self.label_34.setText(QCoreApplication.translate("Dialog", u"Total de pontos obtidos na evolu\u00e7\u00e3o da qualifica\u00e7\u00e3o mensurada.:", None))
        self.label_39.setText(QCoreApplication.translate("Dialog", u"Pontua\u00e7\u00e3o final da chefia:", None))
        self.label_20.setText(QCoreApplication.translate("Dialog", u"Pontua\u00e7\u00e3o final do servidor:", None))
        self.pontos_total_servidor.setText("")
        self.label_36.setText(QCoreApplication.translate("Dialog", u"Total de pontos obtidos no quesito disciplina:", None))
        self.label_21.setText(QCoreApplication.translate("Dialog", u"Total de pontos obtidos no quisito penalidade:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Dialog", u"Pontua\u00e7\u00e3o", None))
        ___qtablewidgetitem = self.tramites.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"Data", None));
        ___qtablewidgetitem1 = self.tramites.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Dialog", u"Comprovante", None));
        ___qtablewidgetitem2 = self.tramites.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Dialog", u"Destino", None));
        self.excluidos.setText(QCoreApplication.translate("Dialog", u"Exclu\u00eddos", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_5), QCoreApplication.translate("Dialog", u"Tramites", None))
    # retranslateUi

