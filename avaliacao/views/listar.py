# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(921, 482)
        Form.setMinimumSize(QSize(921, 482))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.avaliacoes = QTableWidget(Form)
        if (self.avaliacoes.columnCount() < 6):
            self.avaliacoes.setColumnCount(6)
        __qtablewidgetitem = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(5, __qtablewidgetitem5)
        self.avaliacoes.setObjectName(u"avaliacoes")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.avaliacoes.sizePolicy().hasHeightForWidth())
        self.avaliacoes.setSizePolicy(sizePolicy)
        self.avaliacoes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.avaliacoes.setAlternatingRowColors(True)
        self.avaliacoes.setSelectionMode(QAbstractItemView.SingleSelection)
        self.avaliacoes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.avaliacoes.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.avaliacoes.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.avaliacoes.setSortingEnabled(True)
        self.avaliacoes.horizontalHeader().setHighlightSections(False)
        self.avaliacoes.horizontalHeader().setProperty("showSortIndicator", True)
        self.avaliacoes.horizontalHeader().setStretchLastSection(False)
        self.avaliacoes.verticalHeader().setVisible(False)

        self.gridLayout.addWidget(self.avaliacoes, 0, 3, 2, 1)

        self.fechar = QPushButton(Form)
        self.fechar.setObjectName(u"fechar")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.fechar, 1, 1, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 1, 0, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_3, 1, 2, 1, 1)

        self.tabWidget = QTabWidget(Form)
        self.tabWidget.setObjectName(u"tabWidget")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy2)
        self.tabWidget.setMaximumSize(QSize(300, 16777215))
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout = QVBoxLayout(self.tab)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.groupBox = QGroupBox(self.tab)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy3)
        self.gridLayout_4 = QGridLayout(self.groupBox)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.base = QCheckBox(self.groupBox)
        self.base.setObjectName(u"base")
        sizePolicy4 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Minimum)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.base.sizePolicy().hasHeightForWidth())
        self.base.setSizePolicy(sizePolicy4)
        self.base.setChecked(True)

        self.gridLayout_4.addWidget(self.base, 0, 0, 1, 1)

        self.data_base = QDateEdit(self.groupBox)
        self.data_base.setObjectName(u"data_base")
        sizePolicy5 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.data_base.sizePolicy().hasHeightForWidth())
        self.data_base.setSizePolicy(sizePolicy5)
        self.data_base.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.data_base, 0, 1, 1, 2)


        self.verticalLayout.addWidget(self.groupBox)

        self.groupBox_3 = QGroupBox(self.tab)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.gridLayout_5 = QGridLayout(self.groupBox_3)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.aberto = QRadioButton(self.groupBox_3)
        self.aberto.setObjectName(u"aberto")
        self.aberto.setChecked(True)

        self.gridLayout_5.addWidget(self.aberto, 0, 0, 1, 1)

        self.devolvido = QRadioButton(self.groupBox_3)
        self.devolvido.setObjectName(u"devolvido")

        self.gridLayout_5.addWidget(self.devolvido, 1, 1, 1, 1)

        self.enviado = QRadioButton(self.groupBox_3)
        self.enviado.setObjectName(u"enviado")

        self.gridLayout_5.addWidget(self.enviado, 0, 1, 1, 1)

        self.atrasado = QRadioButton(self.groupBox_3)
        self.atrasado.setObjectName(u"atrasado")

        self.gridLayout_5.addWidget(self.atrasado, 1, 0, 1, 1)

        self.reavaliar = QRadioButton(self.groupBox_3)
        self.reavaliar.setObjectName(u"reavaliar")

        self.gridLayout_5.addWidget(self.reavaliar, 2, 0, 1, 1)

        self.finalizado = QRadioButton(self.groupBox_3)
        self.finalizado.setObjectName(u"finalizado")

        self.gridLayout_5.addWidget(self.finalizado, 2, 1, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_3)

        self.groupBox_4 = QGroupBox(self.tab)
        self.groupBox_4.setObjectName(u"groupBox_4")
        sizePolicy3.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy3)
        self.gridLayout_6 = QGridLayout(self.groupBox_4)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.lotacao = QComboBox(self.groupBox_4)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy3.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy3)
        self.lotacao.setMinimumSize(QSize(250, 0))

        self.gridLayout_6.addWidget(self.lotacao, 1, 0, 1, 1)

        self.label = QLabel(self.groupBox_4)
        self.label.setObjectName(u"label")
        sizePolicy6 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy6)

        self.gridLayout_6.addWidget(self.label, 0, 0, 1, 1)

        self.label_2 = QLabel(self.groupBox_4)
        self.label_2.setObjectName(u"label_2")
        sizePolicy6.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy6)

        self.gridLayout_6.addWidget(self.label_2, 2, 0, 1, 1)

        self.sublotacao = QComboBox(self.groupBox_4)
        self.sublotacao.setObjectName(u"sublotacao")
        sizePolicy3.setHeightForWidth(self.sublotacao.sizePolicy().hasHeightForWidth())
        self.sublotacao.setSizePolicy(sizePolicy3)

        self.gridLayout_6.addWidget(self.sublotacao, 3, 0, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_4)

        self.filtrar = QPushButton(self.tab)
        self.filtrar.setObjectName(u"filtrar")

        self.verticalLayout.addWidget(self.filtrar)

        self.total_avaliacoes = QLabel(self.tab)
        self.total_avaliacoes.setObjectName(u"total_avaliacoes")
        sizePolicy3.setHeightForWidth(self.total_avaliacoes.sizePolicy().hasHeightForWidth())
        self.total_avaliacoes.setSizePolicy(sizePolicy3)

        self.verticalLayout.addWidget(self.total_avaliacoes)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_2 = QVBoxLayout(self.tab_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.groupBox_2 = QGroupBox(self.tab_2)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_2 = QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.selecionar_todos = QPushButton(self.groupBox_2)
        self.selecionar_todos.setObjectName(u"selecionar_todos")

        self.gridLayout_2.addWidget(self.selecionar_todos, 0, 0, 1, 1)

        self.deselecionar_todos = QPushButton(self.groupBox_2)
        self.deselecionar_todos.setObjectName(u"deselecionar_todos")

        self.gridLayout_2.addWidget(self.deselecionar_todos, 0, 2, 1, 1)

        self.inverter_selecao = QPushButton(self.groupBox_2)
        self.inverter_selecao.setObjectName(u"inverter_selecao")

        self.gridLayout_2.addWidget(self.inverter_selecao, 0, 1, 1, 1)


        self.verticalLayout_2.addWidget(self.groupBox_2)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer_5)

        self.edicao_lote = QPushButton(self.tab_2)
        self.edicao_lote.setObjectName(u"edicao_lote")
        self.edicao_lote.setEnabled(True)

        self.verticalLayout_2.addWidget(self.edicao_lote)

        self.label_3 = QLabel(self.tab_2)
        self.label_3.setObjectName(u"label_3")
        sizePolicy3.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy3)
        self.label_3.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_3)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer)

        self.exportar_filtradas = QPushButton(self.tab_2)
        self.exportar_filtradas.setObjectName(u"exportar_filtradas")
        self.exportar_filtradas.setEnabled(True)
        sizePolicy3.setHeightForWidth(self.exportar_filtradas.sizePolicy().hasHeightForWidth())
        self.exportar_filtradas.setSizePolicy(sizePolicy3)
        self.exportar_filtradas.setLayoutDirection(Qt.LeftToRight)

        self.verticalLayout_2.addWidget(self.exportar_filtradas)

        self.label_4 = QLabel(self.tab_2)
        self.label_4.setObjectName(u"label_4")
        sizePolicy5.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy5)
        self.label_4.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_4)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer_4)

        self.pushButton = QPushButton(self.tab_2)
        self.pushButton.setObjectName(u"pushButton")

        self.verticalLayout_2.addWidget(self.pushButton)

        self.label_5 = QLabel(self.tab_2)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_5)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout_2.addItem(self.horizontalSpacer_6)

        self.acao = QPushButton(self.tab_2)
        self.acao.setObjectName(u"acao")
        self.acao.setEnabled(False)

        self.verticalLayout_2.addWidget(self.acao)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 3)

        QWidget.setTabOrder(self.tabWidget, self.base)
        QWidget.setTabOrder(self.base, self.data_base)
        QWidget.setTabOrder(self.data_base, self.aberto)
        QWidget.setTabOrder(self.aberto, self.enviado)
        QWidget.setTabOrder(self.enviado, self.atrasado)
        QWidget.setTabOrder(self.atrasado, self.devolvido)
        QWidget.setTabOrder(self.devolvido, self.reavaliar)
        QWidget.setTabOrder(self.reavaliar, self.finalizado)
        QWidget.setTabOrder(self.finalizado, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.filtrar)
        QWidget.setTabOrder(self.filtrar, self.selecionar_todos)
        QWidget.setTabOrder(self.selecionar_todos, self.inverter_selecao)
        QWidget.setTabOrder(self.inverter_selecao, self.deselecionar_todos)
        QWidget.setTabOrder(self.deselecionar_todos, self.edicao_lote)
        QWidget.setTabOrder(self.edicao_lote, self.exportar_filtradas)
        QWidget.setTabOrder(self.exportar_filtradas, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.acao)
        QWidget.setTabOrder(self.acao, self.avaliacoes)
        QWidget.setTabOrder(self.avaliacoes, self.fechar)

        self.retranslateUi(Form)
        self.filtrar.released.connect(Form.atualizar)
        self.exportar_filtradas.released.connect(Form.exportar)
        self.avaliacoes.cellDoubleClicked.connect(Form.abrir_avaliacao)
        self.fechar.released.connect(Form.close)
        self.selecionar_todos.pressed.connect(Form.selecionar_todos)
        self.deselecionar_todos.released.connect(Form.deselecionar_todos)
        self.inverter_selecao.released.connect(Form.reverter_selecao)
        self.edicao_lote.released.connect(Form.edicao_lote)
        self.pushButton.released.connect(Form.gerar_formularios)
        self.acao.released.connect(Form.acao)

        self.tabWidget.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Avalia\u00e7\u00f5es", None))
        ___qtablewidgetitem = self.avaliacoes.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"id", None));
        ___qtablewidgetitem1 = self.avaliacoes.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"#", None));
        ___qtablewidgetitem2 = self.avaliacoes.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Servidor", None));
        ___qtablewidgetitem3 = self.avaliacoes.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem4 = self.avaliacoes.horizontalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o", None));
        ___qtablewidgetitem5 = self.avaliacoes.horizontalHeaderItem(5)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o", None));
        self.fechar.setText(QCoreApplication.translate("Form", u"Fechar", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Per\u00edodo:", None))
        self.base.setText(QCoreApplication.translate("Form", u"A partir de:", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form", u"Estado do formul\u00e1rio:", None))
        self.aberto.setText(QCoreApplication.translate("Form", u"A enviar", None))
        self.devolvido.setText(QCoreApplication.translate("Form", u"A apurar", None))
        self.enviado.setText(QCoreApplication.translate("Form", u"Enviado", None))
        self.atrasado.setText(QCoreApplication.translate("Form", u"Atrasado", None))
        self.reavaliar.setText(QCoreApplication.translate("Form", u"A reavaliar", None))
        self.finalizado.setText(QCoreApplication.translate("Form", u"Finalizado", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("Form", u"Local de trabalho:", None))
        self.label.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o", None))
        self.filtrar.setText(QCoreApplication.translate("Form", u"Filtrar", None))
        self.total_avaliacoes.setText(QCoreApplication.translate("Form", u"0 avalia\u00e7\u00f5es encontradas", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Form", u"Filtros", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Selecionar:", None))
        self.selecionar_todos.setText(QCoreApplication.translate("Form", u"Todos", None))
        self.deselecionar_todos.setText(QCoreApplication.translate("Form", u"Nenhum", None))
        self.inverter_selecao.setText(QCoreApplication.translate("Form", u"Inverter", None))
        self.edicao_lote.setText(QCoreApplication.translate("Form", u"Edi\u00e7\u00e3o em lote", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"<html><head/><body><p align=\"justify\">A edi\u00e7\u00e3o em lote permite a inser\u00e7\u00e3o e modifica\u00e7\u00e3o de dados nos registros selecionados.</p></body></html>", None))
        self.exportar_filtradas.setText(QCoreApplication.translate("Form", u"Exportar avalia\u00e7\u00f5es filtradas", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"<html><head/><body><p align=\"justify\">Exporte as avalia\u00e7\u00f5es filtradas para um arquivo edit\u00e1vel.</p></body></html>", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Gerar formul\u00e1rio", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Cria os formul\u00e1rio de avalila\u00e7\u00e3o e os exporta em um documento odt.", None))
        self.acao.setText(QCoreApplication.translate("Form", u"Enviar formul\u00e1rios", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Ferramentas", None))
    # retranslateUi

