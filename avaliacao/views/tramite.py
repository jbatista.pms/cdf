# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'tramite.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(431, 244)
        Dialog.setMinimumSize(QSize(431, 244))
        Dialog.setMaximumSize(QSize(431, 244))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.data = QDateEdit(Dialog)
        self.data.setObjectName(u"data")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.data.sizePolicy().hasHeightForWidth())
        self.data.setSizePolicy(sizePolicy)
        self.data.setCalendarPopup(True)

        self.gridLayout.addWidget(self.data, 1, 0, 1, 1)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 4, 0, 1, 2)

        self.observacao = QPlainTextEdit(Dialog)
        self.observacao.setObjectName(u"observacao")
        self.observacao.setTabChangesFocus(True)

        self.gridLayout.addWidget(self.observacao, 5, 0, 1, 2)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 0, 1, 1, 1)

        self.comprovante = QLineEdit(Dialog)
        self.comprovante.setObjectName(u"comprovante")

        self.gridLayout.addWidget(self.comprovante, 1, 1, 1, 1)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_2, 0, 5, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer, 0, 0, 1, 1)

        self.salvar = QPushButton(self.frame)
        self.salvar.setObjectName(u"salvar")

        self.gridLayout_2.addWidget(self.salvar, 0, 4, 1, 1)

        self.fechar = QPushButton(self.frame)
        self.fechar.setObjectName(u"fechar")

        self.gridLayout_2.addWidget(self.fechar, 0, 2, 1, 1)

        self.excluir = QPushButton(self.frame)
        self.excluir.setObjectName(u"excluir")

        self.gridLayout_2.addWidget(self.excluir, 0, 1, 1, 1)

        self.historico = QPushButton(self.frame)
        self.historico.setObjectName(u"historico")

        self.gridLayout_2.addWidget(self.historico, 0, 3, 1, 1)


        self.gridLayout.addWidget(self.frame, 7, 0, 1, 2)

        self.aviso = QLabel(Dialog)
        self.aviso.setObjectName(u"aviso")
        self.aviso.setTextFormat(Qt.RichText)
        self.aviso.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.aviso, 6, 0, 1, 2)

        QWidget.setTabOrder(self.data, self.comprovante)
        QWidget.setTabOrder(self.comprovante, self.observacao)
        QWidget.setTabOrder(self.observacao, self.excluir)
        QWidget.setTabOrder(self.excluir, self.fechar)
        QWidget.setTabOrder(self.fechar, self.historico)
        QWidget.setTabOrder(self.historico, self.salvar)

        self.retranslateUi(Dialog)
        self.excluir.released.connect(Dialog.excluir)
        self.fechar.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.salvar)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Tramite", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Data:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Observa\u00e7\u00e3o/Informa\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Comprovante", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.aviso.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p><span style=\" color:#ff0004;\">Exclu\u00eddo</span></p></body></html>", None))
    # retranslateUi

