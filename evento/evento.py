import sys
from datetime import date
from PySide2 import QtCore

from QtUtils import subwindowsbase
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.db import DataBase
from QtUtils.colecoes.excluir import Excluir
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.historico import historico
from QtUtils.validadores import TextoEmMaisculo
from QtUtils.text import text_format_uppercase

from avaliacao.models import Avaliacao
from intersticio.modelos import Intersticio
from servidor.mantenedor import MantenedorServidor
from .views.evento import Ui_Dialog
from .models import Evento


class Control(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def avaliacao_coincidente(self, data_inicial=None):
        data_inicial = data_inicial or self.instance.data_inicial
        return Avaliacao.select2().where(
            (Avaliacao.data_inicial<=data_inicial) &
            (Avaliacao.data_final>=data_inicial) &
            (Intersticio.aberto==True) &
            (Intersticio.servidor==self.instance.servidor)
        ).first()

    def excluir(self):
        if self.instance and not self.impedimento() and Excluir(self).exec_():
            with DataBase().obter_database().atomic():
                self.instance.excluir_instancia()
                mant_ser = MantenedorServidor(self.instance.servidor)
                mant_ser.calcular_prazos_intersticios()
                mant_ser.salvar()
            self.enviar_sinal_exclusao(self.instance)
            self.accept()
    
    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def impedimento(self, avaliacao=None):
        # Verificar impedimentos para alteração ou exclusão de evento com base
        # na avaliação coincidente
        # TODO: Retirar após regularização
        return False
        avaliacao = avaliacao or self.avaliacao_coincidente()
        while not avaliacao is None:
            if not avaliacao.ultimo_tramite() is None:
                Alerta(self,
                    text="O período é anterior ou coincide com o período de uma avaliação, "
                        "cujo formulário já foi enviado.\n"
                        "Não será possível inserir ou atualizar esse evento, uma vez que formulários "
                        "já enviados não podem ter seus períodos alterados."
                ).exec_()
                return True
            avaliacao = Avaliacao.select2().where(Avaliacao.anterior==avaliacao).first()
        return False

    def inicializar(self, *args, **kwargs):
        self.ui.servidor.setText(self.instance.servidor.nome)
        self.ui.matricula.setText(self.instance.servidor.matricula)
        self.ui.descricao.setValidator(TextoEmMaisculo(self))
        text_format_uppercase(self.ui.informacoes)
        if self.instance.id is None:
            data_hoje = datetime_qdate(date.today())
            self.ui.excluir.hide()
            self.ui.historico.hide()
            self.setWindowTitle("Novo evento")
            self.ui.data_inicial.setDate(data_hoje)
            self.ui.data_final.setDate(data_hoje)
            self.ui.aviso.hide()
        else:
            self.ui.descricao.setText(self.instance.descricao)
            self.ui.informacoes.setPlainText(self.instance.informacoes)
            self.ui.data_inicial.setDate(
                datetime_qdate(self.instance.data_inicial)
                )
            self.ui.data_final.setDate(
                datetime_qdate(self.instance.data_final)
                )
            if self.instance.indeterminado:
                self.ui.indeterminado.setCheckState(QtCore.Qt.Checked)
            else:
                self.ui.indeterminado.setCheckState(QtCore.Qt.Unchecked)
            if self.instance.tratamento_saude:
                self.ui.tratamento_saude.setCheckState(QtCore.Qt.Checked)
            else:
                self.ui.tratamento_saude.setCheckState(QtCore.Qt.Unchecked)
            # Licença para acompanhar pessoa doente na família
            if self.instance.acompanhamento:
                self.ui.acompanhamento.setCheckState(QtCore.Qt.Checked)
            else:
                self.ui.acompanhamento.setCheckState(QtCore.Qt.Unchecked)
            # Impedir alterações caso esteja excluido ou exista avaliação coincidente 
            # que esteja em andamento ou finalizada.
            if self.instance.data_exclusao != None or self.instance.servidor.data_exclusao != None:
                self.ui.salvar.setEnabled(False)
                self.ui.excluir.setEnabled(False)
                if self.instance.data_exclusao != None:
                    self.ui.aviso.setText(
                        '<p align="center"><span style=" color:#ff0000;">Excluído.</span></p>'
                    )
                elif self.instance.servidor.data_exclusao != None:
                    self.ui.aviso.setText(
                        '<p align="center"><span style=" color:#ff0000;">'
                        'Servidor excluído.'
                        '</span></p>'
                    )
            # TODO: Corrigir após regularização da comissão
            avaliacao_coincidente = False #self.avaliacao_coincidente()
            self.ui.aviso.hide()
            if avaliacao_coincidente:
                if avaliacao_coincidente.ultimo_tramite() is None:
                    self.ui.aviso.hide()
                else:
                    self.ui.salvar.setEnabled(False)
                    self.ui.excluir.setEnabled(False)

    def teste_unica(self, windows):
        if windows.instance.id == self.instance.id:
            return True
        return False

    def salvar(self):
        data_inicial = qdate_datetime(self.ui.data_inicial.date())
        data_final = qdate_datetime(self.ui.data_final.date())
        descricao = self.ui.descricao.text()
        indeterminado = self.ui.indeterminado.isChecked()
        tratamento_saude = self.ui.tratamento_saude.isChecked()
        acompanhamento = self.ui.acompanhamento.isChecked()
        if not descricao:
            return Alerta(self, text="Descrição não foi informada.").exec_()
        elif data_inicial > data_final and not indeterminado:
            return Alerta(self, text="Data inicial é maior que data final").exec_()

        avaliacao_coincidente = self.avaliacao_coincidente(data_inicial)
        eventos_concidentes = Evento.select2().where(
            (Evento.data_inicial<=data_inicial) & 
            (Evento.data_final>=data_final) &
            (Evento.id!=self.instance.id) &
            (Evento.servidor==self.instance.servidor)
        ).count()
        if avaliacao_coincidente is None:
            return Alerta(self, text="Período não coincide com nenhuma avaliação").exec_()
        elif self.impedimento(avaliacao_coincidente):
            return 
        elif eventos_concidentes > 0:
            return Alerta(self, text="Existe evento no período selecionado. Verifique.").exec_()

        self.instance.servidor = self.instance.servidor
        self.instance.informacoes = self.ui.informacoes.toPlainText()
        self.instance.descricao = descricao
        self.instance.indeterminado = indeterminado
        self.instance.tratamento_saude = tratamento_saude
        self.instance.acompanhamento = acompanhamento
        self.instance.data_inicial = data_inicial
        self.instance.data_final = data_final
        self.instance.total = (
            self.instance.data_final - self.instance.data_inicial
            ).days + 1
        with DataBase().obter_database().atomic():
            self.instance.save()
            mant_ser = MantenedorServidor(self.instance.servidor)
            mant_ser.calcular_prazos_intersticios()
            mant_ser.salvar()
        self.enviar_sinal_atualizacao(self.instance)
        self.accept()
