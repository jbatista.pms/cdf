from datetime import datetime

from peewee import *

from QtUtils.db import ModelBase
from QtUtils.historico.models import Historico

from avaliacao.models import Avaliacao
from servidor.models import Servidor


class Evento(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, on_delete='CASCADE')
    servidor = ForeignKeyField(
        Servidor,
        related_name='eventos',
        on_delete='CASCADE',
        )
    descricao = TextField()
    informacoes = TextField()
    data_inicial = DateField()
    data_final = DateField(null=True)
    total = IntegerField(null=True)
    avaliacoes = ManyToManyField(Avaliacao, backref='eventos')
    # Controle
    indeterminado = BooleanField()
    tratamento_saude = BooleanField(null=True, default=False)
    acompanhamento = BooleanField(null=True, default=False)

    class Meta:
        order_by = ['-data_inicial',]

    def __str__(self):
        return f"{self.descricao} ({self.data_inicial}/{self.data_final})"

    def exportar_extras():
        return ['servidor', 'servidor.cargo', 'servidor.lotacao']
    
    def exportar_ignorar():
        return ModelBase.exportar_ignorar()
    
    @classmethod
    def ha_indeterminados(cls, data_limite: datetime, servidor: Servidor) -> bool:
        eventos = cls.select2().where(
            (cls.data_inicial<=data_limite) &
            (cls.indeterminado==True) &
            (cls.servidor==servidor)
        )
        return eventos.count() > 0
    
    def ignorar_campos_mudancas():
        return Historico.ignorar_campos_mudancas() + [
            'data_base','servidor','total','avaliacao','intersticio'
            ]
    
    @classmethod
    def select2(cls, excluidos=False, *args, **kwargs):
        return cls.select(*args, **kwargs).where(
            (cls.origem.is_null(True)) &
            (cls.data_exclusao.is_null(not excluidos))
        ).order_by(cls.data_inicial.desc())

AvaliacaoEventoThrough = Evento.avaliacoes.get_through_model()