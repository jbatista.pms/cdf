# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'evento.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(509, 453)
        Dialog.setMinimumSize(QSize(509, 453))
        Dialog.setMaximumSize(QSize(509, 453))
        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout_3 = QGridLayout()
#ifndef Q_OS_MAC
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
#endif
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label, 0, 0, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_3, 1, 0, 1, 1)

        self.servidor = QLabel(Dialog)
        self.servidor.setObjectName(u"servidor")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.servidor.sizePolicy().hasHeightForWidth())
        self.servidor.setSizePolicy(sizePolicy1)

        self.gridLayout_3.addWidget(self.servidor, 0, 1, 1, 2)

        self.matricula = QLabel(Dialog)
        self.matricula.setObjectName(u"matricula")
        sizePolicy1.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy1)

        self.gridLayout_3.addWidget(self.matricula, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_3)

        self.groupBox_2 = QGroupBox(Dialog)
        self.groupBox_2.setObjectName(u"groupBox_2")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.groupBox_2.sizePolicy().hasHeightForWidth())
        self.groupBox_2.setSizePolicy(sizePolicy2)
        self.verticalLayout = QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_4 = QLabel(self.groupBox_2)
        self.label_4.setObjectName(u"label_4")

        self.verticalLayout.addWidget(self.label_4)

        self.descricao = QLineEdit(self.groupBox_2)
        self.descricao.setObjectName(u"descricao")

        self.verticalLayout.addWidget(self.descricao)

        self.tratamento_saude = QCheckBox(self.groupBox_2)
        self.tratamento_saude.setObjectName(u"tratamento_saude")

        self.verticalLayout.addWidget(self.tratamento_saude)

        self.acompanhamento = QCheckBox(self.groupBox_2)
        self.acompanhamento.setObjectName(u"acompanhamento")

        self.verticalLayout.addWidget(self.acompanhamento)

        self.label_2 = QLabel(self.groupBox_2)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout.addWidget(self.label_2)

        self.informacoes = QTextEdit(self.groupBox_2)
        self.informacoes.setObjectName(u"informacoes")
        self.informacoes.setTabChangesFocus(True)

        self.verticalLayout.addWidget(self.informacoes)


        self.verticalLayout_2.addWidget(self.groupBox_2)

        self.groupBox = QGroupBox(Dialog)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.data_final = QDateEdit(self.groupBox)
        self.data_final.setObjectName(u"data_final")
        sizePolicy1.setHeightForWidth(self.data_final.sizePolicy().hasHeightForWidth())
        self.data_final.setSizePolicy(sizePolicy1)
        self.data_final.setCalendarPopup(True)

        self.gridLayout.addWidget(self.data_final, 1, 3, 1, 1)

        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy3)

        self.gridLayout.addWidget(self.label_5, 1, 0, 1, 1)

        self.data_inicial = QDateEdit(self.groupBox)
        self.data_inicial.setObjectName(u"data_inicial")
        sizePolicy1.setHeightForWidth(self.data_inicial.sizePolicy().hasHeightForWidth())
        self.data_inicial.setSizePolicy(sizePolicy1)
        self.data_inicial.setCalendarPopup(True)

        self.gridLayout.addWidget(self.data_inicial, 1, 1, 1, 1)

        self.label_6 = QLabel(self.groupBox)
        self.label_6.setObjectName(u"label_6")
        sizePolicy3.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy3)

        self.gridLayout.addWidget(self.label_6, 1, 2, 1, 1)

        self.indeterminado = QCheckBox(self.groupBox)
        self.indeterminado.setObjectName(u"indeterminado")

        self.gridLayout.addWidget(self.indeterminado, 0, 0, 1, 4)


        self.verticalLayout_2.addWidget(self.groupBox)

        self.aviso = QLabel(Dialog)
        self.aviso.setObjectName(u"aviso")
        self.aviso.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.aviso)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        sizePolicy3.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy3)
        self.widget.setLayoutDirection(Qt.RightToLeft)
        self.gridLayout_2 = QGridLayout(self.widget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_2, 1, 0, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer, 1, 7, 1, 1)

        self.historico = QPushButton(self.widget)
        self.historico.setObjectName(u"historico")

        self.gridLayout_2.addWidget(self.historico, 1, 3, 1, 1)

        self.salvar = QPushButton(self.widget)
        self.salvar.setObjectName(u"salvar")
        sizePolicy4 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.salvar, 1, 2, 1, 1)

        self.excluir = QPushButton(self.widget)
        self.excluir.setObjectName(u"excluir")
        sizePolicy4.setHeightForWidth(self.excluir.sizePolicy().hasHeightForWidth())
        self.excluir.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.excluir, 1, 5, 1, 1)

        self.fechar = QPushButton(self.widget)
        self.fechar.setObjectName(u"fechar")
        sizePolicy4.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.fechar, 1, 6, 1, 1)


        self.verticalLayout_2.addWidget(self.widget)

        QWidget.setTabOrder(self.descricao, self.tratamento_saude)
        QWidget.setTabOrder(self.tratamento_saude, self.acompanhamento)
        QWidget.setTabOrder(self.acompanhamento, self.informacoes)
        QWidget.setTabOrder(self.informacoes, self.indeterminado)
        QWidget.setTabOrder(self.indeterminado, self.data_inicial)
        QWidget.setTabOrder(self.data_inicial, self.data_final)
        QWidget.setTabOrder(self.data_final, self.fechar)
        QWidget.setTabOrder(self.fechar, self.excluir)
        QWidget.setTabOrder(self.excluir, self.historico)
        QWidget.setTabOrder(self.historico, self.salvar)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.salvar)
        self.excluir.released.connect(Dialog.excluir)
        self.indeterminado.toggled.connect(self.data_final.setDisabled)
        self.historico.released.connect(Dialog.historico)
        self.tratamento_saude.toggled.connect(self.acompanhamento.setDisabled)
        self.acompanhamento.toggled.connect(self.tratamento_saude.setDisabled)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Evento", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Servidor:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.servidor.setText(QCoreApplication.translate("Dialog", u"servidor", None))
        self.matricula.setText(QCoreApplication.translate("Dialog", u"matricula", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Dados", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Descri\u00e7\u00e3o:", None))
        self.tratamento_saude.setText(QCoreApplication.translate("Dialog", u"Licen\u00e7a para tratamento de sa\u00fade.", None))
        self.acompanhamento.setText(QCoreApplication.translate("Dialog", u"Licen\u00e7a para acompanhar pessoa com doen\u00e7a na fam\u00edlia.", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Informa\u00e7\u00f5es:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"Per\u00edodo", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Data inicial:", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Data final:", None))
        self.indeterminado.setText(QCoreApplication.translate("Dialog", u"Data final indefinida", None))
        self.aviso.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p align=\"justify\"><span style=\" color:#ff0000;\">N\u00e3o \u00e9 poss\u00edvel alterar ou excluir o evento, pois seu per\u00edodo coincide com o de uma avalia\u00e7\u00e3o em andamento ou completa.</span></p></body></html>", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
    # retranslateUi

