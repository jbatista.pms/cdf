from QtUtils import Definicoes, QtUtils

definicoes_cliente = {
    'abreviatura_nome': 'CDF',
    'classe_janela_principal': 'app.JanelaPrincipal',
    'inicializar_cef': True,
    'nome_aplicacao': 'Comissão de Desenvolvimento Funcional - CDF',
    'modelos': [
        'ComumComissoes.cargo.models.Cargo',
        'ComumComissoes.lotacao.models.Lotacao',
        'servidor.models.Servidor',
        'intersticio.modelos.Intersticio',
        'avaliacao.models.Avaliacao',
        'avaliacao.models.Tramite',
        'evento.models.Evento',
        'evento.models.AvaliacaoEventoThrough',
        'falta.models.Falta',
    ],
    'modo_execucao': Definicoes.MODO_REDE,
    'verificar_atualizacao': True,
}

if __name__ == '__main__':
    if QtUtils.configurar(definicoes_cliente):
        QtUtils.executar()