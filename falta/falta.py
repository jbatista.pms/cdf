from datetime import date

from QtUtils import subwindowsbase
from QtUtils.date import qdate_datetime, datetime_qdate
from QtUtils.db import DataBase
from QtUtils.colecoes.excluir import Excluir
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.historico import historico
from QtUtils.text import text_format_uppercase

from avaliacao.models import Avaliacao
from intersticio.modelos import Intersticio
from servidor.mantenedor import MantenedorServidor
from .views.falta import Ui_Dialog


class Control(subwindowsbase.Formulario):
    classe_ui = Ui_Dialog

    def avaliacao_coincidente(self, data_efeito=None):
        data_efeito = data_efeito or self.instance.data_efeito
        return Avaliacao.select2().where(
            (Avaliacao.data_inicial<=data_efeito) &
            (Avaliacao.data_final>=data_efeito) &
            (Intersticio.aberto==True) &
            (Intersticio.servidor==self.instance.servidor) &
            (Intersticio.origem.is_null(True))
        ).first()

    def excluir(self):
        if self.instance and not self.impedimento() and Excluir(self).exec_():
            with DataBase().obter_database().atomic():
                self.instance.excluir_instancia()
                mant_ser = MantenedorServidor(self.instance.servidor)
                mant_ser.calcular_prazos_intersticios()
                mant_ser.salvar()
            self.enviar_sinal_exclusao(self.instance)
            self.accept()
    
    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def impedimento(self, avaliacao=None):
        # Verificar impedimentos para alteração ou exclusão de evento com base
        # na avaliação coincidente
        # TODO: Retirar após regularização pela comissão dos períodos de avaliação
        return False
        avaliacao = avaliacao or self.avaliacao_coincidente()
        while not avaliacao is None:
            if not avaliacao.ultimo_tramite() is None:
                Alerta(self,
                    text="A data de efeito é anterior ou coincide com o período de uma avaliação, "
                        "cujo formulário já foi enviado.\n"
                        "Não será possível inserir ou atualizar esse evento, uma vez que formulários "
                        "já enviados não podem ter seus períodos alterados."
                ).exec_()
                return True
            avaliacao = Avaliacao.select2().where(Avaliacao.anterior==avaliacao).first()
        return False

    def inicializar(self, *args, **kwargs):
        self.ui.servidor.setText(self.instance.servidor.nome)
        self.ui.matricula.setText(self.instance.servidor.matricula)
        text_format_uppercase(self.ui.informacoes)
        if self.instance.id is None:
            self.ui.excluir.hide()
            self.ui.historico.hide()
            self.setWindowTitle("Nova falta")
            self.ui.data_efeito.setDate(datetime_qdate(date.today()))
            self.ui.aviso.hide()
        else:
            self.ui.data_efeito.setDate(datetime_qdate(self.instance.data_efeito))
            self.ui.dias.setValue(self.instance.dias)
            self.ui.informacoes.setPlainText(self.instance.informacoes)
            # Impedir alterações caso esteja excluido ou exista avaliação coincidente 
            # que esteja em andamento ou finalizada.
            # TODO: Corrigir após regularização da comissão
            avaliacao_coincidente = self.avaliacao_coincidente()
            if self.instance.data_exclusao != None or self.instance.servidor.data_exclusao != None:
                self.ui.salvar.setEnabled(False)
                self.ui.excluir.setEnabled(False)
                if self.instance.data_exclusao != None:
                    self.ui.aviso.setText(
                        '<p align="center"><span style=" color:#ff0000;">Excluído.</span></p>'
                    )
                elif self.instance.servidor.data_exclusao != None:
                    self.ui.aviso.setText(
                        '<p align="center"><span style=" color:#ff0000;">'
                        'Servidor excluído.'
                        '</span></p>'
                    )
            elif not avaliacao_coincidente is None:
                self.ui.aviso.hide()
                """
                if self.instance.avaliacao.ultimo_tramite() is None:
                    self.ui.aviso.hide()
                else:
                    self.ui.salvar.setEnabled(False)
                    self.ui.excluir.setEnabled(False)
                """

    def teste_unica(self, windows):
        if windows.instance.id == self.instance.id:
            return True
        return False

    def salvar(self):
        data_efeito = qdate_datetime(self.ui.data_efeito.date())
        dias = self.ui.dias.value()

        self.instance.avaliacao = self.avaliacao_coincidente(data_efeito)
        if self.instance.avaliacao is None:
            return Alerta(self,
                text="Período não coincide com nenhuma avaliação."
                ).exec_()
        elif self.impedimento(self.instance.avaliacao):
            return
        elif not dias:
            return Alerta(self,
                text="Preencher número de dias."
                ).exec_()
        self.instance.servidor = self.instance.servidor
        self.instance.informacoes = self.ui.informacoes.toPlainText()
        self.instance.data_efeito = data_efeito
        self.instance.dias = dias
        with DataBase().obter_database().atomic():
            self.instance.save()
            mant_ser = MantenedorServidor(self.instance.servidor)
            mant_ser.calcular_prazos_intersticios()
            mant_ser.salvar()
        self.enviar_sinal_atualizacao(self.instance)
        self.accept()
