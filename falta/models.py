from peewee import *

from QtUtils.historico.models import Historico
from avaliacao.models import Avaliacao
from servidor.models import Servidor


class Falta(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    servidor = ForeignKeyField(
        Servidor,
        related_name='faltas',
        on_delete='CASCADE',
        )
    data_efeito = DateField()
    dias = IntegerField()	
    informacoes = TextField()
    avaliacao = ForeignKeyField(
        Avaliacao,
        related_name='faltas',
        null=True,
        on_delete='CASCADE',
    )

    def __repr__(self):
        return '<Falta data_efeito=%s, dias=%i>' % (self.data_efeito, self.dias)
    
    def __str__(self):
        return "%i dias de falta" % self.dias
    
    def ignorar_campos_mudancas():
        return Historico.ignorar_campos_mudancas() + [
            'data_base','intersticio','avaliacao','servidor'
            ]
    
    def select2(excluidos=False, *args, **kwargs):
        return Falta.select(*args, **kwargs).where(
            (Falta.origem.is_null(True)) &
            (Falta.data_exclusao.is_null(not excluidos))
        ).order_by(Falta.data_efeito.desc())