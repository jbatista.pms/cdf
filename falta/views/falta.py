# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'falta.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(509, 307)
        Dialog.setMinimumSize(QSize(509, 307))
        Dialog.setMaximumSize(QSize(509, 307))
        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.gridLayout_4 = QGridLayout()
#ifndef Q_OS_MAC
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
#endif
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.servidor = QLabel(Dialog)
        self.servidor.setObjectName(u"servidor")
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.servidor.sizePolicy().hasHeightForWidth())
        self.servidor.setSizePolicy(sizePolicy)

        self.gridLayout_4.addWidget(self.servidor, 0, 1, 1, 1)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy1)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label, 0, 0, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        sizePolicy1.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy1)
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_3, 1, 0, 1, 1)

        self.matricula = QLabel(Dialog)
        self.matricula.setObjectName(u"matricula")
        sizePolicy.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy)

        self.gridLayout_4.addWidget(self.matricula, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_4)

        self.groupBox_2 = QGroupBox(Dialog)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_3 = QGridLayout(self.groupBox_2)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label_5 = QLabel(self.groupBox_2)
        self.label_5.setObjectName(u"label_5")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy2)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_5, 0, 2, 1, 1)

        self.data_efeito = QDateEdit(self.groupBox_2)
        self.data_efeito.setObjectName(u"data_efeito")
        self.data_efeito.setCalendarPopup(True)

        self.gridLayout_3.addWidget(self.data_efeito, 0, 1, 1, 1)

        self.dias = QSpinBox(self.groupBox_2)
        self.dias.setObjectName(u"dias")
        self.dias.setMinimum(1)
        self.dias.setMaximum(100)

        self.gridLayout_3.addWidget(self.dias, 0, 3, 1, 1)

        self.label_4 = QLabel(self.groupBox_2)
        self.label_4.setObjectName(u"label_4")
        sizePolicy2.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy2)

        self.gridLayout_3.addWidget(self.label_4, 0, 0, 1, 1)

        self.label_2 = QLabel(self.groupBox_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label_2, 3, 0, 1, 4)

        self.informacoes = QTextEdit(self.groupBox_2)
        self.informacoes.setObjectName(u"informacoes")
        self.informacoes.setTabChangesFocus(True)

        self.gridLayout_3.addWidget(self.informacoes, 4, 0, 1, 4)


        self.verticalLayout_2.addWidget(self.groupBox_2)

        self.aviso = QLabel(Dialog)
        self.aviso.setObjectName(u"aviso")
        self.aviso.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.aviso)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy3)
        self.widget.setLayoutDirection(Qt.RightToLeft)
        self.gridLayout_2 = QGridLayout(self.widget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.fechar = QPushButton(self.widget)
        self.fechar.setObjectName(u"fechar")
        sizePolicy4 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.fechar, 1, 4, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_2, 1, 0, 1, 1)

        self.salvar = QPushButton(self.widget)
        self.salvar.setObjectName(u"salvar")
        sizePolicy4.setHeightForWidth(self.salvar.sizePolicy().hasHeightForWidth())
        self.salvar.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.salvar, 1, 2, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer, 1, 6, 1, 1)

        self.historico = QPushButton(self.widget)
        self.historico.setObjectName(u"historico")

        self.gridLayout_2.addWidget(self.historico, 1, 3, 1, 1)

        self.excluir = QPushButton(self.widget)
        self.excluir.setObjectName(u"excluir")
        sizePolicy4.setHeightForWidth(self.excluir.sizePolicy().hasHeightForWidth())
        self.excluir.setSizePolicy(sizePolicy4)

        self.gridLayout_2.addWidget(self.excluir, 1, 5, 1, 1)


        self.verticalLayout_2.addWidget(self.widget)

        QWidget.setTabOrder(self.data_efeito, self.dias)
        QWidget.setTabOrder(self.dias, self.informacoes)
        QWidget.setTabOrder(self.informacoes, self.excluir)
        QWidget.setTabOrder(self.excluir, self.fechar)
        QWidget.setTabOrder(self.fechar, self.historico)
        QWidget.setTabOrder(self.historico, self.salvar)

        self.retranslateUi(Dialog)
        self.fechar.released.connect(Dialog.reject)
        self.salvar.released.connect(Dialog.salvar)
        self.excluir.released.connect(Dialog.excluir)
        self.historico.released.connect(Dialog.historico)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Falta", None))
        self.servidor.setText(QCoreApplication.translate("Dialog", u"servidor", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Servidor:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.matricula.setText(QCoreApplication.translate("Dialog", u"matricula", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Dialog", u"Dados", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Dias:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Data de efeito:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Informa\u00e7\u00f5es:", None))
        self.aviso.setText(QCoreApplication.translate("Dialog", u"<html><head/><body><p align=\"justify\"><span style=\" color:#ff0000;\">N\u00e3o \u00e9 poss\u00edvel alterar ou excluir a falta, pois sua data de efeito est\u00e1 dentro do per\u00edodo de uma avalia\u00e7\u00e3o em andamento ou completa.</span></p></body></html>", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.historico.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
    # retranslateUi

