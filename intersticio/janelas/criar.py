# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'criar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(632, 387)
        Dialog.setMinimumSize(QSize(632, 387))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignJustify|Qt.AlignVCenter)
        self.label.setWordWrap(True)

        self.verticalLayout.addWidget(self.label)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignJustify|Qt.AlignVCenter)
        self.label_2.setWordWrap(True)

        self.verticalLayout.addWidget(self.label_2)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout.addWidget(self.label_3)

        self.tabela = QTableWidget(Dialog)
        if (self.tabela.columnCount() < 4):
            self.tabela.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        if (self.tabela.rowCount() < 1):
            self.tabela.setRowCount(1)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.tabela.setVerticalHeaderItem(0, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.tabela.setItem(0, 0, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        self.tabela.setItem(0, 1, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        self.tabela.setItem(0, 2, __qtablewidgetitem7)
        self.tabela.setObjectName(u"tabela")
        self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tabela.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabela.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.horizontalHeader().setHighlightSections(False)
        self.tabela.verticalHeader().setVisible(False)

        self.verticalLayout.addWidget(self.tabela)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.btn_executar = QPushButton(self.widget_2)
        self.btn_executar.setObjectName(u"btn_executar")

        self.horizontalLayout.addWidget(self.btn_executar)

        self.pushButton = QPushButton(self.widget_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addWidget(self.widget_2)

        QWidget.setTabOrder(self.tabela, self.btn_executar)
        QWidget.setTabOrder(self.btn_executar, self.pushButton)

        self.retranslateUi(Dialog)
        self.btn_executar.released.connect(Dialog.executar)
        self.pushButton.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Criar interst\u00edcio", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"No procedimento, \u00e9 apurado todos o servidores ativos sem registro de interst\u00edcios no sistema, criando novos nestes casos.", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Abaixo estar\u00e3o listados todos os servidores, que ap\u00f3s a execu\u00e7\u00e3o, passar\u00e3o a contar com novos registros de interst\u00edcios.", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"A a\u00e7\u00e3o n\u00e3o poder\u00e1 ser desfeita.", None))
        ___qtablewidgetitem = self.tabela.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"Nome", None));
        ___qtablewidgetitem1 = self.tabela.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula", None));
        ___qtablewidgetitem2 = self.tabela.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Dialog", u"Cargo", None));
        ___qtablewidgetitem3 = self.tabela.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o", None));
        ___qtablewidgetitem4 = self.tabela.verticalHeaderItem(0)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Dialog", u"1", None));

        __sortingEnabled = self.tabela.isSortingEnabled()
        self.tabela.setSortingEnabled(False)
        ___qtablewidgetitem5 = self.tabela.item(0, 0)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Dialog", u"JOAO BATISTA DOS SANTOS FILHO", None));
        ___qtablewidgetitem6 = self.tabela.item(0, 1)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("Dialog", u"10/2231-07", None));
        ___qtablewidgetitem7 = self.tabela.item(0, 2)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("Dialog", u"AGENTE ADMINISTRATIVO", None));
        self.tabela.setSortingEnabled(__sortingEnabled)

        self.btn_executar.setText(QCoreApplication.translate("Dialog", u"Executar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
    # retranslateUi

