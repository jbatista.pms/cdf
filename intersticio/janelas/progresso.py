# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'progresso.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(400, 95)
        Dialog.setMinimumSize(QSize(400, 95))
        Dialog.setMaximumSize(QSize(400, 95))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.mensagem = QLabel(Dialog)
        self.mensagem.setObjectName(u"mensagem")
        self.mensagem.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.mensagem)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_3)

        self.progresso = QProgressBar(Dialog)
        self.progresso.setObjectName(u"progresso")
        self.progresso.setValue(0)

        self.verticalLayout.addWidget(self.progresso)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Criando interst\u00edcios", None))
        self.mensagem.setText(QCoreApplication.translate("Dialog", u"Servidor 1 de 100", None))
    # retranslateUi

