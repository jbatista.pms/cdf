# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'relatorio_intersticios_cumpridos.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(876, 556)
        Form.setMinimumSize(QSize(876, 556))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.servidores = QTableWidget(Form)
        if (self.servidores.columnCount() < 4):
            self.servidores.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.servidores.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        self.servidores.setObjectName(u"servidores")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.servidores.sizePolicy().hasHeightForWidth())
        self.servidores.setSizePolicy(sizePolicy)
        self.servidores.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.servidores.setAlternatingRowColors(True)
        self.servidores.setSelectionMode(QAbstractItemView.SingleSelection)
        self.servidores.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.servidores.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.servidores.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.servidores.setSortingEnabled(True)
        self.servidores.horizontalHeader().setHighlightSections(False)
        self.servidores.horizontalHeader().setProperty("showSortIndicator", True)
        self.servidores.horizontalHeader().setStretchLastSection(False)
        self.servidores.verticalHeader().setVisible(False)

        self.gridLayout.addWidget(self.servidores, 0, 3, 2, 1)

        self.fechar = QPushButton(Form)
        self.fechar.setObjectName(u"fechar")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.fechar.sizePolicy().hasHeightForWidth())
        self.fechar.setSizePolicy(sizePolicy1)

        self.gridLayout.addWidget(self.fechar, 1, 1, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 1, 0, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Minimum, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_3, 1, 2, 1, 1)

        self.tabWidget = QTabWidget(Form)
        self.tabWidget.setObjectName(u"tabWidget")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy2)
        self.tabWidget.setMaximumSize(QSize(300, 16777215))
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout = QVBoxLayout(self.tab)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.groupBox = QGroupBox(self.tab)
        self.groupBox.setObjectName(u"groupBox")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy3)
        self.gridLayout_4 = QGridLayout(self.groupBox)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.cumpridos_ate = QDateEdit(self.groupBox)
        self.cumpridos_ate.setObjectName(u"cumpridos_ate")
        sizePolicy1.setHeightForWidth(self.cumpridos_ate.sizePolicy().hasHeightForWidth())
        self.cumpridos_ate.setSizePolicy(sizePolicy1)
        self.cumpridos_ate.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.cumpridos_ate, 1, 2, 1, 2)

        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_4.addWidget(self.label_3, 1, 1, 1, 1)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_4.addWidget(self.label_4, 0, 1, 1, 1)

        self.cumpridos_de = QDateEdit(self.groupBox)
        self.cumpridos_de.setObjectName(u"cumpridos_de")
        sizePolicy1.setHeightForWidth(self.cumpridos_de.sizePolicy().hasHeightForWidth())
        self.cumpridos_de.setSizePolicy(sizePolicy1)
        self.cumpridos_de.setCalendarPopup(True)

        self.gridLayout_4.addWidget(self.cumpridos_de, 0, 2, 1, 1)


        self.verticalLayout.addWidget(self.groupBox)

        self.groupBox_4 = QGroupBox(self.tab)
        self.groupBox_4.setObjectName(u"groupBox_4")
        sizePolicy3.setHeightForWidth(self.groupBox_4.sizePolicy().hasHeightForWidth())
        self.groupBox_4.setSizePolicy(sizePolicy3)
        self.gridLayout_6 = QGridLayout(self.groupBox_4)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.lotacao = QComboBox(self.groupBox_4)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy3.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy3)
        self.lotacao.setMinimumSize(QSize(250, 0))

        self.gridLayout_6.addWidget(self.lotacao, 1, 0, 1, 1)

        self.label = QLabel(self.groupBox_4)
        self.label.setObjectName(u"label")
        sizePolicy4 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy4)

        self.gridLayout_6.addWidget(self.label, 0, 0, 1, 1)

        self.label_2 = QLabel(self.groupBox_4)
        self.label_2.setObjectName(u"label_2")
        sizePolicy4.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy4)

        self.gridLayout_6.addWidget(self.label_2, 2, 0, 1, 1)

        self.sublotacao = QComboBox(self.groupBox_4)
        self.sublotacao.setObjectName(u"sublotacao")
        sizePolicy3.setHeightForWidth(self.sublotacao.sizePolicy().hasHeightForWidth())
        self.sublotacao.setSizePolicy(sizePolicy3)

        self.gridLayout_6.addWidget(self.sublotacao, 3, 0, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_4)

        self.groupBox_2 = QGroupBox(self.tab)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.verticalLayout_3 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.avs_sem_restricao = QRadioButton(self.groupBox_2)
        self.avs_sem_restricao.setObjectName(u"avs_sem_restricao")
        self.avs_sem_restricao.setChecked(True)

        self.verticalLayout_3.addWidget(self.avs_sem_restricao)

        self.avs_possui_tres = QRadioButton(self.groupBox_2)
        self.avs_possui_tres.setObjectName(u"avs_possui_tres")

        self.verticalLayout_3.addWidget(self.avs_possui_tres)

        self.avs_possui_alguma_nenhuma = QRadioButton(self.groupBox_2)
        self.avs_possui_alguma_nenhuma.setObjectName(u"avs_possui_alguma_nenhuma")

        self.verticalLayout_3.addWidget(self.avs_possui_alguma_nenhuma)


        self.verticalLayout.addWidget(self.groupBox_2)

        self.groupBox_3 = QGroupBox(self.tab)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.formLayout = QFormLayout(self.groupBox_3)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setFieldGrowthPolicy(QFormLayout.AllNonFixedFieldsGrow)
        self.fin_progressao = QRadioButton(self.groupBox_3)
        self.fin_progressao.setObjectName(u"fin_progressao")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.fin_progressao)

        self.fin_promocao = QRadioButton(self.groupBox_3)
        self.fin_promocao.setObjectName(u"fin_promocao")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.fin_promocao)

        self.radioButton_3 = QRadioButton(self.groupBox_3)
        self.radioButton_3.setObjectName(u"radioButton_3")
        self.radioButton_3.setChecked(True)

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.radioButton_3)


        self.verticalLayout.addWidget(self.groupBox_3)

        self.filtrar = QPushButton(self.tab)
        self.filtrar.setObjectName(u"filtrar")

        self.verticalLayout.addWidget(self.filtrar)

        self.total_registros = QLabel(self.tab)
        self.total_registros.setObjectName(u"total_registros")
        sizePolicy3.setHeightForWidth(self.total_registros.sizePolicy().hasHeightForWidth())
        self.total_registros.setSizePolicy(sizePolicy3)

        self.verticalLayout.addWidget(self.total_registros)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_2 = QVBoxLayout(self.tab_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.btn_exportar = QPushButton(self.tab_2)
        self.btn_exportar.setObjectName(u"btn_exportar")

        self.verticalLayout_2.addWidget(self.btn_exportar)

        self.label_5 = QLabel(self.tab_2)
        self.label_5.setObjectName(u"label_5")

        self.verticalLayout_2.addWidget(self.label_5)

        self.pushButton = QPushButton(self.tab_2)
        self.pushButton.setObjectName(u"pushButton")

        self.verticalLayout_2.addWidget(self.pushButton)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 3)

        QWidget.setTabOrder(self.tabWidget, self.cumpridos_de)
        QWidget.setTabOrder(self.cumpridos_de, self.cumpridos_ate)
        QWidget.setTabOrder(self.cumpridos_ate, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.avs_sem_restricao)
        QWidget.setTabOrder(self.avs_sem_restricao, self.avs_possui_tres)
        QWidget.setTabOrder(self.avs_possui_tres, self.avs_possui_alguma_nenhuma)
        QWidget.setTabOrder(self.avs_possui_alguma_nenhuma, self.filtrar)
        QWidget.setTabOrder(self.filtrar, self.btn_exportar)
        QWidget.setTabOrder(self.btn_exportar, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.servidores)
        QWidget.setTabOrder(self.servidores, self.fechar)

        self.retranslateUi(Form)
        self.fechar.released.connect(Form.close)
        self.filtrar.released.connect(Form.filtrar)
        self.btn_exportar.released.connect(Form.exportar)
        self.pushButton.released.connect(Form.imprimir_relatorio)
        self.servidores.cellDoubleClicked.connect(Form.abrir_servidor)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Relat\u00f3rio de interst\u00edcios cumpridos", None))
        ___qtablewidgetitem = self.servidores.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Servidor", None));
        ___qtablewidgetitem1 = self.servidores.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Matr\u00edcula", None));
        ___qtablewidgetitem2 = self.servidores.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem3 = self.servidores.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Aproveitamento", None));
        self.fechar.setText(QCoreApplication.translate("Form", u"Fechar", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Per\u00edodo", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"At\u00e9:", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"A partir de:", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("Form", u"Local de trabalho", None))
        self.label.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o:", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Avalia\u00e7\u00f5es", None))
        self.avs_sem_restricao.setText(QCoreApplication.translate("Form", u"Sem crit\u00e9rio", None))
        self.avs_possui_tres.setText(QCoreApplication.translate("Form", u"Possui as tr\u00eas", None))
        self.avs_possui_alguma_nenhuma.setText(QCoreApplication.translate("Form", u"Possui alguma ou nenhuma", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form", u"Finalidade", None))
        self.fin_progressao.setText(QCoreApplication.translate("Form", u"Progress\u00e3o (>=60%)", None))
        self.fin_promocao.setText(QCoreApplication.translate("Form", u"Promo\u00e7\u00e3o (>=75%)", None))
        self.radioButton_3.setText(QCoreApplication.translate("Form", u"Sem crit\u00e9rio", None))
        self.filtrar.setText(QCoreApplication.translate("Form", u"Filtrar", None))
        self.total_registros.setText(QCoreApplication.translate("Form", u"0 interst\u00edcios encontrados.", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Form", u"Filtros", None))
        self.btn_exportar.setText(QCoreApplication.translate("Form", u"Exportar", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Exportar dados dos interst\u00edcios em um arquivo CSV.", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"Imprimir relat\u00f3rio", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Ferramentas", None))
    # retranslateUi

