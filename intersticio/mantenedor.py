from datetime import timedelta
from loguru import logger

from ComumComissoes.cargo.models import Cargo
from QtUtils.db import DataBase
from QtUtils.prazos import Prazo

from avaliacao.models import Avaliacao
from avaliacao.mantenedor import MantenedorAvaliacao
from evento.models import Evento
from intersticio.modelos import Intersticio


class IntersticioMantenedor(object):
    __intersticio = None
    
    def __init__(self, intersticio):
        self.__intersticio = intersticio
    
    def __estipular_data_inicial(self):
        if self.__intersticio.anterior is None:
            self.__intersticio.data_inicial = self.__intersticio.servidor.data_base
        else:
            self.__intersticio.data_inicial = self.__intersticio.anterior.data_final
            self.__intersticio.data_inicial += timedelta(days=1)
        logger.debug(
            f"Data inicial de '{self.__intersticio}' estipulada "
            f"para {self.__intersticio.data_inicial}."
        )
    
    def apurar_aproveitamento(self):
        avs = self.avaliacoes_completas()
        if len(avs) < 3:
            self.__intersticio.aproveitamento = 0
        else:
            # A pontuação máxima em uma avaliação para os cargos de ensino médio
            # é de 996 pontos. (Quadro 2, Anexo IV, Decreto nº 3.566/16)
            # Portanto, em um interstício o máximo obtido é de 2.988, ao contrário
            # dos 3.000 para ensino fundamental e superior.
            # Acrecenta-se a pontuação total 300 pontos possíveis no quesito 
            # disciplina nas três avaliações.
            if self.__intersticio.servidor.cargo.nivel_escolaridade == Cargo.NIVEL_MEDIO:
                pontuacao_maxima = 3288
            else:
                pontuacao_maxima = 3300
            pontuacao = sum([a.pontos_total for a in avs]) / pontuacao_maxima
            self.__intersticio.aproveitamento = pontuacao * 100

    def avaliacoes(self):
        return Avaliacao.select2().filter(Avaliacao.intersticio == self.__intersticio)
    
    def avaliacoes_completas(self):
        avs = Avaliacao.select2().filter(
            (Avaliacao.pontos_total > 0) &
            (Avaliacao.intersticio == self.__intersticio)
        )
        return [a for a in avs if a.pronto_para_intersticio()]

    def calcular_prazo(self):
        with DataBase().obter_database().atomic():
            self.__estipular_data_inicial()
            self.salvar()
            # Calcular prazos de avaliações
            avaliacoes = []
            for av in self.avaliacoes().objects():
                man_av = MantenedorAvaliacao(av)
                man_av.calcular_prazo()
                man_av.salvar()
                avaliacoes.append(man_av.avaliacao)
            # Obter prazo de final de instersício
            self.__intersticio.data_final = avaliacoes[-1].data_final
            self.__intersticio.indeterminado = Evento.ha_indeterminados(
                data_limite=self.__intersticio.data_final,
                servidor=self.__intersticio.servidor,
            )
    
    @classmethod
    def criar(cls, servidor, anterior):
        logger.info(f"Criando novo interstício para o servidor {servidor}.")
        mant = cls(Intersticio(servidor=servidor, anterior=anterior))
        mant.__estipular_data_inicial()
        # Estipular data final
        mant.__intersticio.data_final = Prazo(mant.__intersticio.data_inicial).prorrogar(36)
        logger.debug(
            f"Data final de '{mant.__intersticio}' estipulada "
            f"para {mant.__intersticio.data_final}."
        )
        mant.salvar()
        return mant
    
    @property
    def intersticio(self):
        return self.__intersticio
    
    def salvar(self):
        self.__intersticio.save()