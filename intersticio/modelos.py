from peewee import *
from QtUtils.historico.models import Historico
from servidor.models import Servidor


class Intersticio(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    servidor = ForeignKeyField(
        Servidor,
        related_name='intersticios',
        on_delete='CASCADE',
        )
    data_inicial = DateField()
    data_final = DateField()
    aberto = BooleanField(default=True)
    anterior = ForeignKeyField(
        'self',
        null=True,
        on_delete='SET NULL',
    )
    posterior = ForeignKeyField(
        'self',
        null=True,
        on_delete='SET NULL',
    )
    indeterminado = BooleanField(default=False)
    aproveitamento = DecimalField(default=0, max_digits=5, decimal_places=2)

    def __str__(self):
        return 'Interstício de {servidor} no período de {datai} a {dataf}'.format(
            servidor=self.servidor,
            datai=self.data_inicial,
            dataf=self.data_final,
        )
    
    @property
    def aproveitamento_str(self):
        return str(self.aproveitamento).replace('.', ',') + '%'
    
    @staticmethod
    def exportar_ignorar():
        return Historico.exportar_ignorar() + ['anterior','data_base']

    @classmethod
    def selecao_abertos(cls):
        return cls.select2().filter(cls.aberto==True)
    
    @classmethod
    def selecao_cumpridos(cls, de=None, ate=None, ser_inativo=False):
        selecao = cls.selecao_abertos().filter(
            cls.indeterminado==False,
        ).join(Servidor).filter(
            (Servidor.inativo==ser_inativo) &
            (Servidor.data_exclusao.is_null(True))
        )
        # Ordenar por nome de servidores
        selecao = selecao.order_by(Servidor.nome.asc())
        # Instersticios cumpridos a partir de data
        if de:
            selecao = selecao.where(cls.data_final>de)
        if ate:
            selecao = selecao.where(cls.data_final<ate)
        return selecao
    
    @classmethod
    def select2(cls, *args, **kwargs):
        return cls.select(*args, **kwargs).where(
            cls.origem.is_null(True)
        ).order_by(cls.data_inicial.desc())