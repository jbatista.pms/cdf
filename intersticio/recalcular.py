from PySide2 import QtCore

from QtUtils import qt, subwindowsbase
from QtUtils.colecoes.dialogos import Informacao
from QtUtils.db import DataBase

from servidor.mantenedor import MantenedorServidor
from servidor.models import Servidor

from .janelas import recalcular, progresso


class RecalcularIntersticioDialogo(subwindowsbase.Formulario):
    __servidores = []
    __total_registros = 0
    classe_ui = recalcular.Ui_Dialog

    def __popular_tabela(self):
        self.__total_registros = len(self.__servidores)
        self.ui.tabela.setSortingEnabled(False)
        self.ui.tabela.setRowCount(self.__total_registros)
        for n, servidor in enumerate(self.__servidores):
            # Nome
            item = qt.QTableWidgetItem()
            item.setText(servidor.nome)
            self.ui.tabela.setItem(n, 0, item)
            # Matrícula
            item = qt.QTableWidgetItem()
            item.setText(servidor.matricula)
            item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            self.ui.tabela.setItem(n, 1, item)
            # Cargo
            item = qt.QTableWidgetItem()
            item.setText(str(servidor.cargo))
            self.ui.tabela.setItem(n, 2, item)
            # Lotação
            item = qt.QTableWidgetItem()
            item.setText(str(servidor.lotacao))
            self.ui.tabela.setItem(n, 3, item)
        self.ui.tabela.resizeColumnsToContents()
    
    def __obter_servidores(self):
        self.__servidores = Servidor.selecao_ativos()
        self.ui.btn_executar.setEnabled(bool(self.__servidores))
    
    def executar(self):
        if RecalcularIntersticioProgressoDialogo(parent=self, servidores=self.__servidores).exec_():
            info = Informacao(parent=self)
            info.setText("{} servidores processados!".format(self.__total_registros))
            info.exec_()

    def inicializar(self, *args, **kwargs):
        self.__obter_servidores()
        self.__popular_tabela()


class RecalcularIntersticioThread(QtCore.QThread):
    atualizaProgresso = QtCore.Signal(int)
    atualizaMensagem = QtCore.Signal(str)
    conclusao = QtCore.Signal()

    def __init__(self, servidores):
        self.servidores = servidores
        self.total_servidores = len(servidores)
        QtCore.QThread.__init__(self)
    
    def __atualizar_progresso(self, numero):
        self.atualizaProgresso.emit((numero/self.total_servidores)*100)
    
    def __emitir_mensagem(self, numero):
        msg = "Servidor {numero} de {total}".format(numero=numero, total=self.total_servidores)
        self.atualizaMensagem.emit(msg)
    
    def run(self):
        with DataBase().obter_database().atomic() as db:
            for n, servidor in enumerate(self.servidores):
                self.__emitir_mensagem(n)
                mant_ser = MantenedorServidor(servidor)
                mant_ser.recalcular_intersticios()
                self.__atualizar_progresso(n)
        self.conclusao.emit()


class RecalcularIntersticioProgressoDialogo(subwindowsbase.Formulario):
    __servidores = []
    classe_ui = progresso.Ui_Dialog

    def __init__(self, parent, servidores):
        self.__servidores = servidores
        super().__init__(parent=parent)
        self.setWindowTitle('Recalculando interstícios')

    def atualizar_progress(self, progresso):
        self.ui.progresso.setValue(int(progresso))

    def atualizar_mensagem(self, mensagem):
        self.ui.mensagem.setText(mensagem)

    def conclusao(self):
        self.accept()
    
    def inicializar(self, *args, **kwargs):
        self.th = RecalcularIntersticioThread(servidores=self.__servidores)
        self.th.atualizaProgresso.connect(self.atualizar_progress)
        self.th.atualizaMensagem.connect(self.atualizar_mensagem)
        self.th.conclusao.connect(self.conclusao)
        self.th.start()