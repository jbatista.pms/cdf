import csv
from collections import OrderedDict
from datetime import date
from servidor.models import Servidor
from PySide2 import QtCore, QtWidgets

from ComumComissoes.lotacao import controles_uteis
from ComumComissoes.lotacao.models import Lotacao
from QtUtils import qt, subwindowsbase
from QtUtils.colecoes import dialogos
from QtUtils.date import datetime_qdate, qdate_datetime
from QtUtils.relatorio import Relatorio, RelatorioViewSubWindows

from avaliacao.models import Avaliacao
from servidor.servidor import Control as JanelaServidor
from .janelas.relatorio_intersticios_cumpridos import Ui_Form
from .modelos import Intersticio


class IntersticioRelatorioJanela(subwindowsbase.Listar, controles_uteis.LotacaoSelecao):
    classe_ui = Ui_Form
    intersticios = None # Lista de interstícios filtrados
    lista_dict_inters = None # Dicionário com interstícios listados para obtenção
    filtros = OrderedDict() # Dados de filtros para relatório

    def __init__(self, parent, *args, **kwargs):
        self.lista_dict_inters = {}
        super().__init__(parent=parent, *args, **kwargs)
    
    def __filtro_em_texto(self):
        if not self.filtros:
            return 'Sem filtros.'
        texto = ''
        for k,v in self.filtros.items():
            if texto:
                texto += '; '
            texto += "{k}: {v}".format(k=k,v=v)
        texto += '.'
        return texto
    
    def __filtro_remover(self, nome):
        if nome in self.filtros:
            self.filtros.pop(nome)
    
    def abrir_servidor(self, row, column):
        id_ser = int(self.ui.servidores.verticalHeaderItem(row).text())
        JanelaServidor(parent=self, instance=self.lista_dict_inters[id_ser].servidor).show()

    def dados_avaliacao(self, avaliacao):
        dict_return = OrderedDict()
        dict_return['data_inicial'] = avaliacao.data_inicial
        dict_return['data_final'] = avaliacao.data_final
        dict_return['pontos_chefia'] = avaliacao.pontos_chefia
        dict_return['pontos_servidor'] = avaliacao.pontos_servidor
        dict_return['pontos_disciplina'] = avaliacao.pontos_disciplina
        dict_return['pontos_penalidade'] = avaliacao.pontos_penalidade
        dict_return['pontos_evolucao'] = avaliacao.pontos_evolucao
        dict_return['pontos_total'] = avaliacao.pontos_total
        dict_return['reavaliar'] = avaliacao.reavaliar
        return dict_return

    def dados_intersticio(self, intersticio, exportar=True):
        avaliacoes = Avaliacao.select2().filter(
            intersticio=intersticio,
        ).order_by(
            Avaliacao.data_inicial.asc(),
        )
        dict_return = OrderedDict()
        dict_return['servidor'] = intersticio.servidor.nome
        dict_return['matricula'] = intersticio.servidor.matricula
        dict_return['cargo'] = intersticio.servidor.cargo.nome
        if intersticio.servidor.lotacao.pai is None:
            dict_return['lotacao'] = intersticio.servidor.lotacao.nome
            dict_return['sublotacao'] = ''
        else:
            dict_return['lotacao'] = intersticio.servidor.lotacao.pai.nome
            dict_return['sublotacao'] = intersticio.servidor.lotacao.nome
        dict_return['data_inicial'] = intersticio.data_inicial
        dict_return['data_final'] = intersticio.data_final
        dict_return['aproveitamento'] = str(intersticio.aproveitamento).replace('.',',') + '%'

        if exportar:
            for n,av in enumerate(avaliacoes):
                dados_av = self.dados_avaliacao(avaliacao=av)
                for k,v in dados_av.items():
                    dict_return['av{0}_{1}'.format(n+1,k)] = v
        else:
            dict_return['avaliacoes'] = [
                self.dados_avaliacao(avaliacao=avaliacoes[0]),
                self.dados_avaliacao(avaliacao=avaliacoes[1]),
                self.dados_avaliacao(avaliacao=avaliacoes[2]),
            ]
        return dict_return

    def dados_intersticios(self):
        return [self.dados_intersticio(inter) for inter in self.intersticios]

    def exportar(self):
        # Verificar se há interstícios
        if self.intersticios is None:
            return dialogos.Alerta(
                parent=self,
                text="Não há interstícios para exportar!"
            ).exec_()

        # Obter nome de arquivo de saída
        nome_arquivo = QtWidgets.QFileDialog.getSaveFileName(self,
            "Exportar relatório", '',
            "CSV com cabeçalho (*.csv);;All Files (*)"
        )
        if not nome_arquivo or nome_arquivo[0] == '':
            return
        
        # Abrir arquivo e verificar
        try:
            arq = open(nome_arquivo[0], 'w')
        except PermissionError as error:
            dialogos.Alerta(
                parent=self,
                text="Não foi possível criar o arquivo.\nTalvez esteja aberto.\nVerifique!"
            ).exec_()
            raise error
        
        registros = self.dados_intersticios()
        colunas = registros[0].keys()
        arq_csv = csv.DictWriter(arq, fieldnames=colunas, delimiter=';', lineterminator='\n')
        arq_csv.writeheader()
        arq_csv.writerows(registros)
        arq.close()

    def filtrar(self):
        intersticios = self.filtrar_periodo()
        intersticios = self.filtrar_finalidade(intersticios)
        intersticios = self.filtrar_avaliacao(intersticios)
        intersticios = self.filtrar_lotacao(intersticios)

        self.lista_dict_inters = {i.id:i for i in intersticios}
        self.intersticios = intersticios
        self.popular_tabela()
    
    def filtrar_avaliacao(self, query):
        # Filtrar por avaliações concluidas
        if self.ui.avs_possui_tres.isChecked():
            self.filtros['Avaliações'] = 'Possui as três'
            return query.filter(Intersticio.aproveitamento>0)
        elif self.ui.avs_possui_alguma_nenhuma.isChecked():
            self.filtros['Avaliações'] = 'Possui alguma ou nenhuma'
            return query.filter(Intersticio.aproveitamento==0)
        self.__filtro_remover('Avaliações')
        return query
    
    def filtrar_finalidade(self, query):
        if self.ui.fin_progressao.isChecked():
            self.filtros['Finalidade'] = 'Progressão (média>=60%)'
            return query.filter(Intersticio.aproveitamento>=60)
        elif self.ui.fin_promocao.isChecked():
            self.filtros['Finalidade'] = 'Promoção (média>=75%)'
            return query.filter(Intersticio.aproveitamento>=75)
        self.__filtro_remover('Finalidade')
        return query
    
    def filtrar_lotacao(self, query):
        # Filtrar lotação
        lotacao = self.lotacao_selecionada()
        sublotacao_indice = self.sublotacao_indice()
        if sublotacao_indice>1:
            sub_lotacao = self.sublotacao_selecionada()
            self.filtros['Sublotação'] = str(sub_lotacao)
            return query.where(Servidor.lotacao==sub_lotacao)
        elif sublotacao_indice==0:
            self.filtros['Lotação'] = str(lotacao)
            self.filtros['Sublotação'] = 'Todas'
            query = query.join(Lotacao)
            return query.where(
                (Lotacao.pai==lotacao) | (Servidor.lotacao==lotacao)
            )
        elif lotacao:
            self.filtros['Lotação'] = str(lotacao)
            self.filtros['Sublotação'] = 'Somente'
            return query.where(Servidor.lotacao==lotacao)
        self.filtros['Lotação'] = 'Todas'
        self.__filtro_remover('Sublotação')
        return query
    
    def filtrar_periodo(self):
        # Filtrar pelo período
        de = qdate_datetime(self.ui.cumpridos_de.date())
        ate = qdate_datetime(self.ui.cumpridos_ate.date())
        self.filtros['De'] = de.strftime('%d/%m/%Y')
        self.filtros['Até'] = ate.strftime('%d/%m/%Y')
        return Intersticio.selecao_cumpridos(de=de, ate=ate)
    
    def imprimir_relatorio(self):
        if self.intersticios:
            objetos = [{
                'intersticios': [
                    self.dados_intersticio(i, exportar=False) for i in self.intersticios
                ],
                'total': len(self.intersticios),
                'filtros': self.__filtro_em_texto(),
            }]
            relatorio = Relatorio(
                parent=self, 
                objetos=objetos,
                template='intersticio/documentos/relatorio_intersticios_cumpridos.odt',
            )
            relatorio.exec_()
            if relatorio.documento:
                RelatorioViewSubWindows(self, relatorio.documento).show()

    def inicializar(self, *args, **kwargs):
        self.carregar_lotacoes()
        self.ui.cumpridos_ate.setDate(datetime_qdate(date.today()))
        self.headerH = self.ui.servidores.horizontalHeader()
        self.headerH.setDefaultAlignment(QtCore.Qt.AlignCenter)
    
    def popular_tabela(self):
        total = len(self.intersticios)
        self.ui.total_registros.setText("%i interstícios encontrados." % total)

        self.ui.servidores.setSortingEnabled(False)
        self.ui.servidores.setRowCount(total)
        for n,inter in enumerate(self.intersticios):
            # Id
            item = qt.QTableWidgetItem()
            item.setText(str(inter.id))
            self.ui.servidores.setVerticalHeaderItem(n, item)
            # Nome
            item = qt.QTableWidgetItem()
            item.setText(inter.servidor.nome)
            self.ui.servidores.setItem(n, 0, item)
            # Matrícula
            item = qt.QTableWidgetItem()
            item.setText(inter.servidor.matricula)
            item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            self.ui.servidores.setItem(n, 1, item)
            # Final do interstício
            item = qt.QTableWidgetItem()
            item.setText(inter.strftime('data_final'))
            item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            self.ui.servidores.setItem(n, 2, item)
            # Avaliações
            item = qt.QTableWidgetItem()
            item.setText(inter.aproveitamento_str)
            item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            self.ui.servidores.setItem(n, 3, item)

        #self.ui.servidores.setSortingEnabled(True)
        self.headerH.resizeSection(1,15)
        self.ui.servidores.resizeColumnsToContents()