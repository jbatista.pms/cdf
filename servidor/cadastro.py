from datetime import date
from PySide2 import QtCore

from ComumComissoes.cargo.models import Cargo
from QtUtils.db import DataBase
from QtUtils import subwindowsbase
from QtUtils.date import datetime_qdate, qdate_datetime
from QtUtils.validadores import TextoEmMaisculo
from QtUtils.colecoes.dialogos import Confirmacao
from QtUtils.colecoes.dialogos import Alerta
from ComumComissoes.lotacao import controles_uteis
from .views.alterar import Ui_Dialog
from .models import Servidor
from .mantenedor import MantenedorServidor


class Cadastro(subwindowsbase.Formulario, controles_uteis.LotacaoSelecao):
    classe_ui = Ui_Dialog
    selecao_busca = False

    def accept(self):
        self.instance.nome = self.ui.nome.text()
        self.instance.matricula = self.ui.matricula.text()
        self.instance.data_admissao = qdate_datetime(self.ui.data_admissao.date())
        self.instance.lotacao = self.locatao_resultado()
        self.instance.cargo = self.cargos[self.ui.cargo.currentIndex()]
        self.instance.inativo = self.ui.inativo.isChecked()

        # Teste para saber se todos os campos foram preenchidos
        for campo, valor in self.instance.__data__.items():
            if not valor and not campo in ('inativo','origem','user','data_exclusao'):
                Alerta(self,text="Campo %s não prenchido." % campo).exec_()
                return
        # Salvando alterações
        if self.instance.id:
            self.instance.save()
        else:
            # Se admissão enterior a Lei nº 2.609/15, data base será 01/01/2016,
            # entrada em vigor do estatuto e instituição da progressão.
            if self.instance.data_admissao.year < 2016:
                self.instance.data_base = date(2016,1,1)
            else:
                self.instance.data_base = self.instance.data_admissao
            # Salvando no banco de dados, e criando interstício e novas avaliações
            with DataBase().obter_database().atomic():
                self.instance.save()
                mant_ser = MantenedorServidor(self.instance)
                mant_ser.novo_intersticio()

        self.enviar_sinal_atualizacao(self.instance)
        super().accept()

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', Servidor())

    def inicializar(self, *args, **kwargs):
        self.ui.nome.setValidator(TextoEmMaisculo(self))
        self.ui.matricula.setValidator(TextoEmMaisculo(self))
        self.carregar_lotacoes()
        self.carregar_cargo()
        if self.instance.id != None:
            self.setWindowTitle("Alterar cadastro de servidor")
            self.ui.nome.setText(self.instance.nome)
            self.ui.matricula.setText(self.instance.matricula)
            self.ui.data_admissao.setDate(datetime_qdate(self.instance.data_admissao))
            self.selecionar_lotacao(self.instance.lotacao)
            if self.instance.inativo:
                self.ui.inativo.setCheckState(QtCore.Qt.Checked)
            else:
                self.ui.inativo.setCheckState(QtCore.Qt.Unchecked)
        self.ui.nome.setFocus()

    def carregar_cargo(self):
        self.cargos = list(Cargo.select2())
        self.ui.cargo.addItems([i.nome for i in self.cargos])
        if self.instance.id:
            self.ui.cargo.setCurrentIndex(
                self.cargos.index(self.instance.cargo)
                )

    def excluir(self):
        with DataBase().obter_database().atomic():
            if Confirmacao(
                parent=self, 
                text="Excluir servidor %s?" % self.instance.nome,
            ).exec_():
                self.instance.excluir_instancia()
                self.enviar_sinal_exclusao(self.instance)
                self.close()