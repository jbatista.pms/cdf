from .models import Servidor

def dados_servidor(servidor):
        dados = {}
        for i in list(Servidor.campos):
            dados[i] = str(getattr(servidor, i))
        dados['data_admissao'] = servidor.strftime('data_admissao')
        if servidor.lotacao.pai:
            dados['lotacao'] = servidor.lotacao.pai
            dados['sub_lotacao'] = servidor.lotacao
        else:
            dados['lotacao'] = servidor.lotacao
            dados['sub_lotacao'] = ''
        return dados