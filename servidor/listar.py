import sys
from PySide2 import QtCore, QtWidgets

from QtUtils import subwindowsbase
from QtUtils.validadores import TextoEmMaisculo
from QtUtils.qt import celula_data
from ComumComissoes.cargo.models import Cargo
from ComumComissoes.lotacao.models import Lotacao
from ComumComissoes.lotacao import controles_uteis
from .models import Servidor
from .views.listar import *
from . import servidor
from .cadastro import Cadastro


class Control(subwindowsbase.Listar, controles_uteis.LotacaoSelecao):
    lotacao = None
    classe_ui = Ui_Form

    def inicializar(self, *args, **kwargs):
        self.ui.pesquisa.setValidator(TextoEmMaisculo(self))
        self.ui.pesquisa.setFocus()

        self.carregar_cargos()
        self.carregar_lotacoes()

        self.atualizar()
        self.ui.tabela.horizontalHeader().setSectionHidden(0,True)

    def atualizar(self):
        pesquisa = self.ui.pesquisa.text()
        lotacao = self.lotacao_selecionada()
        sublotacao = self.sublotacao_indice()
        cargo = self.ui.cargo.currentIndex()
        inativo = self.ui.inativo.isChecked()

        fs = Servidor.select2(excluidos=self.ui.excluidos.isChecked()).where(
            Servidor.inativo==inativo
        )
        if pesquisa:
            fs = fs.where(Servidor.nome.contains(pesquisa) | Servidor.matricula.contains(pesquisa))
        
        if cargo>0:
            fs = fs.where(Servidor.cargo==self.cargos[cargo-1])

        if sublotacao>1:
            fs = fs.where(Servidor.lotacao==self.sublotacao_selecionada())
        elif sublotacao==0:
            fs = fs.join(Lotacao)
            fs = fs.where((Lotacao.pai==lotacao) | (Servidor.lotacao==lotacao))
        elif lotacao:
            fs = fs.where(Servidor.lotacao==lotacao)

        quantidade = fs.count()
        if quantidade == 1:
            self.ui.contador.setText("%i registro econtrado" % quantidade)
        else:
            self.ui.contador.setText("%i registros econtrados" % quantidade)

        self.ui.tabela.setRowCount(quantidade)
        for f in range(quantidade):
            self.ui.tabela.setVerticalHeaderItem(
                f,QtWidgets.QTableWidgetItem()
            )
            self.ui.tabela.verticalHeaderItem(f).setText(
                str(fs[f].id)
            )
            self.ui.tabela.setItem(f,0, QtWidgets.QTableWidgetItem())
            self.ui.tabela.setItem(f,1, QtWidgets.QTableWidgetItem())
            self.ui.tabela.setItem(f,2, QtWidgets.QTableWidgetItem())
            self.ui.tabela.item(f,0).setText(str(fs[f].id))
            celula_data(self.ui.tabela.item(f,1))
            self.ui.tabela.item(f,1).setText(fs[f].matricula)
            self.ui.tabela.item(f,2).setText("  "+fs[f].nome)

        self.list_dict = {str(i.id):i for i in fs}

    def abrir(self, row, column):
        id_servidor = self.ui.tabela.item(row,0).text()
        servidor.Control(self, instance=self.list_dict[id_servidor]).show()

    def cadastrar(self):
        Cadastro(self).exec_()
    
    def carregar_cargos(self):
        self.cargos = [i for i in Cargo.todos()]
        self.ui.cargo.clear()
        self.ui.cargo.addItem("")
        self.ui.cargo.addItems([i.nome for i in self.cargos])

    def limpar_cargo(self):
        self.ui.cargo.setCurrentIndex(0)

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, Cargo):
            self.carregar_cargos()
        elif isinstance(instance, Lotacao):
            self.carregar_lotacoes()
        if isinstance(instance, (Servidor, Cargo, Lotacao)):
            self.atualizar()

    def receber_sinal_exclusao(self, instance):
        if type(instance) == Servidor:
            self.atualizar()
