from QtUtils.db import DataBase
from loguru import logger

from avaliacao.mantenedor import MantenedorAvaliacao
from intersticio.mantenedor import IntersticioMantenedor
from intersticio.modelos import Intersticio


class MantenedorServidor(object):
    __ser = None

    def __init__(self, servidor):
        self.__ser = servidor
    
    def calcular_prazos_intersticios(self):
        with DataBase().obter_database().atomic():
            for inter in self.intersticios().filter(Intersticio.aberto==True).objects():
                man_inter = IntersticioMantenedor(inter)
                man_inter.calcular_prazo()
                man_inter.salvar()
    
    def intersticios(self):
        return Intersticio.select2().filter(
            Intersticio.servidor==self.__ser
        ).order_by(Intersticio.data_inicial.asc())

    def intersticios_abertos(self):
        return Intersticio.selecao_abertos().filter(Intersticio.servidor==self.__ser)
    
    def novo_intersticio(self):
        ultimo_intersticio = self.intersticios().order_by(Intersticio.data_inicial.desc()).first()
        if ultimo_intersticio and ultimo_intersticio.data_final >= DataBase().data().date():
            logger.error(f"Existe interstício em vigor para o servidor {self.__ser}")
            return False

        with DataBase().obter_database().atomic():
            mant_inter = IntersticioMantenedor.criar(
                servidor=self.__ser,
                anterior=ultimo_intersticio,
            )
            # Criar e calcular período de avaliações
            if ultimo_intersticio:
                anterior = IntersticioMantenedor(ultimo_intersticio).avaliacoes()[-1]
            else:
                anterior = None
            for o in range(1,4):
                mant_av = MantenedorAvaliacao.criar(
                    intersticio=mant_inter.intersticio,
                    anterior=anterior,
                    ordem=o,
                )
                mant_av.calcular_prazo()
                mant_av.salvar()
                anterior = mant_av.avaliacao
            # Salvar como interstício posterior
            if ultimo_intersticio:
                ultimo_intersticio.posterior = mant_inter.intersticio
                ultimo_intersticio.save()
                
        return True
    
    def recalcular_intersticios(self):
        logger.debug(f"Recalculando interstícios de {self.__ser}.")
        intersticios = self.intersticios_abertos().order_by(Intersticio.data_inicial.asc())
        if intersticios:
            for inter in intersticios:
                mant_int = IntersticioMantenedor(inter)
                mant_int.apurar_aproveitamento()
                mant_int.calcular_prazo()
                mant_int.salvar()
        else:
            logger.debug(f"Não há interstícios para {self.__ser}.")
    
    def salvar(self):
        self.__ser.save()