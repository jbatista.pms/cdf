from peewee import *
from QtUtils.db import ModelBase
from playhouse import fields

from ComumComissoes.lotacao.models import Lotacao
from ComumComissoes.cargo.models import Cargo
from QtUtils.historico.models import Historico


class Servidor(Historico):
    origem = ForeignKeyField('self', related_name='historico', null=True, db_column='origem')
    nome = CharField()
    matricula = CharField()
    data_admissao = DateField()
    data_base = DateField(null=True)
    cargo = ForeignKeyField(Cargo, related_name='servidores')
    lotacao = ForeignKeyField(Lotacao, related_name='servidores')
    inativo = BooleanField(default=False)

    class Meta:
        order_by = ('nome',)

    def __str__(self):
        return str(self.nome)
    
    def exportar_extras():
        return ['cargo', 'lotacao']
    
    def exportar_ignorar():
        return Historico.exportar_ignorar() + ['data_base']
    
    def ignorar_campos_mudancas():
        return Historico.ignorar_campos_mudancas() + ['data_base',]
    
    @classmethod
    def selecao_ativos(cls):
        return cls.select2().filter(inativo=False)

    def select2(excluidos=False, *args, **kwargs):
        return Servidor.select(*args, **kwargs).where(
            (Servidor.origem.is_null(True)) &
            (Servidor.data_exclusao.is_null(not excluidos))
        ).order_by(Servidor.nome)