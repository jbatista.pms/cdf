import sys
from PySide2 import QtCore, QtWidgets

from QtUtils.db import DataBase
from QtUtils import qt
from QtUtils import subwindowsbase
from QtUtils.date import datestr_date
from QtUtils.colecoes import excluir
from evento.models import Evento
from evento import evento
from falta.models import Falta
from falta import falta
from QtUtils.historico import historico
from avaliacao.models import Avaliacao, Tramite
from avaliacao import avaliacao
from ComumComissoes.lotacao.models import Lotacao
from intersticio.modelos import Intersticio
from ComumComissoes.cargo.models import Cargo
from .views.servidor import *
from .models import Servidor
from .cadastro import Cadastro
from .mantenedor import MantenedorServidor


class Control(subwindowsbase.Listar):
    __avaliacoes = []
    encontrado = True
    classe_ui = Ui_Form

    def __obter_avaliacaoes(self):
        self.__avaliacoes = Avaliacao.select2().where(Intersticio.servidor==self.instance)
        self.lista_dict_avaliacoes = {str(a.id):a for a in self.__avaliacoes.objects()}

    def inicializar(self, *args, **kwargs):
        self.carregar_dados()
        self.carregar_avaliacoes()
        self.carregar_eventos()
        self.carregar_intersticios()
        self.carregar_faltas()

    def carregar_avaliacoes(self):
        self.__obter_avaliacaoes()
        self.ui.avaliacoes.setRowCount(self.__avaliacoes.count())
        for n, avaliacao in enumerate(self.__avaliacoes):
            data_inicial, data_final = avaliacao.obter_prazos()
            # Inserir Id
            item = qt.QTableWidgetItem()
            item.setText(str(avaliacao.id))
            self.ui.avaliacoes.setVerticalHeaderItem(n, item)
            # Inserir data inicial
            item = qt.QTableWidgetItem()
            item.setText(data_inicial)
            item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
            self.ui.avaliacoes.setItem(n, 0, item)
            # Inserir data Final
            item = qt.QTableWidgetItem()
            item.setText(data_final)
            item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
            self.ui.avaliacoes.setItem(n, 1, item)
            # Inserir estado
            item = qt.QTableWidgetItem()
            item.setText(avaliacao.estado())
            item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
            self.ui.avaliacoes.setItem(n, 2, item)
            # Inserir pontos
            if avaliacao.pronto_para_intersticio():
                item = qt.QTableWidgetItem()
                item.setText(str(avaliacao.pontos_total))
                item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
                self.ui.avaliacoes.setItem(n, 3, item)
        #self.ui.avaliacoes.resizeColumnsToContents()

    def carregar_dados(self):
        self.setWindowTitle(self.instance.nome)
        self.ui.nome.setText(self.instance.nome)
        self.ui.matricula.setText(self.instance.matricula)
        self.ui.dataadmissao.setText(self.instance.strftime('data_admissao'))
        self.ui.cargo.setText(self.instance.cargo.nome)
        self.ui.lotacao.setText(self.instance.lotacao.descricao)
        if self.instance.data_exclusao != None:
            self.ui.alterar.setEnabled(False)
            self.ui.novo_evento.setEnabled(False)
            self.ui.nova_falta.setEnabled(False)

    def carregar_eventos(self):
        eventos = self.instance.eventos.select().where(
            (Evento.origem.is_null(True)) &
            (Evento.data_exclusao.is_null(not self.ui.eventos_excluidos.isChecked()))
        ).order_by(Evento.data_inicial.desc())
        self.ui.eventos.setRowCount(eventos.count())
        for f in range(eventos.count()):
            self.ui.eventos.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            for i in range(0,5):
                self.ui.eventos.setItem(f,i, qt.QTableWidgetItem())
            self.carregar_evento(f, eventos[f])
        self.lista_dict_eventos = {str(e.id):e for e in eventos}

    def carregar_evento(self, indice, evento):
        self.ui.eventos.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.eventos.verticalHeaderItem(indice).setText(
            str(evento.id)
        )
        self.ui.eventos.item(indice,0).setText(evento.strftime('data_inicial'))
        if evento.indeterminado:
            self.ui.eventos.item(indice,2).setText("Indeterminado")
            self.ui.eventos.item(indice,1).setText("Indeterminado")
        else:
            self.ui.eventos.item(indice,2).setText(str(evento.total))
            self.ui.eventos.item(indice,1).setText(
                evento.strftime('data_final')
            )
        self.ui.eventos.item(indice,3).setText("  "+evento.descricao)

        for i in range(0,3):
            celula_data(self.ui.eventos.item(indice,i))

    def carregar_faltas(self):
        faltas = self.instance.faltas.select().where(
            (Falta.origem.is_null(True)) & 
            (Falta.data_exclusao.is_null(not self.ui.faltas_excluidas.isChecked()))
        ).order_by(Falta.data_efeito.desc())
        self.ui.faltas.setRowCount(faltas.count())
        for f in range(faltas.count()):
            self.ui.faltas.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            for i in range(0,2):
                self.ui.faltas.setItem(f,i, qt.QTableWidgetItem())
            self.carregar_falta(f, faltas[f])
        self.lista_dict_faltas = {str(f.id):f for f in faltas}

    def carregar_falta(self, indice, falta):
        self.ui.faltas.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.faltas.verticalHeaderItem(indice).setText(
            str(falta.id)
        )
        self.ui.faltas.item(indice,0).setText(falta.strftime('data_efeito'))
        self.ui.faltas.item(indice,1).setText(str(falta.dias))

        for i in range(0,2):
            celula_data(self.ui.faltas.item(indice,i))

    def carregar_intersticio(self, indice, intersticio):
        self.ui.intersticios.setVerticalHeaderItem(
            indice, qt.QTableWidgetItem()
        )
        self.ui.intersticios.verticalHeaderItem(indice).setText(
            str(intersticio.id)
        )
        if intersticio.indeterminado:
            if intersticio.anterior and intersticio.anterior.indeterminado:
                data_inicial = "Indeterminado"
                data_final = "Indeterminado"
            else:
                data_inicial = intersticio.strftime('data_inicial')
                data_final ="Indeterminado"
        else:
            data_inicial = intersticio.strftime('data_inicial')
            data_final = intersticio.strftime('data_final')
        self.ui.intersticios.item(indice,0).setText(data_inicial)
        self.ui.intersticios.item(indice,1).setText(data_final)
        self.ui.intersticios.item(indice,2).setText(str(intersticio.aberto))
        # Aproveitamento
        if intersticio.aproveitamento:
            self.ui.intersticios.item(indice,3).setText(intersticio.aproveitamento_str)
        else:
            self.ui.intersticios.item(indice,3).setText('----')
        # Alinhas dados
        for i in range(0,4):
            celula_data(self.ui.intersticios.item(indice,i))

    def carregar_intersticios(self):
        intersticios = self.instance.intersticios.select().where(
            Intersticio.origem.is_null(True)
        ).order_by(Intersticio.data_inicial.desc())
        self.ui.intersticios.setRowCount(intersticios.count())
        for f in range(intersticios.count()):
            self.ui.intersticios.setVerticalHeaderItem(
                f,qt.QTableWidgetItem()
            )
            self.ui.intersticios.setItem(f,0, qt.QTableWidgetItem())
            self.ui.intersticios.setItem(f,1, qt.QTableWidgetItem())
            self.ui.intersticios.setItem(f,2, qt.QTableWidgetItem())
            self.ui.intersticios.setItem(f,3, qt.QTableWidgetItem())
            self.carregar_intersticio(f, intersticios[f])
    
    def historico(self):
        historico.Historico(self, instance=self.instance).exec_()

    def abrir_avaliacao(self, row, column):
        avaliacao_id = self.ui.avaliacoes.verticalHeaderItem(row).text()
        instance = self.lista_dict_avaliacoes[avaliacao_id]
        avaliacao.Avaliacao(parent=self, instance=instance).exec_()

    def abrir_evento(self, row, column):
        instance = self.lista_dict_eventos[self.ui.eventos.verticalHeaderItem(row).text()]
        evento.Control(self, instance=instance).exec_()

    def abrir_falta(self, row, column):
        instance = self.lista_dict_faltas[self.ui.faltas.verticalHeaderItem(row).text()]
        falta.Control(self,instance=instance).exec_()

    def alterar(self):
        Cadastro(self, instance=self.instance).exec_()

    def novo_evento(self):
        if evento.Control(self, instance=Evento(servidor=self.instance)).exec_():
            self.inicializar()

    def nova_falta(self):
        if falta.Control(self, instance=Falta(servidor=self.instance)).exec_():
            self.inicializar()

    def atualizar_avaliacoes(self):
        with DataBase().obter_database().atomic():
            mant_ser = MantenedorServidor(self.instance)
            mant_ser.calcular_prazos_intersticios()
            mant_ser.salvar()
        self.inicializar()

    def teste_unica(self, windows):
        return windows.instance.matricula == self.instance.matricula

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, (Avaliacao, Intersticio, Tramite)):
            self.carregar_avaliacoes()
            self.carregar_intersticios()
        elif isinstance(instance, (Evento, Falta)):
            self.carregar_avaliacoes()
            self.carregar_intersticios()
            self.carregar_eventos()
            self.carregar_faltas()
        elif isinstance(instance, (Servidor, Lotacao, Cargo)):
            self.carregar_dados()

    def receber_sinal_exclusao(self, instance):
        if isinstance(instance, (Evento, Falta)):
            self.carregar_avaliacoes()
            self.carregar_intersticios()
            self.carregar_eventos()
            self.carregar_faltas()
        elif isinstance(instance, (Lotacao, Cargo)):
            self.carregar_dados()
        elif isinstance(instance, Servidor):
            self.inicializar()



def celula_data(celula):
    celula.setTextAlignment(
        QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter
    )
