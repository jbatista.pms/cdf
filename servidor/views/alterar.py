# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'alterar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(447, 231)
        Dialog.setMinimumSize(QSize(447, 231))
        Dialog.setMaximumSize(QSize(447, 231))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_6 = QLabel(Dialog)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout.addWidget(self.label_6, 5, 0, 1, 1)

        self.sublotacao = QComboBox(Dialog)
        self.sublotacao.setObjectName(u"sublotacao")

        self.gridLayout.addWidget(self.sublotacao, 5, 1, 1, 3)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.label_4 = QLabel(Dialog)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)

        self.matricula = QLineEdit(Dialog)
        self.matricula.setObjectName(u"matricula")

        self.gridLayout.addWidget(self.matricula, 2, 1, 1, 1)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.excluir = QPushButton(self.frame)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.salvar = QPushButton(self.frame)
        self.salvar.setObjectName(u"salvar")

        self.horizontalLayout.addWidget(self.salvar)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout.addWidget(self.frame, 8, 0, 1, 4)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 2, 2, 1, 1)

        self.data_admissao = QDateEdit(Dialog)
        self.data_admissao.setObjectName(u"data_admissao")
        self.data_admissao.setCalendarPopup(True)

        self.gridLayout.addWidget(self.data_admissao, 2, 3, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 7, 0, 1, 4)

        self.lotacao = QComboBox(Dialog)
        self.lotacao.setObjectName(u"lotacao")

        self.gridLayout.addWidget(self.lotacao, 4, 1, 1, 3)

        self.label_5 = QLabel(Dialog)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_5, 4, 0, 1, 1)

        self.cargo = QComboBox(Dialog)
        self.cargo.setObjectName(u"cargo")

        self.gridLayout.addWidget(self.cargo, 3, 1, 1, 3)

        self.nome = QLineEdit(Dialog)
        self.nome.setObjectName(u"nome")

        self.gridLayout.addWidget(self.nome, 0, 1, 1, 3)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.inativo = QCheckBox(Dialog)
        self.inativo.setObjectName(u"inativo")

        self.gridLayout.addWidget(self.inativo, 6, 1, 1, 3)

        QWidget.setTabOrder(self.nome, self.matricula)
        QWidget.setTabOrder(self.matricula, self.data_admissao)
        QWidget.setTabOrder(self.data_admissao, self.cargo)
        QWidget.setTabOrder(self.cargo, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.sublotacao)

        self.retranslateUi(Dialog)
        self.salvar.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)
        self.excluir.released.connect(Dialog.excluir)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro de Servidor", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Sublota\u00e7\u00e3o:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Matr\u00edcula:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Cargo:", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Data de admiss\u00e3o:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Lota\u00e7\u00e3o:", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.inativo.setText(QCoreApplication.translate("Dialog", u"Demitido/Exonerado/Inativo", None))
    # retranslateUi

