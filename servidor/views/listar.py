# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(519, 623)
        sizePolicy = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        Form.setMinimumSize(QSize(519, 623))
        Form.setMaximumSize(QSize(16777215, 16777215))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.tabela = QTableWidget(Form)
        if (self.tabela.columnCount() < 3):
            self.tabela.setColumnCount(3)
        __qtablewidgetitem = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        self.tabela.setObjectName(u"tabela")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.tabela.sizePolicy().hasHeightForWidth())
        self.tabela.setSizePolicy(sizePolicy1)
        self.tabela.setFrameShape(QFrame.StyledPanel)
        self.tabela.setFrameShadow(QFrame.Sunken)
        self.tabela.setAutoScroll(True)
        self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabela.setTabKeyNavigation(True)
        self.tabela.setDragEnabled(False)
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.tabela.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabela.setGridStyle(Qt.CustomDashLine)
        self.tabela.setSortingEnabled(False)
        self.tabela.setWordWrap(True)
        self.tabela.setCornerButtonEnabled(True)
        self.tabela.horizontalHeader().setCascadingSectionResizes(True)
        self.tabela.horizontalHeader().setMinimumSectionSize(30)
        self.tabela.horizontalHeader().setDefaultSectionSize(100)
        self.tabela.horizontalHeader().setHighlightSections(False)
        self.tabela.horizontalHeader().setProperty("showSortIndicator", True)
        self.tabela.horizontalHeader().setStretchLastSection(True)
        self.tabela.verticalHeader().setVisible(False)
        self.tabela.verticalHeader().setCascadingSectionResizes(False)

        self.gridLayout.addWidget(self.tabela, 8, 0, 1, 3)

        self.contador = QLabel(Form)
        self.contador.setObjectName(u"contador")

        self.gridLayout.addWidget(self.contador, 9, 0, 1, 3)

        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        sizePolicy2 = QSizePolicy(QSizePolicy.Ignored, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy2)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, -1)
        self.label_2 = QLabel(self.frame)
        self.label_2.setObjectName(u"label_2")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy3)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_2, 2, 0, 1, 1)

        self.lotacao = QComboBox(self.frame)
        self.lotacao.setObjectName(u"lotacao")
        sizePolicy4 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.lotacao.sizePolicy().hasHeightForWidth())
        self.lotacao.setSizePolicy(sizePolicy4)
        self.lotacao.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.gridLayout_2.addWidget(self.lotacao, 2, 1, 1, 1)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")
        sizePolicy5 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.pushButton_2.sizePolicy().hasHeightForWidth())
        self.pushButton_2.setSizePolicy(sizePolicy5)
        self.pushButton_2.setMaximumSize(QSize(22, 22))

        self.gridLayout_2.addWidget(self.pushButton_2, 1, 2, 1, 1)

        self.pushButton = QPushButton(self.frame)
        self.pushButton.setObjectName(u"pushButton")
        sizePolicy5.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy5)
        self.pushButton.setMaximumSize(QSize(22, 22))
        self.pushButton.setLayoutDirection(Qt.LeftToRight)

        self.gridLayout_2.addWidget(self.pushButton, 0, 2, 1, 1)

        self.pushButton_3 = QPushButton(self.frame)
        self.pushButton_3.setObjectName(u"pushButton_3")
        sizePolicy5.setHeightForWidth(self.pushButton_3.sizePolicy().hasHeightForWidth())
        self.pushButton_3.setSizePolicy(sizePolicy5)
        self.pushButton_3.setMaximumSize(QSize(22, 22))

        self.gridLayout_2.addWidget(self.pushButton_3, 2, 2, 1, 1)

        self.cargo = QComboBox(self.frame)
        self.cargo.setObjectName(u"cargo")
        sizePolicy4.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy4)
        self.cargo.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.gridLayout_2.addWidget(self.cargo, 1, 1, 1, 1)

        self.pesquisa = QLineEdit(self.frame)
        self.pesquisa.setObjectName(u"pesquisa")
        sizePolicy6 = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.pesquisa.sizePolicy().hasHeightForWidth())
        self.pesquisa.setSizePolicy(sizePolicy6)

        self.gridLayout_2.addWidget(self.pesquisa, 0, 1, 1, 1)

        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")
        sizePolicy7 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy7.setHorizontalStretch(0)
        sizePolicy7.setVerticalStretch(0)
        sizePolicy7.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy7)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.label_4 = QLabel(self.frame)
        self.label_4.setObjectName(u"label_4")
        sizePolicy7.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy7)
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_4, 1, 0, 1, 1)

        self.pushButton_4 = QPushButton(self.frame)
        self.pushButton_4.setObjectName(u"pushButton_4")
        self.pushButton_4.setMaximumSize(QSize(22, 22))

        self.gridLayout_2.addWidget(self.pushButton_4, 3, 2, 1, 1)

        self.label_3 = QLabel(self.frame)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_3, 3, 0, 1, 1)

        self.sublotacao = QComboBox(self.frame)
        self.sublotacao.setObjectName(u"sublotacao")

        self.gridLayout_2.addWidget(self.sublotacao, 3, 1, 1, 1)

        self.frame_2 = QFrame(self.frame)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_2)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.inativo = QCheckBox(self.frame_2)
        self.inativo.setObjectName(u"inativo")

        self.horizontalLayout.addWidget(self.inativo)

        self.excluidos = QCheckBox(self.frame_2)
        self.excluidos.setObjectName(u"excluidos")

        self.horizontalLayout.addWidget(self.excluidos)


        self.gridLayout_2.addWidget(self.frame_2, 5, 0, 1, 2)


        self.gridLayout.addWidget(self.frame, 0, 0, 1, 3)

        QWidget.setTabOrder(self.pesquisa, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.cargo)
        QWidget.setTabOrder(self.cargo, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.lotacao)
        QWidget.setTabOrder(self.lotacao, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.sublotacao)
        QWidget.setTabOrder(self.sublotacao, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.inativo)
        QWidget.setTabOrder(self.inativo, self.excluidos)
        QWidget.setTabOrder(self.excluidos, self.tabela)

        self.retranslateUi(Form)
        self.tabela.cellDoubleClicked.connect(Form.abrir)
        self.cargo.currentIndexChanged.connect(Form.atualizar)
        self.pesquisa.textChanged.connect(Form.atualizar)
        self.pushButton.released.connect(self.pesquisa.clear)
        self.pushButton_2.released.connect(Form.limpar_cargo)
        self.pushButton_3.released.connect(Form.limpar_lotacao)
        self.pushButton.released.connect(self.pesquisa.setFocus)
        self.pushButton_4.released.connect(Form.limpar_sublotacao)
        self.sublotacao.currentIndexChanged.connect(Form.atualizar)
        self.lotacao.currentIndexChanged.connect(Form.atualizar)
        self.inativo.released.connect(Form.atualizar)
        self.excluidos.stateChanged.connect(Form.atualizar)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Servidores", None))
        ___qtablewidgetitem = self.tabela.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"id", None));
        ___qtablewidgetitem1 = self.tabela.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Matr\u00edcula", None));
        ___qtablewidgetitem2 = self.tabela.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Nome", None));
        self.contador.setText(QCoreApplication.translate("Form", u"0 registros encontrados", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o:", None))
        self.pushButton_2.setText(QCoreApplication.translate("Form", u"X", None))
        self.pushButton.setText(QCoreApplication.translate("Form", u"X", None))
        self.pushButton_3.setText(QCoreApplication.translate("Form", u"X", None))
        self.label.setText(QCoreApplication.translate("Form", u"Nome/Matr\u00edcula:", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Cargo:", None))
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"X", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Sublota\u00e7\u00e3o:", None))
        self.inativo.setText(QCoreApplication.translate("Form", u"Demitido/Exonerado/Inativo", None))
        self.excluidos.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
    # retranslateUi

