# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'servidor.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(711, 442)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Form.sizePolicy().hasHeightForWidth())
        Form.setSizePolicy(sizePolicy)
        Form.setMinimumSize(QSize(711, 442))
        Form.setMaximumSize(QSize(16777215, 16777215))
        self.gridLayout = QGridLayout(Form)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout_4 = QGridLayout()
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.dataadmissao = QLabel(Form)
        self.dataadmissao.setObjectName(u"dataadmissao")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.dataadmissao.sizePolicy().hasHeightForWidth())
        self.dataadmissao.setSizePolicy(sizePolicy1)

        self.gridLayout_4.addWidget(self.dataadmissao, 4, 2, 1, 1)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_4.addWidget(self.label, 3, 1, 1, 1)

        self.nome = QLabel(Form)
        self.nome.setObjectName(u"nome")
        sizePolicy1.setHeightForWidth(self.nome.sizePolicy().hasHeightForWidth())
        self.nome.setSizePolicy(sizePolicy1)

        self.gridLayout_4.addWidget(self.nome, 1, 2, 1, 1)

        self.label_5 = QLabel(Form)
        self.label_5.setObjectName(u"label_5")
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_4.addWidget(self.label_5, 4, 1, 1, 1)

        self.cargo = QLabel(Form)
        self.cargo.setObjectName(u"cargo")
        sizePolicy1.setHeightForWidth(self.cargo.sizePolicy().hasHeightForWidth())
        self.cargo.setSizePolicy(sizePolicy1)
        self.cargo.setWordWrap(True)

        self.gridLayout_4.addWidget(self.cargo, 3, 2, 1, 1)

        self.lotacao = QLabel(Form)
        self.lotacao.setObjectName(u"lotacao")
        self.lotacao.setWordWrap(True)

        self.gridLayout_4.addWidget(self.lotacao, 5, 2, 1, 1)

        self.matricula = QLabel(Form)
        self.matricula.setObjectName(u"matricula")
        sizePolicy1.setHeightForWidth(self.matricula.sizePolicy().hasHeightForWidth())
        self.matricula.setSizePolicy(sizePolicy1)

        self.gridLayout_4.addWidget(self.matricula, 2, 2, 1, 1)

        self.label_7 = QLabel(Form)
        self.label_7.setObjectName(u"label_7")
        sizePolicy.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy)
        self.label_7.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_7, 2, 1, 1, 1)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_4.addWidget(self.label_2, 1, 1, 1, 1)

        self.label_3 = QLabel(Form)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTop|Qt.AlignTrailing)

        self.gridLayout_4.addWidget(self.label_3, 5, 1, 1, 1)


        self.gridLayout.addLayout(self.gridLayout_4, 0, 0, 1, 1)

        self.excluidos = QTabWidget(Form)
        self.excluidos.setObjectName(u"excluidos")
        self.excluidos.setTabShape(QTabWidget.Rounded)
        self.tab_avaliacoes = QWidget()
        self.tab_avaliacoes.setObjectName(u"tab_avaliacoes")
        self.gridLayout_2 = QGridLayout(self.tab_avaliacoes)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.avaliacoes = QTableWidget(self.tab_avaliacoes)
        if (self.avaliacoes.columnCount() < 4):
            self.avaliacoes.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.avaliacoes.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        if (self.avaliacoes.rowCount() < 2):
            self.avaliacoes.setRowCount(2)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.avaliacoes.setVerticalHeaderItem(0, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.avaliacoes.setVerticalHeaderItem(1, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        __qtablewidgetitem6.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.avaliacoes.setItem(0, 0, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        __qtablewidgetitem7.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.avaliacoes.setItem(0, 1, __qtablewidgetitem7)
        self.avaliacoes.setObjectName(u"avaliacoes")
        self.avaliacoes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.avaliacoes.setTabKeyNavigation(True)
        self.avaliacoes.setDragEnabled(False)
        self.avaliacoes.setDragDropOverwriteMode(True)
        self.avaliacoes.setDragDropMode(QAbstractItemView.NoDragDrop)
        self.avaliacoes.setAlternatingRowColors(True)
        self.avaliacoes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.avaliacoes.setTextElideMode(Qt.ElideNone)
        self.avaliacoes.setGridStyle(Qt.SolidLine)
        self.avaliacoes.setSortingEnabled(False)
        self.avaliacoes.setWordWrap(True)
        self.avaliacoes.setCornerButtonEnabled(True)
        self.avaliacoes.horizontalHeader().setCascadingSectionResizes(True)
        self.avaliacoes.horizontalHeader().setDefaultSectionSize(110)
        self.avaliacoes.horizontalHeader().setHighlightSections(False)
        self.avaliacoes.horizontalHeader().setProperty("showSortIndicator", False)
        self.avaliacoes.horizontalHeader().setStretchLastSection(False)
        self.avaliacoes.verticalHeader().setVisible(False)
        self.avaliacoes.verticalHeader().setStretchLastSection(False)

        self.gridLayout_2.addWidget(self.avaliacoes, 0, 0, 1, 2)

        self.excluidos.addTab(self.tab_avaliacoes, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayout_7 = QGridLayout(self.tab_2)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.intersticios = QTableWidget(self.tab_2)
        if (self.intersticios.columnCount() < 4):
            self.intersticios.setColumnCount(4)
        __qtablewidgetitem8 = QTableWidgetItem()
        __qtablewidgetitem8.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setHorizontalHeaderItem(0, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        __qtablewidgetitem9.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setHorizontalHeaderItem(1, __qtablewidgetitem9)
        __qtablewidgetitem10 = QTableWidgetItem()
        __qtablewidgetitem10.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setHorizontalHeaderItem(2, __qtablewidgetitem10)
        __qtablewidgetitem11 = QTableWidgetItem()
        __qtablewidgetitem11.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setHorizontalHeaderItem(3, __qtablewidgetitem11)
        if (self.intersticios.rowCount() < 1):
            self.intersticios.setRowCount(1)
        __qtablewidgetitem12 = QTableWidgetItem()
        __qtablewidgetitem12.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setVerticalHeaderItem(0, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        __qtablewidgetitem13.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setItem(0, 0, __qtablewidgetitem13)
        __qtablewidgetitem14 = QTableWidgetItem()
        __qtablewidgetitem14.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setItem(0, 1, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        __qtablewidgetitem15.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter|Qt.AlignCenter);
        self.intersticios.setItem(0, 2, __qtablewidgetitem15)
        self.intersticios.setObjectName(u"intersticios")
        self.intersticios.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.intersticios.setAlternatingRowColors(True)
        self.intersticios.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.intersticios.horizontalHeader().setDefaultSectionSize(110)
        self.intersticios.horizontalHeader().setHighlightSections(False)
        self.intersticios.horizontalHeader().setProperty("showSortIndicator", False)
        self.intersticios.verticalHeader().setVisible(False)
        self.intersticios.verticalHeader().setHighlightSections(False)

        self.gridLayout_7.addWidget(self.intersticios, 0, 0, 1, 1)

        self.excluidos.addTab(self.tab_2, "")
        self.tab_eventos = QWidget()
        self.tab_eventos.setObjectName(u"tab_eventos")
        self.gridLayout_5 = QGridLayout(self.tab_eventos)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.novo_evento = QPushButton(self.tab_eventos)
        self.novo_evento.setObjectName(u"novo_evento")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.novo_evento.sizePolicy().hasHeightForWidth())
        self.novo_evento.setSizePolicy(sizePolicy2)

        self.gridLayout_5.addWidget(self.novo_evento, 0, 0, 1, 1)

        self.eventos = QTableWidget(self.tab_eventos)
        if (self.eventos.columnCount() < 4):
            self.eventos.setColumnCount(4)
        __qtablewidgetitem16 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(0, __qtablewidgetitem16)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(1, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(2, __qtablewidgetitem18)
        __qtablewidgetitem19 = QTableWidgetItem()
        self.eventos.setHorizontalHeaderItem(3, __qtablewidgetitem19)
        if (self.eventos.rowCount() < 2):
            self.eventos.setRowCount(2)
        __qtablewidgetitem20 = QTableWidgetItem()
        self.eventos.setVerticalHeaderItem(0, __qtablewidgetitem20)
        __qtablewidgetitem21 = QTableWidgetItem()
        self.eventos.setVerticalHeaderItem(1, __qtablewidgetitem21)
        self.eventos.setObjectName(u"eventos")
        self.eventos.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.eventos.setAlternatingRowColors(True)
        self.eventos.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.eventos.horizontalHeader().setCascadingSectionResizes(True)
        self.eventos.horizontalHeader().setDefaultSectionSize(110)
        self.eventos.horizontalHeader().setHighlightSections(False)
        self.eventos.horizontalHeader().setProperty("showSortIndicator", True)
        self.eventos.horizontalHeader().setStretchLastSection(False)
        self.eventos.verticalHeader().setVisible(False)

        self.gridLayout_5.addWidget(self.eventos, 1, 0, 1, 3)

        self.eventos_excluidos = QCheckBox(self.tab_eventos)
        self.eventos_excluidos.setObjectName(u"eventos_excluidos")

        self.gridLayout_5.addWidget(self.eventos_excluidos, 0, 2, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_5.addItem(self.horizontalSpacer_3, 0, 1, 1, 1)

        self.excluidos.addTab(self.tab_eventos, "")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_3 = QGridLayout(self.tab)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.faltas_excluidas = QCheckBox(self.tab)
        self.faltas_excluidas.setObjectName(u"faltas_excluidas")

        self.gridLayout_3.addWidget(self.faltas_excluidas, 0, 2, 1, 1)

        self.faltas = QTableWidget(self.tab)
        if (self.faltas.columnCount() < 2):
            self.faltas.setColumnCount(2)
        __qtablewidgetitem22 = QTableWidgetItem()
        self.faltas.setHorizontalHeaderItem(0, __qtablewidgetitem22)
        __qtablewidgetitem23 = QTableWidgetItem()
        self.faltas.setHorizontalHeaderItem(1, __qtablewidgetitem23)
        if (self.faltas.rowCount() < 2):
            self.faltas.setRowCount(2)
        __qtablewidgetitem24 = QTableWidgetItem()
        self.faltas.setVerticalHeaderItem(0, __qtablewidgetitem24)
        __qtablewidgetitem25 = QTableWidgetItem()
        self.faltas.setVerticalHeaderItem(1, __qtablewidgetitem25)
        self.faltas.setObjectName(u"faltas")
        self.faltas.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.faltas.setAlternatingRowColors(True)
        self.faltas.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.faltas.horizontalHeader().setCascadingSectionResizes(True)
        self.faltas.horizontalHeader().setMinimumSectionSize(150)
        self.faltas.horizontalHeader().setDefaultSectionSize(110)
        self.faltas.horizontalHeader().setHighlightSections(False)
        self.faltas.horizontalHeader().setProperty("showSortIndicator", True)
        self.faltas.horizontalHeader().setStretchLastSection(False)
        self.faltas.verticalHeader().setVisible(False)

        self.gridLayout_3.addWidget(self.faltas, 1, 0, 1, 3)

        self.nova_falta = QPushButton(self.tab)
        self.nova_falta.setObjectName(u"nova_falta")
        sizePolicy2.setHeightForWidth(self.nova_falta.sizePolicy().hasHeightForWidth())
        self.nova_falta.setSizePolicy(sizePolicy2)

        self.gridLayout_3.addWidget(self.nova_falta, 0, 0, 1, 1)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_4, 0, 1, 1, 1)

        self.excluidos.addTab(self.tab, "")

        self.gridLayout.addWidget(self.excluidos, 1, 0, 1, 2)

        self.frame = QFrame(Form)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setSpacing(5)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.alterar = QPushButton(self.frame)
        self.alterar.setObjectName(u"alterar")
        sizePolicy2.setHeightForWidth(self.alterar.sizePolicy().hasHeightForWidth())
        self.alterar.setSizePolicy(sizePolicy2)

        self.horizontalLayout.addWidget(self.alterar)

        self.pushButton_5 = QPushButton(self.frame)
        self.pushButton_5.setObjectName(u"pushButton_5")

        self.horizontalLayout.addWidget(self.pushButton_5)

        self.pushButton_4 = QPushButton(self.frame)
        self.pushButton_4.setObjectName(u"pushButton_4")
        sizePolicy2.setHeightForWidth(self.pushButton_4.sizePolicy().hasHeightForWidth())
        self.pushButton_4.setSizePolicy(sizePolicy2)

        self.horizontalLayout.addWidget(self.pushButton_4)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout.addWidget(self.frame, 2, 0, 1, 1)

        QWidget.setTabOrder(self.excluidos, self.avaliacoes)
        QWidget.setTabOrder(self.avaliacoes, self.intersticios)
        QWidget.setTabOrder(self.intersticios, self.novo_evento)
        QWidget.setTabOrder(self.novo_evento, self.eventos_excluidos)
        QWidget.setTabOrder(self.eventos_excluidos, self.eventos)
        QWidget.setTabOrder(self.eventos, self.nova_falta)
        QWidget.setTabOrder(self.nova_falta, self.faltas_excluidas)
        QWidget.setTabOrder(self.faltas_excluidas, self.faltas)
        QWidget.setTabOrder(self.faltas, self.alterar)
        QWidget.setTabOrder(self.alterar, self.pushButton_4)

        self.retranslateUi(Form)
        self.avaliacoes.cellDoubleClicked.connect(Form.abrir_avaliacao)
        self.novo_evento.clicked.connect(Form.novo_evento)
        self.eventos.cellDoubleClicked.connect(Form.abrir_evento)
        self.alterar.released.connect(Form.alterar)
        self.faltas.cellDoubleClicked.connect(Form.abrir_falta)
        self.nova_falta.released.connect(Form.nova_falta)
        self.pushButton_5.released.connect(Form.close)
        self.pushButton_4.released.connect(Form.historico)
        self.eventos_excluidos.stateChanged.connect(Form.carregar_eventos)
        self.faltas_excluidas.stateChanged.connect(Form.carregar_faltas)

        self.excluidos.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Servidor", None))
        self.dataadmissao.setText(QCoreApplication.translate("Form", u"data de admiss\u00e3o", None))
        self.label.setText(QCoreApplication.translate("Form", u"Cargo:", None))
        self.nome.setText(QCoreApplication.translate("Form", u"nome", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Data de admiss\u00e3o:", None))
        self.cargo.setText(QCoreApplication.translate("Form", u"cargo", None))
        self.lotacao.setText(QCoreApplication.translate("Form", u"lotacao", None))
        self.matricula.setText(QCoreApplication.translate("Form", u"matricula", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Matr\u00edcula:", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Nome:", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Lota\u00e7\u00e3o:", None))
        ___qtablewidgetitem = self.avaliacoes.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Form", u"Data inicial", None));
        ___qtablewidgetitem1 = self.avaliacoes.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem2 = self.avaliacoes.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Form", u"Formul\u00e1rio", None));
        ___qtablewidgetitem3 = self.avaliacoes.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Form", u"Pontos", None));
        ___qtablewidgetitem4 = self.avaliacoes.verticalHeaderItem(0)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Form", u"New Row", None));
        ___qtablewidgetitem5 = self.avaliacoes.verticalHeaderItem(1)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Form", u"New Row", None));

        __sortingEnabled = self.avaliacoes.isSortingEnabled()
        self.avaliacoes.setSortingEnabled(False)
        ___qtablewidgetitem6 = self.avaliacoes.item(0, 0)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("Form", u"18/01/1990", None));
        ___qtablewidgetitem7 = self.avaliacoes.item(0, 1)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("Form", u"18/01/1990", None));
        self.avaliacoes.setSortingEnabled(__sortingEnabled)

        self.excluidos.setTabText(self.excluidos.indexOf(self.tab_avaliacoes), QCoreApplication.translate("Form", u"Avalia\u00e7\u00f5es", None))
        ___qtablewidgetitem8 = self.intersticios.horizontalHeaderItem(0)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("Form", u"Data in\u00edcio", None));
        ___qtablewidgetitem9 = self.intersticios.horizontalHeaderItem(1)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem10 = self.intersticios.horizontalHeaderItem(2)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("Form", u"Aberto", None));
        ___qtablewidgetitem11 = self.intersticios.horizontalHeaderItem(3)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("Form", u"Aproveitamento", None));
        ___qtablewidgetitem12 = self.intersticios.verticalHeaderItem(0)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("Form", u"1", None));

        __sortingEnabled1 = self.intersticios.isSortingEnabled()
        self.intersticios.setSortingEnabled(False)
        ___qtablewidgetitem13 = self.intersticios.item(0, 0)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("Form", u"q", None));
        ___qtablewidgetitem14 = self.intersticios.item(0, 1)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("Form", u"d", None));
        ___qtablewidgetitem15 = self.intersticios.item(0, 2)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("Form", u"e", None));
        self.intersticios.setSortingEnabled(__sortingEnabled1)

        self.excluidos.setTabText(self.excluidos.indexOf(self.tab_2), QCoreApplication.translate("Form", u"Interst\u00edcios", None))
        self.novo_evento.setText(QCoreApplication.translate("Form", u"Novo", None))
        ___qtablewidgetitem16 = self.eventos.horizontalHeaderItem(0)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("Form", u"Data inicial", None));
        ___qtablewidgetitem17 = self.eventos.horizontalHeaderItem(1)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("Form", u"Data final", None));
        ___qtablewidgetitem18 = self.eventos.horizontalHeaderItem(2)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("Form", u"Total de dias", None));
        ___qtablewidgetitem19 = self.eventos.horizontalHeaderItem(3)
        ___qtablewidgetitem19.setText(QCoreApplication.translate("Form", u"Descri\u00e7\u00e3o", None));
        ___qtablewidgetitem20 = self.eventos.verticalHeaderItem(0)
        ___qtablewidgetitem20.setText(QCoreApplication.translate("Form", u"1", None));
        ___qtablewidgetitem21 = self.eventos.verticalHeaderItem(1)
        ___qtablewidgetitem21.setText(QCoreApplication.translate("Form", u"2", None));
        self.eventos_excluidos.setText(QCoreApplication.translate("Form", u"Exclu\u00eddos", None))
        self.excluidos.setTabText(self.excluidos.indexOf(self.tab_eventos), QCoreApplication.translate("Form", u"Eventos", None))
        self.faltas_excluidas.setText(QCoreApplication.translate("Form", u"Excluidos", None))
        ___qtablewidgetitem22 = self.faltas.horizontalHeaderItem(0)
        ___qtablewidgetitem22.setText(QCoreApplication.translate("Form", u"Data de efeito", None));
        ___qtablewidgetitem23 = self.faltas.horizontalHeaderItem(1)
        ___qtablewidgetitem23.setText(QCoreApplication.translate("Form", u"Dias", None));
        ___qtablewidgetitem24 = self.faltas.verticalHeaderItem(0)
        ___qtablewidgetitem24.setText(QCoreApplication.translate("Form", u"1", None));
        ___qtablewidgetitem25 = self.faltas.verticalHeaderItem(1)
        ___qtablewidgetitem25.setText(QCoreApplication.translate("Form", u"2", None));
        self.nova_falta.setText(QCoreApplication.translate("Form", u"Novo", None))
        self.excluidos.setTabText(self.excluidos.indexOf(self.tab), QCoreApplication.translate("Form", u"Faltas", None))
        self.alterar.setText(QCoreApplication.translate("Form", u"Alterar", None))
        self.pushButton_5.setText(QCoreApplication.translate("Form", u"Fechar", None))
        self.pushButton_4.setText(QCoreApplication.translate("Form", u"Hist\u00f3rico", None))
    # retranslateUi

