from datetime import timedelta

from avaliacao.models import Avaliacao
from intersticio.mantenedor import IntersticioMantenedor
from intersticio.modelos import Intersticio
from servidor.mantenedor import MantenedorServidor
from servidor.models import Servidor

def verificar_intersticios():
    print(">>> Apurando erros entre as datas de avaliações.")
    for ser in Servidor.select2():
        avaliacoes = Avaliacao.select2().filter(Intersticio.servidor==ser)
        dt_inicial_proximo = None
        for av in avaliacoes:
            if av.anterior and not av.indeterminado:
                if av.data_inicial != dt_inicial_proximo:
                    print('- %s' % av.intersticio.servidor)
                    print('  Data inicial: %s' % av.data_inicial)
                    print('  Data final: %s' % av.data_final)
                    print('  Anterior: %s' % av.anterior != None)
            dt_inicial_proximo = av.data_final + timedelta(days=1)

def recalcular_intersticios():
    print(">>> Recalculando prazos e notas de interstícios.")
    servidores = Servidor.select2()
    for ser in servidores:
        print('Recalculando Servidor: %s' % ser)
        mant_ser = MantenedorServidor(ser)
        mant_ser.recalcular_intersticios()
    print("%i Servidores com interstícios recalculados." % len(servidores))

def reordenar_avs():
    print(">>> Reordenando data de avaliações.")
    intersticios = Intersticio.select2().filter(Intersticio.anterior.is_null(False))
    for inter in intersticios.join(Servidor).order_by(Servidor.nome):
        mant_int = IntersticioMantenedor(inter)
        av_erro = mant_int.avaliacoes().objects()[0]
        mant_int_ant = IntersticioMantenedor(inter.anterior)
        av_ant = mant_int_ant.avaliacoes()[-1]
        if av_erro.anterior is None:
            print('Servidor processado: %s' % inter.servidor)
            av_erro.anterior = av_ant
            av_erro.save()
    print("%i interstícios com avaliações reordenadas." % len(intersticios))

def executar():
    reordenar_avs()
    recalcular_intersticios()
    verificar_intersticios()